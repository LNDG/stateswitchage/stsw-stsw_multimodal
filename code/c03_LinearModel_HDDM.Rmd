---
title: "StateSwitch_rmcorr"
output:
  html_document:
    fig_crop: no
  pdf_document: default
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)

#install.packages('R.matlab')
#install.packages('lmSupport')
#install.packages("rlang") 
library(R.matlab)
library(knitr)
library(lmSupport)
library(lmerTest)

options(digits = 2)
opts_knit$set(global.par=TRUE)
```
```{r, include=FALSE}
par(mar=c(5,5,0,0))
```

# read the data for the ANOVA from MATLAB output

```{r dataRead}
rm(list = ls()) # clear workspace

data = readMat('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/multimodal/B_crossCorrelations/B_data/STSWDsummary_YA_forR.mat')
data = as.data.frame(matrix(unlist(data), nrow=30, byrow=T),stringsAsFactors=TRUE, header = TRUE)
data = t(data) # transpose matrix
data = data.frame(data) # convert to data frame
names(data) = as.character(unlist(data[1, ]))
data = data[-1, ]

data$ID = as.factor(data$Subject)
data$cond = as.factor(data$Condition)
data$drift_eeg = as.numeric(sub(",", ".", data$drift_eeg, fixed = TRUE))
data$ndt_eeg = as.numeric(sub(",", ".", data$ndt_eeg, fixed = TRUE))
data$drift_mri = as.numeric(sub(",", ".", data$drift_mri, fixed = TRUE))
data$ndt_mri = as.numeric(sub(",", ".", data$ndt_mri, fixed = TRUE))
data$pupil = as.numeric(sub(",", ".", data$pupil, fixed = TRUE))
data$eeg_lv1 = as.numeric(sub(",", ".", data$eeg_lv1, fixed = TRUE))
data$eeg_prestim_lv1 = as.numeric(sub(",", ".", data$eeg_prestim_lv1, fixed = TRUE))
data$mri_lv1 = as.numeric(sub(",", ".", data$mri_lv1, fixed = TRUE))
data$mri_lv2 = as.numeric(sub(",", ".", data$mri_lv2, fixed = TRUE))
data$aperiodic = as.numeric(sub(",", ".", data$aperiodic, fixed = TRUE))
data$entropy = as.numeric(sub(",", ".", data$entropy, fixed = TRUE))
data$thetapow = as.numeric(sub(",", ".", data$thetapow, fixed = TRUE))
data$thetadur = as.numeric(sub(",", ".", data$thetadur, fixed = TRUE))
data$alphapow = as.numeric(sub(",", ".", data$alphapow, fixed = TRUE))
data$alphadur = as.numeric(sub(",", ".", data$alphadur, fixed = TRUE))
data$eeg_probe = as.numeric(sub(",", ".", data$eeg_probe, fixed = TRUE))
data$CPPslope = as.numeric(sub(",", ".", data$CPPslope, fixed = TRUE))
data$CPPthreshold = as.numeric(sub(",", ".", data$CPPthreshold, fixed = TRUE))
data$betaslope = as.numeric(sub(",", ".", data$betaslope, fixed = TRUE))
data$betathreshold = as.numeric(sub(",", ".", data$betathreshold, fixed = TRUE))
data$NDTpotential = as.numeric(sub(",", ".", data$NDTpotential, fixed = TRUE))
data$thresh_eeg = as.numeric(sub(",", ".", data$thresh_eeg, fixed = TRUE))
data$thresh_mri = as.numeric(sub(",", ".", data$thresh_mri, fixed = TRUE))
data$ThetaResp = as.numeric(sub(",", ".", data$ThetaResp, fixed = TRUE))

data$acc_eeg = as.numeric(sub(",", ".", data$acc_eeg, fixed = TRUE))
data$acc_mri = as.numeric(sub(",", ".", data$acc_mri, fixed = TRUE))
data$rt_eeg = as.numeric(sub(",", ".", data$rt_eeg, fixed = TRUE))
data$rt_mri = as.numeric(sub(",", ".", data$rt_mri, fixed = TRUE))
```

# select subset of EEG-MRI data (via rowwise deletion); n = 42 subjects

```{r selectMRdata}
data_multi = data[complete.cases(data), ]
```

## perfrom linear models

The logic of assessing bivariate relations is the folowing: We fit a general linear model to the data, including covariates for both ID and Condition. This is indentical in logic to repeated measures correlations, but avoids Simpson's paradox by not assessing mean within-person changes between x and y as a function of load, but inter-individual differences in the magnitude of change (cf., second level slopes) between x and y. This is achieved by accounting for load condition in the model. Note that R is sinterchangeable with regards to the choice of IV and DV.

Type III sum-of-squares (~SPSS) are implemented in drop1 (and modelCompare) outputs reported here http://dwoll.de/rexrepos/posts/anovaSStypes.html#model-comparisons-using-drop1

# drift-ndt (eeg)

```{r, echo = FALSE,  out.width="50%", fig.align = "center"}

y = data$drift_eeg
x = data$ndt_eeg
lmerModel1 = lm(y ~ data$ID + data$cond)
lmerModel2 = lm(y ~ data$ID + data$cond + x)
comparisonOutput = modelCompare(lmerModel1, lmerModel2)

statsResults = vector(mode = "list")
statsResults$type3rmcorr = stats::drop1(lmerModel2, ~., test = "F")
statsResults$SSFactor = statsResults$type3rmcorr$"Sum of Sq"[4]
statsResults$SSresidual = statsResults$type3rmcorr$RSS[1]
statsResults$errordf = lmerModel2$df.residual
statsResults$rmcorrvalue = as.numeric(sign(stats::coef(lmerModel2)["x"]) * 
                                        sqrt(statsResults$SSFactor/(statsResults$SSFactor + statsResults$SSresidual)))
statsResults$rmcorrvalueCI = psych::r.con(statsResults$rmcorrvalue, statsResults$errordf)

```
r(`r statsResults$errordf`) = `r round(statsResults$rmcorrvalue,2)`, 95%CI [`r round(statsResults$rmcorrvalueCI[1],2)`, `r round(statsResults$rmcorrvalueCI[2],2)`], p = `r comparisonOutput$p`

# drift-acc (eeg)

```{r, echo = FALSE,  out.width="50%", fig.align = "center"}

y = data$drift_eeg
x = data$acc_eeg
lmerModel1 = lm(y ~ data$ID + data$cond)
lmerModel2 = lm(y ~ data$ID + data$cond + x)
comparisonOutput = modelCompare(lmerModel1, lmerModel2)

statsResults = vector(mode = "list")
statsResults$type3rmcorr = stats::drop1(lmerModel2, ~., test = "F")
statsResults$SSFactor = statsResults$type3rmcorr$"Sum of Sq"[4]
statsResults$SSresidual = statsResults$type3rmcorr$RSS[1]
statsResults$errordf = lmerModel2$df.residual
statsResults$rmcorrvalue = as.numeric(sign(stats::coef(lmerModel2)["x"]) * 
                                        sqrt(statsResults$SSFactor/(statsResults$SSFactor + statsResults$SSresidual)))
statsResults$rmcorrvalueCI = psych::r.con(statsResults$rmcorrvalue, statsResults$errordf)

```
r(`r statsResults$errordf`) = `r round(statsResults$rmcorrvalue,2)`, 95%CI [`r round(statsResults$rmcorrvalueCI[1],2)`, `r round(statsResults$rmcorrvalueCI[2],2)`], p = `r comparisonOutput$p`

# drift-rt (eeg)

```{r, echo = FALSE,  out.width="50%", fig.align = "center"}

y = data$drift_eeg
x = data$rt_eeg
lmerModel1 = lm(y ~ data$ID + data$cond)
lmerModel2 = lm(y ~ data$ID + data$cond + x)
comparisonOutput = modelCompare(lmerModel1, lmerModel2)

statsResults = vector(mode = "list")
statsResults$type3rmcorr = stats::drop1(lmerModel2, ~., test = "F")
statsResults$SSFactor = statsResults$type3rmcorr$"Sum of Sq"[4]
statsResults$SSresidual = statsResults$type3rmcorr$RSS[1]
statsResults$errordf = lmerModel2$df.residual
statsResults$rmcorrvalue = as.numeric(sign(stats::coef(lmerModel2)["x"]) * 
                                        sqrt(statsResults$SSFactor/(statsResults$SSFactor + statsResults$SSresidual)))
statsResults$rmcorrvalueCI = psych::r.con(statsResults$rmcorrvalue, statsResults$errordf)

```
r(`r statsResults$errordf`) = `r round(statsResults$rmcorrvalue,2)`, 95%CI [`r round(statsResults$rmcorrvalueCI[1],2)`, `r round(statsResults$rmcorrvalueCI[2],2)`], p = `r comparisonOutput$p`

# ndt-acc (eeg)

```{r, echo = FALSE,  out.width="50%", fig.align = "center"}

y = data$ndt_eeg
x = data$rt_eeg
lmerModel1 = lm(y ~ data$ID + data$cond)
lmerModel2 = lm(y ~ data$ID + data$cond + x)
comparisonOutput = modelCompare(lmerModel1, lmerModel2)

statsResults = vector(mode = "list")
statsResults$type3rmcorr = stats::drop1(lmerModel2, ~., test = "F")
statsResults$SSFactor = statsResults$type3rmcorr$"Sum of Sq"[4]
statsResults$SSresidual = statsResults$type3rmcorr$RSS[1]
statsResults$errordf = lmerModel2$df.residual
statsResults$rmcorrvalue = as.numeric(sign(stats::coef(lmerModel2)["x"]) * 
                                        sqrt(statsResults$SSFactor/(statsResults$SSFactor + statsResults$SSresidual)))
statsResults$rmcorrvalueCI = psych::r.con(statsResults$rmcorrvalue, statsResults$errordf)

```
r(`r statsResults$errordf`) = `r round(statsResults$rmcorrvalue,2)`, 95%CI [`r round(statsResults$rmcorrvalueCI[1],2)`, `r round(statsResults$rmcorrvalueCI[2],2)`], p = `r comparisonOutput$p`

# ndt-rt (eeg)

```{r, echo = FALSE,  out.width="50%", fig.align = "center"}

y = data$ndt_eeg
x = data$rt_eeg
lmerModel1 = lm(y ~ data$ID + data$cond)
lmerModel2 = lm(y ~ data$ID + data$cond + x)
comparisonOutput = modelCompare(lmerModel1, lmerModel2)

statsResults = vector(mode = "list")
statsResults$type3rmcorr = stats::drop1(lmerModel2, ~., test = "F")
statsResults$SSFactor = statsResults$type3rmcorr$"Sum of Sq"[4]
statsResults$SSresidual = statsResults$type3rmcorr$RSS[1]
statsResults$errordf = lmerModel2$df.residual
statsResults$rmcorrvalue = as.numeric(sign(stats::coef(lmerModel2)["x"]) * 
                                        sqrt(statsResults$SSFactor/(statsResults$SSFactor + statsResults$SSresidual)))
statsResults$rmcorrvalueCI = psych::r.con(statsResults$rmcorrvalue, statsResults$errordf)

```
r(`r statsResults$errordf`) = `r round(statsResults$rmcorrvalue,2)`, 95%CI [`r round(statsResults$rmcorrvalueCI[1],2)`, `r round(statsResults$rmcorrvalueCI[2],2)`], p = `r comparisonOutput$p`

# drift-ndt (mri)

```{r, echo = FALSE,  out.width="50%", fig.align = "center"}

y = data_multi$drift_mri
x = data_multi$ndt_mri
lmerModel1 = lm(y ~ data_multi$ID + data_multi$cond)
lmerModel2 = lm(y ~ data_multi$ID + data_multi$cond + x)
comparisonOutput = modelCompare(lmerModel1, lmerModel2)

statsResults = vector(mode = "list")
statsResults$type3rmcorr = stats::drop1(lmerModel2, ~., test = "F")
statsResults$SSFactor = statsResults$type3rmcorr$"Sum of Sq"[4]
statsResults$SSresidual = statsResults$type3rmcorr$RSS[1]
statsResults$errordf = lmerModel2$df.residual
statsResults$rmcorrvalue = as.numeric(sign(stats::coef(lmerModel2)["x"]) * 
                                        sqrt(statsResults$SSFactor/(statsResults$SSFactor + statsResults$SSresidual)))
statsResults$rmcorrvalueCI = psych::r.con(statsResults$rmcorrvalue, statsResults$errordf)

```
r(`r statsResults$errordf`) = `r round(statsResults$rmcorrvalue,2)`, 95%CI [`r round(statsResults$rmcorrvalueCI[1],2)`, `r round(statsResults$rmcorrvalueCI[2],2)`], p = `r comparisonOutput$p`

# drift-acc (mri)

```{r, echo = FALSE,  out.width="50%", fig.align = "center"}

y = data_multi$drift_mri
x = data_multi$acc_mri
lmerModel1 = lm(y ~ data_multi$ID + data_multi$cond)
lmerModel2 = lm(y ~ data_multi$ID + data_multi$cond + x)
comparisonOutput = modelCompare(lmerModel1, lmerModel2)

statsResults = vector(mode = "list")
statsResults$type3rmcorr = stats::drop1(lmerModel2, ~., test = "F")
statsResults$SSFactor = statsResults$type3rmcorr$"Sum of Sq"[4]
statsResults$SSresidual = statsResults$type3rmcorr$RSS[1]
statsResults$errordf = lmerModel2$df.residual
statsResults$rmcorrvalue = as.numeric(sign(stats::coef(lmerModel2)["x"]) * 
                                        sqrt(statsResults$SSFactor/(statsResults$SSFactor + statsResults$SSresidual)))
statsResults$rmcorrvalueCI = psych::r.con(statsResults$rmcorrvalue, statsResults$errordf)

```
r(`r statsResults$errordf`) = `r round(statsResults$rmcorrvalue,2)`, 95%CI [`r round(statsResults$rmcorrvalueCI[1],2)`, `r round(statsResults$rmcorrvalueCI[2],2)`], p = `r comparisonOutput$p`


# drift-rt (mri)

```{r, echo = FALSE,  out.width="50%", fig.align = "center"}

y = data_multi$drift_mri
x = data_multi$rt_mri
lmerModel1 = lm(y ~ data_multi$ID + data_multi$cond)
lmerModel2 = lm(y ~ data_multi$ID + data_multi$cond + x)
comparisonOutput = modelCompare(lmerModel1, lmerModel2)

statsResults = vector(mode = "list")
statsResults$type3rmcorr = stats::drop1(lmerModel2, ~., test = "F")
statsResults$SSFactor = statsResults$type3rmcorr$"Sum of Sq"[4]
statsResults$SSresidual = statsResults$type3rmcorr$RSS[1]
statsResults$errordf = lmerModel2$df.residual
statsResults$rmcorrvalue = as.numeric(sign(stats::coef(lmerModel2)["x"]) * 
                                        sqrt(statsResults$SSFactor/(statsResults$SSFactor + statsResults$SSresidual)))
statsResults$rmcorrvalueCI = psych::r.con(statsResults$rmcorrvalue, statsResults$errordf)

```
r(`r statsResults$errordf`) = `r round(statsResults$rmcorrvalue,2)`, 95%CI [`r round(statsResults$rmcorrvalueCI[1],2)`, `r round(statsResults$rmcorrvalueCI[2],2)`], p = `r comparisonOutput$p`


# ndt-acc (mri)

```{r, echo = FALSE,  out.width="50%", fig.align = "center"}

y = data_multi$ndt_mri
x = data_multi$acc_mri
lmerModel1 = lm(y ~ data_multi$ID + data_multi$cond)
lmerModel2 = lm(y ~ data_multi$ID + data_multi$cond + x)
comparisonOutput = modelCompare(lmerModel1, lmerModel2)

statsResults = vector(mode = "list")
statsResults$type3rmcorr = stats::drop1(lmerModel2, ~., test = "F")
statsResults$SSFactor = statsResults$type3rmcorr$"Sum of Sq"[4]
statsResults$SSresidual = statsResults$type3rmcorr$RSS[1]
statsResults$errordf = lmerModel2$df.residual
statsResults$rmcorrvalue = as.numeric(sign(stats::coef(lmerModel2)["x"]) * 
                                        sqrt(statsResults$SSFactor/(statsResults$SSFactor + statsResults$SSresidual)))
statsResults$rmcorrvalueCI = psych::r.con(statsResults$rmcorrvalue, statsResults$errordf)

```
r(`r statsResults$errordf`) = `r round(statsResults$rmcorrvalue,2)`, 95%CI [`r round(statsResults$rmcorrvalueCI[1],2)`, `r round(statsResults$rmcorrvalueCI[2],2)`], p = `r comparisonOutput$p`


# ndt-rt (mri)

```{r, echo = FALSE,  out.width="50%", fig.align = "center"}

y = data_multi$ndt_mri
x = data_multi$rt_mri
lmerModel1 = lm(y ~ data_multi$ID + data_multi$cond)
lmerModel2 = lm(y ~ data_multi$ID + data_multi$cond + x)
comparisonOutput = modelCompare(lmerModel1, lmerModel2)

statsResults = vector(mode = "list")
statsResults$type3rmcorr = stats::drop1(lmerModel2, ~., test = "F")
statsResults$SSFactor = statsResults$type3rmcorr$"Sum of Sq"[4]
statsResults$SSresidual = statsResults$type3rmcorr$RSS[1]
statsResults$errordf = lmerModel2$df.residual
statsResults$rmcorrvalue = as.numeric(sign(stats::coef(lmerModel2)["x"]) * 
                                        sqrt(statsResults$SSFactor/(statsResults$SSFactor + statsResults$SSresidual)))
statsResults$rmcorrvalueCI = psych::r.con(statsResults$rmcorrvalue, statsResults$errordf)

```
r(`r statsResults$errordf`) = `r round(statsResults$rmcorrvalue,2)`, 95%CI [`r round(statsResults$rmcorrvalueCI[1],2)`, `r round(statsResults$rmcorrvalueCI[2],2)`], p = `r comparisonOutput$p`

# Perform linear mixed model for behavior

```{r, echo = FALSE,  out.width="50%", fig.align = "center"}

#install.packages("lmerTest")
library(nlme)
StatsSummary = data.frame(matrix(ncol = 6, nrow = 24)); # initialize data frame for results

lmeModel = lme(acc_eeg ~ as.numeric(cond), random=~1 | ID, correlation=corCompSymm(form=~1|ID), method="ML", data=data)
summaryModel = summary(lmeModel)
lmeModelAnova = anova(lmeModel,type='marginal')
CI = intervals(lmeModel, which = "fixed")

indModel = 1;
StatsSummary[(indModel-1)*2+1,1] = summary(lmeModel)$coefficients$fixed[2] # beta for condition effect
StatsSummary[(indModel-1)*2+1,2] = CI$fixed[2,1] # lowCI for condition
StatsSummary[(indModel-1)*2+1,3] = CI$fixed[2,2] # highCI for condition
StatsSummary[(indModel-1)*2+1,4] = sqrt(summary(lmeModel)$varFix)[2,2] # std for condition effect
StatsSummary[(indModel-1)*2+1,5] = lmeModelAnova$"F-value"[2] # t for condition effect
StatsSummary[(indModel-1)*2+1,6] = lmeModelAnova$"p-value"[2] # p for condition effect

lmeModel = lme(acc_mri ~ as.numeric(cond), random=~1 | ID, correlation=corCompSymm(form=~1|ID), method="ML", data=data_multi)
summaryModel = summary(lmeModel)
lmeModelAnova = anova(lmeModel,type='marginal')
CI = intervals(lmeModel, which = "fixed")

indModel = 2;
StatsSummary[(indModel-1)*2+1,1] = summary(lmeModel)$coefficients$fixed[2] # beta for condition effect
StatsSummary[(indModel-1)*2+1,2] = CI$fixed[2,1] # lowCI for condition
StatsSummary[(indModel-1)*2+1,3] = CI$fixed[2,2] # highCI for condition
StatsSummary[(indModel-1)*2+1,4] = sqrt(summary(lmeModel)$varFix)[2,2] # std for condition effect
StatsSummary[(indModel-1)*2+1,5] = lmeModelAnova$"F-value"[2] # t for condition effect
StatsSummary[(indModel-1)*2+1,6] = lmeModelAnova$"p-value"[2] # p for condition effect

lmeModel = lme(rt_eeg ~ as.numeric(cond), random=~1 | ID, correlation=corCompSymm(form=~1|ID), method="ML", data=data)
summaryModel = summary(lmeModel)
lmeModelAnova = anova(lmeModel,type='marginal')
CI = intervals(lmeModel, which = "fixed")

indModel = 3;
StatsSummary[(indModel-1)*2+1,1] = summary(lmeModel)$coefficients$fixed[2] # beta for condition effect
StatsSummary[(indModel-1)*2+1,2] = CI$fixed[2,1] # lowCI for condition
StatsSummary[(indModel-1)*2+1,3] = CI$fixed[2,2] # highCI for condition
StatsSummary[(indModel-1)*2+1,4] = sqrt(summary(lmeModel)$varFix)[2,2] # std for condition effect
StatsSummary[(indModel-1)*2+1,5] = lmeModelAnova$"F-value"[2] # t for condition effect
StatsSummary[(indModel-1)*2+1,6] = lmeModelAnova$"p-value"[2] # p for condition effect

lmeModel = lme(rt_mri ~ as.numeric(cond), random=~1 | ID, correlation=corCompSymm(form=~1|ID), method="ML", data=data_multi)
summaryModel = summary(lmeModel)
lmeModelAnova = anova(lmeModel,type='marginal')
CI = intervals(lmeModel, which = "fixed")

indModel = 4;
StatsSummary[(indModel-1)*2+1,1] = summary(lmeModel)$coefficients$fixed[2] # beta for condition effect
StatsSummary[(indModel-1)*2+1,2] = CI$fixed[2,1] # lowCI for condition
StatsSummary[(indModel-1)*2+1,3] = CI$fixed[2,2] # highCI for condition
StatsSummary[(indModel-1)*2+1,4] = sqrt(summary(lmeModel)$varFix)[2,2] # std for condition effect
StatsSummary[(indModel-1)*2+1,5] = lmeModelAnova$"F-value"[2] # t for condition effect
StatsSummary[(indModel-1)*2+1,6] = lmeModelAnova$"p-value"[2] # p for condition effect

```
r(`r statsResults$errordf`) = `r round(statsResults$rmcorrvalue,2)`, 95%CI [`r round(statsResults$rmcorrvalueCI[1],2)`, `r round(statsResults$rmcorrvalueCI[2],2)`], p = `r comparisonOutput$p`

data$acc_eeg = as.numeric(sub(",", ".", data$acc_eeg, fixed = TRUE))
data$acc_mri = as.numeric(sub(",", ".", data$acc_mri, fixed = TRUE))
data$rt_eeg = as.numeric(sub(",", ".", data$rt_eeg, fixed = TRUE))
data$rt_mri = as.numeric(sub(",", ".", data$rt_mri, fixed = TRUE))
