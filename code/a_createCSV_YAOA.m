% create a summary file table with all relevant individual data

% path management
currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..'))

pn.root = pwd;
pn.data_summary = fullfile(pn.root, 'data');
pn.pupil = fullfile(pn.root, '..', 'stsw_eye', 'pupil');
pn.stroop = fullfile(pn.root, '..', 'stsw_beh_stroop');
pn.stroop_cpp = fullfile(pn.root, '..', 'stsw_eeg_stroop', 'erp');
pn.aperiodic = fullfile(pn.root, '..', 'stsw_eeg_task', 'aperiodic');
pn.fooof = fullfile(pn.root, '..', 'stsw_eeg_task', 'aperiodic_fooof');
pn.mse = fullfile(pn.root, '..', 'stsw_eeg_task', 'mse');
pn.tfr = fullfile(pn.root, '..', 'stsw_eeg_task', 'tfr');
pn.pls_excit = fullfile(pn.root, '..', 'stsw_eeg_task', 'pls_aperiodic');
pn.ssvep = fullfile(pn.root, '..', 'stsw_eeg_task', 'ssvep');
pn.mri = fullfile(pn.root, '..', 'stsw_fmri_task', 'pls_mean');
pn.decoding = fullfile(pn.root, '..', 'stsw_fmri_task', 'decoding');

% add tools
addpath(fullfile(pn.root, 'tools'));

%% setup

StateSwitchDynIDs = {1117;1118;1120;1124;1125;1126;1131;1132;1135;1136;1138;1144;1151;...
    1158;1160;1163;1164;1167;1169;1172;1173;1178;1182;1213;1214;1215;1216;...
    1219;1221;1223;1227;1228;1233;1234;1237;1239;1240;1243;1245;1247;1250;...
    1252;1257;1258;1261;1265;1266;1268;1270;1276;1281;2104;2107;2108;2112;...
    2118;2120;2121;2123;2124;2125;2129;2130;2131;2132;2133;2134;2135;2139;...
    2140;2142;2145;2147;2149;2157;2160;2201;2202;2203;2205;2206;2209;2210;...
    2211;2213;2214;2215;2216;2217;2219;2222;2224;2226;2227;2236;2237;2238;...
    2241;2244;2246;2248;2250;2251;2252;2253;2254;2255;2258;2261};

N_total = numel(StateSwitchDynIDs);

StateSwitchDynIDs = cellfun(@num2str, StateSwitchDynIDs, 'un', 0);

STSWD_summary.IDs = StateSwitchDynIDs;

idx_YA = cellfun(@str2num, StateSwitchDynIDs)<2000;
idx_OA = cellfun(@str2num, StateSwitchDynIDs)>2000;

%% median RT, mean Acc (session-specific + PCA across sessions)

curData = fullfile(pn.root, '..', 'stsw_beh_task', 'behavior', 'data', 'SummaryData_N102.mat');
load(curData)

tmp_IDidx = ismember(StateSwitchDynIDs, IDs_all);

STSWD_summary.behav.EEGAcc(tmp_IDidx,:) = squeeze(nanmean(SummaryData.EEG.Acc_mean,2));
STSWD_summary.behav.EEGRT(tmp_IDidx,:) = squeeze(nanmean(SummaryData.EEG.RTs_md,2));
STSWD_summary.behav.EEGRT_std(tmp_IDidx,:) = squeeze(nanmean(SummaryData.EEG.RTs_std,2));
STSWD_summary.behav.MRIAcc(tmp_IDidx,:) = squeeze(nanmean(SummaryData.MRI.Acc_mean,2));
STSWD_summary.behav.MRIRT(tmp_IDidx,:) = squeeze(nanmean(SummaryData.MRI.RTs_md,2));
STSWD_summary.behav.MRIRT(tmp_IDidx,:) = squeeze(nanmean(SummaryData.MRI.RTs_md,2));
STSWD_summary.behav.MRIRT_std(tmp_IDidx,:) = squeeze(nanmean(SummaryData.MRI.RTs_std,2));

% calculate PCA across sessions 
for indCond = 1:4
    [coeff, score] = pca([STSWD_summary.behav.MRIRT(:,indCond), STSWD_summary.behav.EEGRT(:,indCond)]);
    STSWD_summary.behav.RT_PCA(:,indCond) = score(:,1); % get first principle component across sessions
    %figure; scatter(STSWD_summary.behav.MRIRT(:,indCond), score(:,1))
    [coeff, score] = pca([STSWD_summary.behav.MRIAcc(:,indCond), STSWD_summary.behav.EEGAcc(:,indCond)]);
    STSWD_summary.behav.Acc_PCA(:,indCond) = score(:,1); % get first principle component across sessions
end

% linear change
X = [1 1; 1 2; 1 3; 1 4];
b=X\STSWD_summary.behav.EEGAcc'; STSWD_summary.behav.EEGAcc_linear(:,1) = b(2,:);
b=X\STSWD_summary.behav.EEGRT'; STSWD_summary.behav.EEGRT_linear(:,1) = b(2,:);
b=X\STSWD_summary.behav.MRIAcc'; STSWD_summary.behav.MRIAcc_linear(:,1) = b(2,:);
b=X\STSWD_summary.behav.MRIRT'; STSWD_summary.behav.MRIRT_linear(:,1) = b(2,:);

clear tmp_* SummaryData

%% HDDM (drift, threshold, ndt)

% encode YAs
curData = fullfile(pn.root, '..', 'stsw_beh_task', 'hddm', 'data', 'HDDM_summary_YA_vat.mat');
load(curData)

ageIdx{1} = cellfun(@str2num, HDDM_summary.IDs, 'un', 1)<2000;
ageIdx{2} = cellfun(@str2num, HDDM_summary.IDs, 'un', 1)>2000;

tmp_IDidx = ismember(StateSwitchDynIDs, HDDM_summary.IDs(ageIdx{1}));
tmp_IDidx_ddm = ismember(HDDM_summary.IDs, StateSwitchDynIDs(tmp_IDidx));

STSWD_summary.HDDM_vat = [];
STSWD_summary.HDDM_vat.thresholdMRI(tmp_IDidx,1:4) = HDDM_summary.thresholdMRI(tmp_IDidx_ddm,:);
STSWD_summary.HDDM_vat.nondecisionMRI(tmp_IDidx,1:4) = HDDM_summary.nondecisionMRI(tmp_IDidx_ddm,:);
STSWD_summary.HDDM_vat.driftMRI(tmp_IDidx,1:4) = HDDM_summary.driftMRI(tmp_IDidx_ddm,:);
STSWD_summary.HDDM_vat.thresholdEEG(tmp_IDidx,1:4) = HDDM_summary.thresholdEEG(tmp_IDidx_ddm,:);
STSWD_summary.HDDM_vat.nondecisionEEG(tmp_IDidx,1:4) = HDDM_summary.nondecisionEEG(tmp_IDidx_ddm,:);
STSWD_summary.HDDM_vat.driftEEG(tmp_IDidx,1:4) = HDDM_summary.driftEEG(tmp_IDidx_ddm,:);

% encode OAs
curData = fullfile(pn.root, '..', 'stsw_beh_task', 'hddm', 'data', 'HDDM_summary_OA_vat.mat');
load(curData)

tmp_IDidx = ismember(StateSwitchDynIDs, HDDM_summary.IDs(ageIdx{2}));
tmp_IDidx_ddm = ismember(HDDM_summary.IDs, StateSwitchDynIDs(tmp_IDidx));

STSWD_summary.HDDM_vat.thresholdMRI(tmp_IDidx,1:4) = HDDM_summary.thresholdMRI(tmp_IDidx_ddm,:);
STSWD_summary.HDDM_vat.nondecisionMRI(tmp_IDidx,1:4) = HDDM_summary.nondecisionMRI(tmp_IDidx_ddm,:);
STSWD_summary.HDDM_vat.driftMRI(tmp_IDidx,1:4) = HDDM_summary.driftMRI(tmp_IDidx_ddm,:);
STSWD_summary.HDDM_vat.thresholdEEG(tmp_IDidx,1:4) = HDDM_summary.thresholdEEG(tmp_IDidx_ddm,:);
STSWD_summary.HDDM_vat.nondecisionEEG(tmp_IDidx,1:4) = HDDM_summary.nondecisionEEG(tmp_IDidx_ddm,:);
STSWD_summary.HDDM_vat.driftEEG(tmp_IDidx,1:4) = HDDM_summary.driftEEG(tmp_IDidx_ddm,:);

STSWD_summary.HDDM_vat.thresholdEEGMRI = squeeze(nanmean(cat(3,STSWD_summary.HDDM_vat.thresholdEEG, STSWD_summary.HDDM_vat.thresholdMRI),3));
STSWD_summary.HDDM_vat.nondecisionEEGMRI = squeeze(nanmean(cat(3,STSWD_summary.HDDM_vat.nondecisionEEG, STSWD_summary.HDDM_vat.nondecisionMRI),3));
STSWD_summary.HDDM_vat.driftEEGMRI = squeeze(nanmean(cat(3,STSWD_summary.HDDM_vat.driftEEG, STSWD_summary.HDDM_vat.driftMRI),3));

% add slopes
X = [1 1; 1 2; 1 3; 1 4]; 
b=X\STSWD_summary.HDDM_vat.thresholdMRI'; STSWD_summary.HDDM_vat.thresholdMRI_linear(:,1) = b(2,:);
b=X\STSWD_summary.HDDM_vat.nondecisionMRI'; STSWD_summary.HDDM_vat.nondecisionMRI_linear(:,1) = b(2,:);
b=X\STSWD_summary.HDDM_vat.driftMRI'; STSWD_summary.HDDM_vat.driftMRI_linear(:,1) = b(2,:);
b=X\STSWD_summary.HDDM_vat.thresholdEEG'; STSWD_summary.HDDM_vat.thresholdEEG_linear(:,1) = b(2,:);
b=X\STSWD_summary.HDDM_vat.nondecisionEEG'; STSWD_summary.HDDM_vat.nondecisionEEG_linear(:,1) = b(2,:);
b=X\STSWD_summary.HDDM_vat.driftEEG'; STSWD_summary.HDDM_vat.driftEEG_linear(:,1) = b(2,:);

b=X\STSWD_summary.HDDM_vat.thresholdEEGMRI'; STSWD_summary.HDDM_vat.thresholdEEGMRI_linear(:,1) = b(2,:);
b=X\STSWD_summary.HDDM_vat.nondecisionEEGMRI'; STSWD_summary.HDDM_vat.nondecisionEEGMRI_linear(:,1) = b(2,:);
b=X\STSWD_summary.HDDM_vat.driftEEGMRI'; STSWD_summary.HDDM_vat.driftEEGMRI_linear(:,1) = b(2,:);

STSWD_summary.HDDM_vat.thresholdEEGMRI_linear_win(idx_YA) = winsorize_outlier(STSWD_summary.HDDM_vat.thresholdEEGMRI_linear(idx_YA));
STSWD_summary.HDDM_vat.nondecisionEEGMRI_linear_win(idx_YA) = winsorize_outlier(STSWD_summary.HDDM_vat.nondecisionEEGMRI_linear(idx_YA));
STSWD_summary.HDDM_vat.driftEEGMRI_linear_win(idx_YA) = winsorize_outlier(STSWD_summary.HDDM_vat.driftEEGMRI_linear(idx_YA));

STSWD_summary.HDDM_vat.thresholdEEGMRI_linear_win(idx_OA) = winsorize_outlier(STSWD_summary.HDDM_vat.thresholdEEGMRI_linear(idx_OA));
STSWD_summary.HDDM_vat.nondecisionEEGMRI_linear_win(idx_OA) = winsorize_outlier(STSWD_summary.HDDM_vat.nondecisionEEGMRI_linear(idx_OA));
STSWD_summary.HDDM_vat.driftEEGMRI_linear_win(idx_OA) = winsorize_outlier(STSWD_summary.HDDM_vat.driftEEGMRI_linear(idx_OA));

%% HDDM (only v&t)

% % encode YAs
% 
% curData = fullfile(pn.root, '..', 'stsw_beh_task', 'hddm', 'data', 'HDDM_summary_YA_vt.mat');
% load(curData)
% 
% ageIdx{1} = cellfun(@str2num, HDDM_summary.IDs, 'un', 1)<2000;
% ageIdx{2} = cellfun(@str2num, HDDM_summary.IDs, 'un', 1)>2000;
% 
% tmp_IDidx = ismember(StateSwitchDynIDs, HDDM_summary.IDs(ageIdx{1}));
% tmp_IDidx_ddm = ismember(HDDM_summary.IDs, StateSwitchDynIDs(tmp_IDidx));
% 
% STSWD_summary.HDDM_vt = [];
% STSWD_summary.HDDM_vt.thresholdMRI(tmp_IDidx,1:4) = HDDM_summary.thresholdMRI(tmp_IDidx_ddm,:);
% STSWD_summary.HDDM_vt.nondecisionMRI(tmp_IDidx,1:4) = HDDM_summary.nondecisionMRI(tmp_IDidx_ddm,:);
% STSWD_summary.HDDM_vt.driftMRI(tmp_IDidx,1:4) = HDDM_summary.driftMRI(tmp_IDidx_ddm,:);
% STSWD_summary.HDDM_vt.thresholdEEG(tmp_IDidx,1:4) = HDDM_summary.thresholdEEG(tmp_IDidx_ddm,:);
% STSWD_summary.HDDM_vt.nondecisionEEG(tmp_IDidx,1:4) = HDDM_summary.nondecisionEEG(tmp_IDidx_ddm,:);
% STSWD_summary.HDDM_vt.driftEEG(tmp_IDidx,1:4) = HDDM_summary.driftEEG(tmp_IDidx_ddm,:);
% 
% % encode OAs
% curData = fullfile(pn.root, '..', 'stsw_beh_task', 'hddm', 'data', 'HDDM_summary_OA_vt.mat');
% load(curData)
% 
% tmp_IDidx = ismember(StateSwitchDynIDs, HDDM_summary.IDs(ageIdx{2}));
% tmp_IDidx_ddm = ismember(HDDM_summary.IDs, StateSwitchDynIDs(tmp_IDidx));
% 
% STSWD_summary.HDDM_vt.thresholdMRI(tmp_IDidx,1:4) = HDDM_summary.thresholdMRI(tmp_IDidx_ddm,:);
% STSWD_summary.HDDM_vt.nondecisionMRI(tmp_IDidx,1:4) = HDDM_summary.nondecisionMRI(tmp_IDidx_ddm,:);
% STSWD_summary.HDDM_vt.driftMRI(tmp_IDidx,1:4) = HDDM_summary.driftMRI(tmp_IDidx_ddm,:);
% STSWD_summary.HDDM_vt.thresholdEEG(tmp_IDidx,1:4) = HDDM_summary.thresholdEEG(tmp_IDidx_ddm,:);
% STSWD_summary.HDDM_vt.nondecisionEEG(tmp_IDidx,1:4) = HDDM_summary.nondecisionEEG(tmp_IDidx_ddm,:);
% STSWD_summary.HDDM_vt.driftEEG(tmp_IDidx,1:4) = HDDM_summary.driftEEG(tmp_IDidx_ddm,:);
% 
% % add slopes
% X = [1 1; 1 2; 1 3; 1 4]; 
% b=X\STSWD_summary.HDDM_vt.thresholdMRI'; STSWD_summary.HDDM_vt.thresholdMRI_linear(:,1) = b(2,:);
% b=X\STSWD_summary.HDDM_vt.nondecisionMRI'; STSWD_summary.HDDM_vt.nondecisionMRI_linear(:,1) = b(2,:);
% b=X\STSWD_summary.HDDM_vt.driftMRI'; STSWD_summary.HDDM_vt.driftMRI_linear(:,1) = b(2,:);
% b=X\STSWD_summary.HDDM_vt.thresholdEEG'; STSWD_summary.HDDM_vt.thresholdEEG_linear(:,1) = b(2,:);
% b=X\STSWD_summary.HDDM_vt.nondecisionEEG'; STSWD_summary.HDDM_vt.nondecisionEEG_linear(:,1) = b(2,:);
% b=X\STSWD_summary.HDDM_vt.driftEEG'; STSWD_summary.HDDM_vt.driftEEG_linear(:,1) = b(2,:);

%% add CPP slopes

curData = fullfile(pn.root, '..', 'stsw_eeg_task', 'cpp', 'data', 'Z_CPPslopes_YA.mat');
tmp_ya = load(curData, 'CPPslopes');
curData = fullfile(pn.root, '..', 'stsw_eeg_task', 'cpp', 'data', 'Z_CPPslopes_OA.mat');
tmp_oa = load(curData, 'CPPslopes');

CPPslopes.data = cat(1,tmp_ya.CPPslopes.data, tmp_oa.CPPslopes.data);
CPPslopes.IDs = cat(1,tmp_ya.CPPslopes.IDs, tmp_oa.CPPslopes.IDs);

tmp_IDidx = ismember(StateSwitchDynIDs, CPPslopes.IDs);

fieldname = 'CPPslopes';
STSWD_summary.(fieldname).data = NaN(N_total,4);
STSWD_summary.(fieldname).data(tmp_IDidx,:) = CPPslopes.data;
% linear change in brainscore
X = [1 1; 1 2; 1 3; 1 4]; b=X\STSWD_summary.(fieldname).data';
STSWD_summary.(fieldname).linear = NaN(N_total,1);
STSWD_summary.(fieldname).linear(:,1) = b(2,:);
STSWD_summary.(fieldname).linear_win(idx_YA) = winsorize_outlier(STSWD_summary.(fieldname).linear(idx_YA));
STSWD_summary.(fieldname).linear_win(idx_OA) = winsorize_outlier(STSWD_summary.(fieldname).linear(idx_OA));
for indCond = 1:4
    STSWD_summary.(fieldname).data(idx_YA,indCond) = winsorize_outlier(STSWD_summary.(fieldname).data(idx_YA,indCond));
    STSWD_summary.(fieldname).data(idx_OA,indCond) = winsorize_outlier(STSWD_summary.(fieldname).data(idx_OA,indCond));
end

%% add CPP threshold

curData = fullfile(pn.root, '..', 'stsw_eeg_task', 'cpp', 'data', 'Z_CPPthreshold_YA.mat');
tmp_ya = load(curData, 'CPPthreshold');
curData = fullfile(pn.root, '..', 'stsw_eeg_task', 'cpp', 'data', 'Z_CPPthreshold_OA.mat');
tmp_oa = load(curData, 'CPPthreshold');

CPPthreshold.data = cat(1,tmp_ya.CPPthreshold.data, tmp_oa.CPPthreshold.data);
CPPthreshold.IDs = cat(1,tmp_ya.CPPthreshold.IDs, tmp_oa.CPPthreshold.IDs);

tmp_IDidx = ismember(StateSwitchDynIDs, CPPthreshold.IDs);

fieldname = 'CPPthreshold';
STSWD_summary.(fieldname).data = NaN(N_total,4);
STSWD_summary.(fieldname).data(tmp_IDidx,:) = CPPthreshold.data;
% linear change in brainscore
X = [1 1; 1 2; 1 3; 1 4]; b=X\STSWD_summary.(fieldname).data';
STSWD_summary.(fieldname).linear = NaN(N_total,1);
STSWD_summary.(fieldname).linear(:,1) = b(2,:);
STSWD_summary.(fieldname).linear_win(idx_YA) = winsorize_outlier(STSWD_summary.(fieldname).linear(idx_YA));
STSWD_summary.(fieldname).linear_win(idx_OA) = winsorize_outlier(STSWD_summary.(fieldname).linear(idx_OA));
for indCond = 1:4
    STSWD_summary.(fieldname).data(idx_YA,indCond) = winsorize_outlier(STSWD_summary.(fieldname).data(idx_YA,indCond));
    STSWD_summary.(fieldname).data(idx_OA,indCond) = winsorize_outlier(STSWD_summary.(fieldname).data(idx_OA,indCond));
end
%% add beta slopes

curData = fullfile(pn.root, '..', 'stsw_eeg_task', 'tfr_periresponse', 'data', 'BetaSlope_YA.mat');
tmp_ya = load(curData, 'BetaSlope');
curData = fullfile(pn.root, '..', 'stsw_eeg_task', 'tfr_periresponse', 'data', 'BetaSlope_OA.mat');
tmp_oa = load(curData, 'BetaSlope');

BetaSlope.data = cat(1,tmp_ya.BetaSlope.data, tmp_oa.BetaSlope.data);
BetaSlope.IDs = cat(1,tmp_ya.BetaSlope.IDs, tmp_oa.BetaSlope.IDs);

tmp_IDidx = ismember(StateSwitchDynIDs, BetaSlope.IDs);

fieldname = 'BetaSlope';
STSWD_summary.(fieldname).data = NaN(N_total,4);
STSWD_summary.(fieldname).data(tmp_IDidx,:) = BetaSlope.data;
% linear change in brainscore
X = [1 1; 1 2; 1 3; 1 4]; b=X\STSWD_summary.(fieldname).data';
STSWD_summary.(fieldname).linear = NaN(N_total,1);
STSWD_summary.(fieldname).linear(:,1) = b(2,:);
STSWD_summary.(fieldname).linear_win(idx_YA) = winsorize_outlier(STSWD_summary.(fieldname).linear(idx_YA));
STSWD_summary.(fieldname).linear_win(idx_OA) = winsorize_outlier(STSWD_summary.(fieldname).linear(idx_OA));
for indCond = 1:4
    STSWD_summary.(fieldname).data(idx_YA,indCond) = winsorize_outlier(STSWD_summary.(fieldname).data(idx_YA,indCond));
    STSWD_summary.(fieldname).data(idx_OA,indCond) = winsorize_outlier(STSWD_summary.(fieldname).data(idx_OA,indCond));
end

%% add beta threshold

curData = fullfile(pn.root, '..', 'stsw_eeg_task', 'tfr_periresponse', 'data', 'BetaThreshold_YA.mat');
tmp_ya = load(curData, 'BetaThreshold');
curData = fullfile(pn.root, '..', 'stsw_eeg_task', 'tfr_periresponse', 'data', 'BetaThreshold_OA.mat');
tmp_oa = load(curData, 'BetaThreshold');

BetaThreshold.data = cat(1,tmp_ya.BetaThreshold.data, tmp_oa.BetaThreshold.data);
BetaThreshold.IDs = cat(1,tmp_ya.BetaThreshold.IDs, tmp_oa.BetaThreshold.IDs);

tmp_IDidx = ismember(StateSwitchDynIDs, BetaThreshold.IDs);

fieldname = 'BetaThreshold';
STSWD_summary.(fieldname).data = NaN(N_total,4);
STSWD_summary.(fieldname).data(tmp_IDidx,:) = BetaThreshold.data;
% linear change in brainscore
X = [1 1; 1 2; 1 3; 1 4]; b=X\STSWD_summary.(fieldname).data';
STSWD_summary.(fieldname).linear = NaN(N_total,1);
STSWD_summary.(fieldname).linear(:,1) = b(2,:);
STSWD_summary.(fieldname).linear_win(idx_YA) = winsorize_outlier(STSWD_summary.(fieldname).linear(idx_YA));
STSWD_summary.(fieldname).linear_win(idx_OA) = winsorize_outlier(STSWD_summary.(fieldname).linear(idx_OA));
for indCond = 1:4
    STSWD_summary.(fieldname).data(idx_YA,indCond) = winsorize_outlier(STSWD_summary.(fieldname).data(idx_YA,indCond));
    STSWD_summary.(fieldname).data(idx_OA,indCond) = winsorize_outlier(STSWD_summary.(fieldname).data(idx_OA,indCond));
end

%% pupil dilation: stim only

curData = fullfile(pn.pupil, 'data', 'E1_LV1.mat');
load(curData)

tmp_IDidx = ismember(StateSwitchDynIDs, LV1.IDs);

fieldname = 'pupil_LV1';
STSWD_summary.(fieldname).data = NaN(N_total,4);
STSWD_summary.(fieldname).data(tmp_IDidx,:) = LV1.data;
% linear change in brainscore
X = [1 1; 1 2; 1 3; 1 4]; b=X\STSWD_summary.(fieldname).data';
STSWD_summary.(fieldname).linear = NaN(N_total,1);
STSWD_summary.(fieldname).linear(:,1) = b(2,:);
STSWD_summary.(fieldname).linear_win(idx_YA) = winsorize_outlier(STSWD_summary.(fieldname).linear(idx_YA));
STSWD_summary.(fieldname).linear_win(idx_OA) = winsorize_outlier(STSWD_summary.(fieldname).linear(idx_OA));
for indCond = 1:4
    STSWD_summary.(fieldname).data(idx_YA,indCond) = winsorize_outlier(STSWD_summary.(fieldname).data(idx_YA,indCond));
    STSWD_summary.(fieldname).data(idx_OA,indCond) = winsorize_outlier(STSWD_summary.(fieldname).data(idx_OA,indCond));
end

%figure; scatter(STSWD_summary.(fieldname).linear_win, STSWD_summary.(fieldname).linear, 'filled')

%% load fooof estimates

curData = fullfile(pn.fooof, 'data', 'foof_exponents.mat');
load(curData, 'fooof_exponents_rest', 'fooof_exponents');

STSWD_summary.fooof.OneFslope_rest.data = fooof_exponents_rest(:,:,1:60);
STSWD_summary.fooof.OneFslope.data = fooof_exponents(:,:,1:60);

for ind_chan = 1:60
    X = [1 1; 1 2];
    b=X\squeeze(STSWD_summary.fooof.OneFslope_rest.data(:,:,ind_chan))';
    STSWD_summary.fooof.OneFslope_rest.linear(:,ind_chan) = b(2,:);
    STSWD_summary.fooof.OneFslope_rest.linear_win(idx_YA,ind_chan) = ...
        winsorize_outlier(STSWD_summary.fooof.OneFslope_rest.linear(idx_YA,ind_chan));
    STSWD_summary.fooof.OneFslope_rest.linear_win(idx_OA,ind_chan) = ...
        winsorize_outlier(STSWD_summary.fooof.OneFslope_rest.linear(idx_OA,ind_chan));
    
    X = [1 1; 1 2; 1 3; 1 4];
    b=X\squeeze(STSWD_summary.fooof.OneFslope.data(:,:,ind_chan))';
    STSWD_summary.fooof.OneFslope.linear(:,ind_chan) = b(2,:);
    STSWD_summary.fooof.OneFslope.linear_win(idx_YA,ind_chan) = ...
        winsorize_outlier(STSWD_summary.fooof.OneFslope.linear(idx_YA,ind_chan));
    STSWD_summary.fooof.OneFslope.linear_win(idx_OA,ind_chan) = ...
        winsorize_outlier(STSWD_summary.fooof.OneFslope.linear(idx_OA,ind_chan));
end
STSWD_summary.fooof.OneFslope.linear_win(idx_YA) = winsorize_outlier(STSWD_summary.fooof.OneFslope.linear(idx_YA));
STSWD_summary.fooof.OneFslope.linear_win(idx_OA) = winsorize_outlier(STSWD_summary.fooof.OneFslope.linear(idx_OA));

% fooof brainscore

load(fullfile(pn.fooof, 'data', 'g01_LV1.mat'), 'LV1');

tmp_IDidx = ismember(StateSwitchDynIDs, LV1.IDs);

fieldname = 'fooof_LV1';
STSWD_summary.(fieldname).data = NaN(N_total,4);
STSWD_summary.(fieldname).data(tmp_IDidx,:) = LV1.data;
% linear change in brainscore
X = [1 1; 1 2; 1 3; 1 4]; b=X\STSWD_summary.(fieldname).data';
STSWD_summary.(fieldname).linear = NaN(N_total,1);
STSWD_summary.(fieldname).linear(:,1) = b(2,:);
STSWD_summary.(fieldname).linear_win(idx_YA) = winsorize_outlier(STSWD_summary.(fieldname).linear(idx_YA));
STSWD_summary.(fieldname).linear_win(idx_OA) = winsorize_outlier(STSWD_summary.(fieldname).linear(idx_OA));
for indCond = 1:4
    STSWD_summary.(fieldname).data(idx_YA,indCond) = winsorize_outlier(STSWD_summary.(fieldname).data(idx_YA,indCond));
    STSWD_summary.(fieldname).data(idx_OA,indCond) = winsorize_outlier(STSWD_summary.(fieldname).data(idx_OA,indCond));
end
%figure; scatter(STSWD_summary.(fieldname).linear_win, STSWD_summary.(fieldname).linear, 'filled')

%% fine-scale entropy

curData = fullfile(pn.mse, 'data', 'E_statistics', 'eO1_mse_yaoa.mat');
load(curData, 'individualMSE');

tmp_IDidx = ismember(StateSwitchDynIDs, individualMSE.IDs);
STSWD_summary.SE.data(tmp_IDidx,:) = individualMSE.data;
STSWD_summary.SE.data_slope(tmp_IDidx,1) = individualMSE.data_slope;
STSWD_summary.SE.data_slope_win(tmp_IDidx,1) = individualMSE.data_slope_win;

% sampen brainscore

load(fullfile(pn.mse, 'data', 'h01_LV1.mat'), 'LV1');

tmp_IDidx = ismember(StateSwitchDynIDs, LV1.IDs);

fieldname = 'SE_LV1';
STSWD_summary.(fieldname).data = NaN(N_total,4);
STSWD_summary.(fieldname).data(tmp_IDidx,:) = LV1.data;
% linear change in brainscore
X = [1 1; 1 2; 1 3; 1 4]; b=X\STSWD_summary.(fieldname).data';
STSWD_summary.(fieldname).linear = NaN(N_total,1);
STSWD_summary.(fieldname).linear(:,1) = b(2,:);
STSWD_summary.(fieldname).linear_win(idx_YA) = winsorize_outlier(STSWD_summary.(fieldname).linear(idx_YA));
STSWD_summary.(fieldname).linear_win(idx_OA) = winsorize_outlier(STSWD_summary.(fieldname).linear(idx_OA));
for indCond = 1:4
    STSWD_summary.(fieldname).data(idx_YA,indCond) = winsorize_outlier(STSWD_summary.(fieldname).data(idx_YA,indCond));
    STSWD_summary.(fieldname).data(idx_OA,indCond) = winsorize_outlier(STSWD_summary.(fieldname).data(idx_OA,indCond));
end
%figure; scatter(STSWD_summary.(fieldname).linear_win, STSWD_summary.(fieldname).linear, 'filled')

%% EEG-based brainscores (two-group models)

% theta

load(fullfile(pn.tfr, 'data', 'c02_LV1.mat'), 'LV1');
tmp_IDidx = ismember(StateSwitchDynIDs, LV1.IDs);
fieldname = 'tfr_theta_LV1';
STSWD_summary.(fieldname).data = NaN(N_total,4);
STSWD_summary.(fieldname).data(tmp_IDidx,:) = LV1.data;
X = [1 1; 1 2; 1 3; 1 4]; b=X\STSWD_summary.(fieldname).data';
STSWD_summary.(fieldname).linear = NaN(N_total,1);
STSWD_summary.(fieldname).linear(:,1) = b(2,:);
STSWD_summary.(fieldname).linear_win(idx_YA) = winsorize_outlier(STSWD_summary.(fieldname).linear(idx_YA));
STSWD_summary.(fieldname).linear_win(idx_OA) = winsorize_outlier(STSWD_summary.(fieldname).linear(idx_OA));
for indCond = 1:4
    STSWD_summary.(fieldname).data(idx_YA,indCond) = winsorize_outlier(STSWD_summary.(fieldname).data(idx_YA,indCond));
    STSWD_summary.(fieldname).data(idx_OA,indCond) = winsorize_outlier(STSWD_summary.(fieldname).data(idx_OA,indCond));
end

% alpha

load(fullfile(pn.tfr, 'data', 'c01_LV1.mat'), 'LV1');
tmp_IDidx = ismember(StateSwitchDynIDs, LV1.IDs);
fieldname = 'tfr_alpha_LV1';
STSWD_summary.(fieldname).data = NaN(N_total,4);
STSWD_summary.(fieldname).data(tmp_IDidx,:) = LV1.data;
X = [1 1; 1 2; 1 3; 1 4]; b=X\STSWD_summary.(fieldname).data';
STSWD_summary.(fieldname).linear = NaN(N_total,1);
STSWD_summary.(fieldname).linear(:,1) = b(2,:);
STSWD_summary.(fieldname).linear_win(idx_YA) = winsorize_outlier(STSWD_summary.(fieldname).linear(idx_YA));
STSWD_summary.(fieldname).linear_win(idx_OA) = winsorize_outlier(STSWD_summary.(fieldname).linear(idx_OA));
for indCond = 1:4
    STSWD_summary.(fieldname).data(idx_YA,indCond) = winsorize_outlier(STSWD_summary.(fieldname).data(idx_YA,indCond));
    STSWD_summary.(fieldname).data(idx_OA,indCond) = winsorize_outlier(STSWD_summary.(fieldname).data(idx_OA,indCond));
end

% gamma

load(fullfile(pn.tfr, 'data', 'c03_LV1.mat'), 'LV1');
tmp_IDidx = ismember(StateSwitchDynIDs, LV1.IDs);
fieldname = 'tfr_gamma_LV1';
STSWD_summary.(fieldname).data = NaN(N_total,4);
STSWD_summary.(fieldname).data(tmp_IDidx,:) = LV1.data;
X = [1 1; 1 2; 1 3; 1 4]; b=X\STSWD_summary.(fieldname).data';
STSWD_summary.(fieldname).linear = NaN(N_total,1);
STSWD_summary.(fieldname).linear(:,1) = b(2,:);
STSWD_summary.(fieldname).linear_win(idx_YA) = winsorize_outlier(STSWD_summary.(fieldname).linear(idx_YA));
STSWD_summary.(fieldname).linear_win(idx_OA) = winsorize_outlier(STSWD_summary.(fieldname).linear(idx_OA));
for indCond = 1:4
    STSWD_summary.(fieldname).data(idx_YA,indCond) = winsorize_outlier(STSWD_summary.(fieldname).data(idx_YA,indCond));
    STSWD_summary.(fieldname).data(idx_OA,indCond) = winsorize_outlier(STSWD_summary.(fieldname).data(idx_OA,indCond));
end

% prestim alpha

load(fullfile(pn.tfr, 'data', 'c04_LV1.mat'), 'LV1');
tmp_IDidx = ismember(StateSwitchDynIDs, LV1.IDs);
fieldname = 'tfr_prestim_alpha_LV1';
STSWD_summary.(fieldname).data = NaN(N_total,4);
STSWD_summary.(fieldname).data(tmp_IDidx,:) = LV1.data;
X = [1 1; 1 2; 1 3; 1 4]; b=X\STSWD_summary.(fieldname).data';
STSWD_summary.(fieldname).linear = NaN(N_total,1);
STSWD_summary.(fieldname).linear(:,1) = b(2,:);
STSWD_summary.(fieldname).linear_win(idx_YA) = winsorize_outlier(STSWD_summary.(fieldname).linear(idx_YA));
STSWD_summary.(fieldname).linear_win(idx_OA) = winsorize_outlier(STSWD_summary.(fieldname).linear(idx_OA));
for indCond = 1:4
    STSWD_summary.(fieldname).data(idx_YA,indCond) = winsorize_outlier(STSWD_summary.(fieldname).data(idx_YA,indCond));
    STSWD_summary.(fieldname).data(idx_OA,indCond) = winsorize_outlier(STSWD_summary.(fieldname).data(idx_OA,indCond));
end

%% add excitability brainscore

load(fullfile(pn.pls_excit, 'data', 'm01_LV1.mat'), 'LV1');
tmp_IDidx = ismember(StateSwitchDynIDs, LV1.IDs);
fieldname = 'excitability_LV1';
STSWD_summary.(fieldname).data = NaN(N_total,4);
STSWD_summary.(fieldname).data(tmp_IDidx,:) = LV1.data;
X = [1 1; 1 2; 1 3; 1 4]; b=X\STSWD_summary.(fieldname).data';
STSWD_summary.(fieldname).linear = NaN(N_total,1);
STSWD_summary.(fieldname).linear(:,1) = b(2,:);
STSWD_summary.(fieldname).linear_win(idx_YA) = winsorize_outlier(STSWD_summary.(fieldname).linear(idx_YA));
STSWD_summary.(fieldname).linear_win(idx_OA) = winsorize_outlier(STSWD_summary.(fieldname).linear(idx_OA));
for indCond = 1:4
    STSWD_summary.(fieldname).data(idx_YA,indCond) = winsorize_outlier(STSWD_summary.(fieldname).data(idx_YA,indCond));
    STSWD_summary.(fieldname).data(idx_OA,indCond) = winsorize_outlier(STSWD_summary.(fieldname).data(idx_OA,indCond));
end

%% add SSVEP magnitude

curData = fullfile(pn.ssvep, 'data', 'individualFFT', 'Z_SSVEPmag_YAOA.mat');
load(curData, 'SSVEPmag');

tmp_IDidx = ismember(StateSwitchDynIDs, SSVEPmag.IDs);

fieldname = 'SSVEPmag_orig';
STSWD_summary.(fieldname).data = NaN(N_total,4);
STSWD_summary.(fieldname).data(tmp_IDidx,:) = SSVEPmag.orig.data;
X = [1 1; 1 2; 1 3; 1 4]; b=X\STSWD_summary.(fieldname).data';
STSWD_summary.(fieldname).linear = NaN(N_total,1);
STSWD_summary.(fieldname).linear(:,1) = b(2,:);
STSWD_summary.(fieldname).linear_win(idx_YA) = winsorize_outlier(STSWD_summary.(fieldname).linear(idx_YA));
STSWD_summary.(fieldname).linear_win(idx_OA) = winsorize_outlier(STSWD_summary.(fieldname).linear(idx_OA));
for indCond = 1:4
    STSWD_summary.(fieldname).data(idx_YA,indCond) = winsorize_outlier(STSWD_summary.(fieldname).data(idx_YA,indCond));
    STSWD_summary.(fieldname).data(idx_OA,indCond) = winsorize_outlier(STSWD_summary.(fieldname).data(idx_OA,indCond));
end

fieldname = 'SSVEPmag_norm';
STSWD_summary.(fieldname).data = NaN(N_total,4);
STSWD_summary.(fieldname).data(tmp_IDidx,:) = SSVEPmag.norm.data;
X = [1 1; 1 2; 1 3; 1 4]; b=X\STSWD_summary.(fieldname).data';
STSWD_summary.(fieldname).linear = NaN(N_total,1);
STSWD_summary.(fieldname).linear(:,1) = b(2,:);
STSWD_summary.(fieldname).linear_win(idx_YA) = winsorize_outlier(STSWD_summary.(fieldname).linear(idx_YA));
STSWD_summary.(fieldname).linear_win(idx_OA) = winsorize_outlier(STSWD_summary.(fieldname).linear(idx_OA));
for indCond = 1:4
    STSWD_summary.(fieldname).data(idx_YA,indCond) = winsorize_outlier(STSWD_summary.(fieldname).data(idx_YA,indCond));
    STSWD_summary.(fieldname).data(idx_OA,indCond) = winsorize_outlier(STSWD_summary.(fieldname).data(idx_OA,indCond));
end

%% add SPM task PLS brainscore (LV1)

curData = fullfile(pn.mri, 'data', 's6a_LV1');
load(curData);
tmp_IDidx = ismember(StateSwitchDynIDs, LV1.IDs);

fieldname = 'SPM_lv1';
STSWD_summary.(fieldname).data = NaN(N_total,4);
STSWD_summary.(fieldname).data(tmp_IDidx,:) = LV1.data;
X = [1 1; 1 2; 1 3; 1 4]; b=X\STSWD_summary.(fieldname).data';
STSWD_summary.(fieldname).linear = NaN(N_total,1);
STSWD_summary.(fieldname).linear(:,1) = b(2,:);
STSWD_summary.(fieldname).linear_win(idx_YA) = winsorize_outlier(STSWD_summary.(fieldname).linear(idx_YA));
STSWD_summary.(fieldname).linear_win(idx_OA) = winsorize_outlier(STSWD_summary.(fieldname).linear(idx_OA));
for indCond = 1:4
    STSWD_summary.(fieldname).data(idx_YA,indCond) = winsorize_outlier(STSWD_summary.(fieldname).data(idx_YA,indCond));
    STSWD_summary.(fieldname).data(idx_OA,indCond) = winsorize_outlier(STSWD_summary.(fieldname).data(idx_OA,indCond));
end

% add LV2

curData = fullfile(pn.mri, 'data', 's6a_LV2');
load(curData);
tmp_IDidx = ismember(StateSwitchDynIDs, LV2.IDs);

fieldname = 'SPM_lv2';
STSWD_summary.(fieldname).data = NaN(N_total,4);
STSWD_summary.(fieldname).data(tmp_IDidx,:) = LV2.data;
X = [1 1; 1 2; 1 3; 1 4]; b=X\STSWD_summary.(fieldname).data';
STSWD_summary.(fieldname).linear = NaN(N_total,1);
STSWD_summary.(fieldname).linear(:,1) = b(2,:);
STSWD_summary.(fieldname).linear_win(idx_YA) = winsorize_outlier(STSWD_summary.(fieldname).linear(idx_YA));
STSWD_summary.(fieldname).linear_win(idx_OA) = winsorize_outlier(STSWD_summary.(fieldname).linear(idx_OA));
for indCond = 1:4
    STSWD_summary.(fieldname).data(idx_YA,indCond) = winsorize_outlier(STSWD_summary.(fieldname).data(idx_YA,indCond));
    STSWD_summary.(fieldname).data(idx_OA,indCond) = winsorize_outlier(STSWD_summary.(fieldname).data(idx_OA,indCond));
end

% add LV3

curData = fullfile(pn.mri, 'data', 's6a_LV3');
load(curData);
tmp_IDidx = ismember(StateSwitchDynIDs, LV3.IDs);

fieldname = 'SPM_lv3';
STSWD_summary.(fieldname).data = NaN(N_total,4);
STSWD_summary.(fieldname).data(tmp_IDidx,:) = LV3.data;
X = [1 1; 1 2; 1 3; 1 4]; b=X\STSWD_summary.(fieldname).data';
STSWD_summary.(fieldname).linear = NaN(N_total,1);
STSWD_summary.(fieldname).linear(:,1) = b(2,:);
STSWD_summary.(fieldname).linear_win(idx_YA) = winsorize_outlier(STSWD_summary.(fieldname).linear(idx_YA));
STSWD_summary.(fieldname).linear_win(idx_OA) = winsorize_outlier(STSWD_summary.(fieldname).linear(idx_OA));
for indCond = 1:4
    STSWD_summary.(fieldname).data(idx_YA,indCond) = winsorize_outlier(STSWD_summary.(fieldname).data(idx_YA,indCond));
    STSWD_summary.(fieldname).data(idx_OA,indCond) = winsorize_outlier(STSWD_summary.(fieldname).data(idx_OA,indCond));
end

%% add SPM behav PLS brainscore (LV1)

curData = fullfile(pn.mri, 'data', 'pls', 'behavPLS_LV1');
load(curData);
tmp_IDidx = ismember(StateSwitchDynIDs, LV1.IDs);

fieldname = 'behavPLS_BrainS_LV1';
STSWD_summary.(fieldname).linear = NaN(N_total,1);
STSWD_summary.(fieldname).linear(tmp_IDidx,:) = LV1.BrainS;
STSWD_summary.(fieldname).linear_win(idx_YA) = winsorize_outlier(STSWD_summary.(fieldname).linear(idx_YA));
STSWD_summary.(fieldname).linear_win(idx_OA) = winsorize_outlier(STSWD_summary.(fieldname).linear(idx_OA));

fieldname = 'behavPLS_BehavS_LV1';
STSWD_summary.(fieldname).linear = NaN(N_total,1);
STSWD_summary.(fieldname).linear(tmp_IDidx,:) = LV1.BehavS;
STSWD_summary.(fieldname).linear_win(idx_YA) = winsorize_outlier(STSWD_summary.(fieldname).linear(idx_YA));
STSWD_summary.(fieldname).linear_win(idx_OA) = winsorize_outlier(STSWD_summary.(fieldname).linear(idx_OA));

%% decoding

curData = fullfile(pn.decoding, 'data', 'G_DecodingResults', 'g6d_cued_LV1.mat');
load(curData);
tmp_IDidx = ismember(StateSwitchDynIDs, LV1.IDs);

fieldname = 'dec_cued';
STSWD_summary.(fieldname).data = NaN(N_total,4);
STSWD_summary.(fieldname).data(tmp_IDidx,:) = LV1.data;
X = [1 1; 1 2; 1 3; 1 4]; b=X\STSWD_summary.(fieldname).data';
STSWD_summary.(fieldname).linear = NaN(N_total,1);
STSWD_summary.(fieldname).linear(:,1) = b(2,:);
STSWD_summary.(fieldname).linear_win(idx_YA) = winsorize_outlier(STSWD_summary.(fieldname).linear(idx_YA));
STSWD_summary.(fieldname).linear_win(idx_OA) = winsorize_outlier(STSWD_summary.(fieldname).linear(idx_OA));
for indCond = 1:4
    STSWD_summary.(fieldname).data(idx_YA,indCond) = winsorize_outlier(STSWD_summary.(fieldname).data(idx_YA,indCond));
    STSWD_summary.(fieldname).data(idx_OA,indCond) = winsorize_outlier(STSWD_summary.(fieldname).data(idx_OA,indCond));
end

curData = fullfile(pn.decoding, 'data', 'G_DecodingResults','g6d_probed_LV1.mat');
load(curData);
tmp_IDidx = ismember(StateSwitchDynIDs, LV1.IDs);

fieldname = 'dec_probed';
STSWD_summary.(fieldname).data = NaN(N_total,4);
STSWD_summary.(fieldname).data(tmp_IDidx,:) = LV1.data;
X = [1 1; 1 2; 1 3; 1 4]; b=X\STSWD_summary.(fieldname).data';
STSWD_summary.(fieldname).linear = NaN(N_total,1);
STSWD_summary.(fieldname).linear(:,1) = b(2,:);
STSWD_summary.(fieldname).linear_win(idx_YA) = winsorize_outlier(STSWD_summary.(fieldname).linear(idx_YA));
STSWD_summary.(fieldname).linear_win(idx_OA) = winsorize_outlier(STSWD_summary.(fieldname).linear(idx_OA));
for indCond = 1:4
    STSWD_summary.(fieldname).data(idx_YA,indCond) = winsorize_outlier(STSWD_summary.(fieldname).data(idx_YA,indCond));
    STSWD_summary.(fieldname).data(idx_OA,indCond) = winsorize_outlier(STSWD_summary.(fieldname).data(idx_OA,indCond));
end

curData = fullfile(pn.decoding, 'data', 'G_DecodingResults','g6d_unprobed_LV1.mat');
load(curData);
tmp_IDidx = ismember(StateSwitchDynIDs, LV1.IDs);

fieldname = 'dec_unprobed';
STSWD_summary.(fieldname).data = NaN(N_total,4);
STSWD_summary.(fieldname).data(tmp_IDidx,:) = LV1.data;
X = [1 1; 1 2; 1 3; 1 4]; b=X\STSWD_summary.(fieldname).data';
STSWD_summary.(fieldname).linear = NaN(N_total,1);
STSWD_summary.(fieldname).linear(:,1) = b(2,:);
STSWD_summary.(fieldname).linear_win(idx_YA) = winsorize_outlier(STSWD_summary.(fieldname).linear(idx_YA));
STSWD_summary.(fieldname).linear_win(idx_OA) = winsorize_outlier(STSWD_summary.(fieldname).linear(idx_OA));
for indCond = 1:4
    STSWD_summary.(fieldname).data(idx_YA,indCond) = winsorize_outlier(STSWD_summary.(fieldname).data(idx_YA,indCond));
    STSWD_summary.(fieldname).data(idx_OA,indCond) = winsorize_outlier(STSWD_summary.(fieldname).data(idx_OA,indCond));
end

%% add stroop data

curData = fullfile(pn.stroop, 'data', 'stroop_params.mat');
load(curData)

fieldname = 'stroop_rt';
STSWD_summary.(fieldname).data = NaN(N_total,1);
STSWD_summary.(fieldname).data(:,1) = Interference0;

fieldname = 'stroop_rt_mismatch';
STSWD_summary.(fieldname).data = NaN(N_total,1);
STSWD_summary.(fieldname).data(:,1) = Interference1;

fieldname = 'stroop_interference';
STSWD_summary.(fieldname).data = NaN(N_total,1);
STSWD_summary.(fieldname).data(:,1) = InterferenceCosts;

curData = fullfile(pn.stroop_cpp, 'data', 'stroop_cpp.mat');
load(curData)

tmp_IDidx = ismember(StateSwitchDynIDs, stroop_cpp.IDs);

fieldname = 'stroop_cpp';
STSWD_summary.(fieldname).data = NaN(N_total,2);
STSWD_summary.(fieldname).data(tmp_IDidx,:) = stroop_cpp.data;
% linear change in brainscore
X = [1 1; 1 2]; b=X\STSWD_summary.(fieldname).data';
STSWD_summary.(fieldname).linear = NaN(N_total,1);
STSWD_summary.(fieldname).linear(:,1) = b(2,:);
STSWD_summary.(fieldname).linear_win(idx_YA) = winsorize_outlier(STSWD_summary.(fieldname).linear(idx_YA));
STSWD_summary.(fieldname).linear_win(idx_OA) = winsorize_outlier(STSWD_summary.(fieldname).linear(idx_OA));
for indCond = 1:2
    STSWD_summary.(fieldname).data(idx_YA,indCond) = winsorize_outlier(STSWD_summary.(fieldname).data(idx_YA,indCond));
    STSWD_summary.(fieldname).data(idx_OA,indCond) = winsorize_outlier(STSWD_summary.(fieldname).data(idx_OA,indCond));
end

%% save IDs 

STSWD_summary.IDs = StateSwitchDynIDs;

%% save entire structure

save(fullfile(pn.data_summary, 'STSWD_summary_YAOA.mat'), 'STSWD_summary')
