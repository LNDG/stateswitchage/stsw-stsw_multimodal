% assess inter-individual relations between first level linear effects 

% path management
currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..'))

rootpath = pwd;
pn.data_summary = fullfile(rootpath, 'data');
pn.figures = fullfile(rootpath, 'figures', 'correlations'); mkdir(pn.figures);

filename = fullfile(rootpath, 'code', 'id_list.txt');
fileID = fopen(filename);
IDs = textscan(fileID,'%s');
fclose(fileID);
IDs = IDs{1};

filename = fullfile(rootpath, 'code', 'id_list_eegmr.txt');
fileID = fopen(filename);
IDs_EEGMR = textscan(fileID,'%s');
fclose(fileID);
IDs_EEGMR = IDs_EEGMR{1};

%% load summary structure

load(fullfile(pn.data_summary, 'STSWD_summary_YAOA.mat'), 'STSWD_summary')

% select subjects (EEG only)
idxEEG_summary = ismember(STSWD_summary.IDs, IDs);
[STSWD_summary.IDs(idxEEG_summary), IDs]

% select subjects (multimodal only)
idxMulti_summary = ismember(STSWD_summary.IDs, IDs_EEGMR);
[STSWD_summary.IDs(idxMulti_summary), IDs_EEGMR]

idxYA = str2double(STSWD_summary.IDs(idxMulti_summary))<2000;
idxOA = str2double(STSWD_summary.IDs(idxMulti_summary))>=2000;

%% perform 2nd level correlations

color = [.6 .6 .6];

h = figure('units','centimeters','position',[10 10 10 7]);
set(0, 'DefaultFigureRenderer', 'painters');
    ax{1} = subplot(1,1,1); cla; hold on;
    x = STSWD_summary.tfr_prestim_alpha_LV1.linear_win(idxMulti_summary);
    y = STSWD_summary.HDDM_vat.driftEEGMRI_linear_win(idxMulti_summary);
    scatter(x, y, 1, 'filled', 'MarkerFaceColor', color); 
    l1 = lsline(); set(l1, 'Color',.1+color, 'LineWidth', 3);
    scatter(x(idxYA), y(idxYA), 50, 'square', 'filled', 'MarkerFaceColor', .2+color);
    scatter(x(idxOA), y(idxOA), 50, 'diamond', 'filled', 'MarkerFaceColor', color); 
	[r, p] = corrcoef(x, y);
% 	legend([l1], ['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))],'location', 'SouthEast'); legend('boxoff')   
    title(['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))])
    xlabel({'prestim alpha bs';'(linear modulation)'}); ylabel({'Drift rate';'(linear modulation)'})
    ylim([min(y)-(.1.*(max(y)-min(y))), max(y)+(.1.*(max(y)-min(y)))])
    set(h,'Color','w')
    for indAx = 1:1; ax{indAx}.Color = [.2 .2 .2]; end
    set(findall(gcf,'-property','FontSize'),'FontSize',14)
h.InvertHardcopy = 'off';
figureName = ['f_prestimalpha_drift'];
saveas(h, fullfile(pn.figures, figureName), 'epsc');
saveas(h, fullfile(pn.figures, figureName), 'png');