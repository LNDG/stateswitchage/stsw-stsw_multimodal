% % assess relations between x & y
% 
% lme = fitlme(tbl,'y~x+(1|Subject)+(1|Condition)', 'CovariancePattern', {'CompSymm' ; 'CompSymm'});
% lme
% 
% lme = fitlme(tbl,'x~y+(1|Subject)+(1|Condition)', 'CovariancePattern', {'CompSymm' ; 'CompSymm'});
% lme
% 
% % automatically extract values
% lme.Coefficients(2,2).Estimate
% lme.Coefficients(2,5).DF
% lme.Coefficients(2,6).pValue

%% load data

pn.summaryData = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/multimodal/B_crossCorrelations/B_data/';
load([pn.summaryData, 'STSWDsummary_YA_forR.mat'],'data')

dataS = cell2table(data(2:end,:));
dataS.Properties.VariableNames = data(1,:);

tbl = table(double(dataS.Condition),double(dataS.drift_eeg),double(dataS.eeg_prestim_lv1),dataS.Subject,'VariableNames',{'Condition','drift_eeg','eeg_prestim_lv1', 'Subject'});
lme = fitlme(tbl,'drift_eeg~Subject + Condition + eeg_prestim_lv1+Condition*eeg_prestim_lv1', 'CovariancePattern', {'CompSymm' ; 'CompSymm'});
%lme = fitlme(tbl,'drift_eeg~eeg_prestim_lv1+(1|Subject)', 'CovariancePattern', {'CompSymm'});
lme.Coefficients(50,:)


tbl = table(double(dataS.Condition),double(dataS.drift_eeg),double(dataS.eeg_lv1),dataS.Subject,...
    'VariableNames',{'Condition','drift_eeg','eeg_lv1', 'Subject'});
lme = fitlme(tbl,'drift_eeg~Condition+eeg_lv1+(1|Subject)', 'CovariancePattern', {'CompSymm'});
lme.Coefficients(4,:)

tbl = table(double(dataS.Condition),double(dataS.drift_eeg),double(dataS.eeg_lv1),dataS.Subject,...
    'VariableNames',{'Condition','drift_eeg','eeg_lv1', 'Subject'});
lme = fitlme(tbl,'drift_eeg~Condition+eeg_lv1+eeg_lv1*Condition+(1|Subject)', 'CovariancePattern', {'CompSymm'});
lme.Coefficients(4,:)

tbl = table(double(dataS.Condition),double(dataS.drift_eeg),double(dataS.eeg_prestim_lv1),dataS.Subject,'VariableNames',{'Condition','drift_eeg','eeg_prestim_lv1', 'Subject'});
lme = fitlme(tbl,'drift_eeg~Condition+eeg_prestim_lv1+eeg_prestim_lv1*Condition+(1|Subject)', 'CovariancePattern', {'CompSymm'});
lme.Coefficients(4,:)


tbl = table(double(dataS.Condition),double(dataS.drift_eeg),double(dataS.eeg_lv1),dataS.Subject,...
    'VariableNames',{'Condition','drift_eeg','eeg_lv1', 'Subject'});
lme = fitlme(tbl,'drift_eeg~Subject+Condition+eeg_lv1+Condition*eeg_lv1', 'CovariancePattern', {'CompSymm'});
lme.Coefficients(50,:)


tbl = table(double(dataS.Condition),double(dataS.drift_eeg),double(dataS.eeg_lv1),dataS.Subject,...
    'VariableNames',{'Condition','drift_eeg','eeg_lv1', 'Subject'});
lme = fitlme(tbl,'drift_eeg~Subject + Condition + eeg_lv1+Condition*eeg_lv1', 'CovariancePattern', {'CompSymm' ; 'CompSymm'});
lme.Coefficients(50,:)


tbl = table(double(dataS.Condition),double(dataS.drift_eeg),double(dataS.pupil),dataS.Subject,...
    'VariableNames',{'Condition','drift_eeg','pupil', 'Subject'});
lme = fitlme(tbl,'drift_eeg~Subject + Condition + pupil+Condition*pupil', 'CovariancePattern', {'CompSymm' ; 'CompSymm'});
lme.Coefficients(50,:)


lme = fitlme(tbl,'eeg_lv1~drift_eeg+(1|Subject)+(1|Condition)', 'CovariancePattern', {'CompSymm' ; 'CompSymm'});
lme


tbl = table(double(dataS.Condition),double(dataS.drift_eeg),double(dataS.eeg_lv1),dataS.Subject,...
    'VariableNames',{'Condition','drift_eeg','eeg_lv1', 'Subject'});
lme = fitlme(tbl,'eeg_lv1~Subject+Condition+drift_eeg', 'CovariancePattern', {'CompSymm'});
lme

tbl = table(double(dataS.Condition),double(dataS.drift_eeg),double(dataS.eeg_lv1),dataS.Subject,...
    'VariableNames',{'Condition','drift_eeg','eeg_lv1', 'Subject'});
lme = fitlme(tbl,'drift_eeg~Subject+Condition+eeg_lv1', 'CovariancePattern', {'CompSymm'});
lme


%% residualize values for plotting

pn.summaryData = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/multimodal/B_crossCorrelations/B_data/';
load([pn.summaryData, 'STSWDsummary_YA_forR.mat'],'data')

dataS = cell2table(data(2:end,:));
dataS.Properties.VariableNames = data(1,:);

tbl = table(categorical(dataS.Condition),double(dataS.drift_eeg),double(dataS.eeg_lv1),dataS.Subject,...
    'VariableNames',{'Condition','drift_eeg','eeg_lv1', 'Subject'});
lme1 = fitlme(tbl,'drift_eeg~Subject + Condition');
lme2 = fitlme(tbl,'eeg_lv1~Subject + Condition');
%lme.Coefficients(50,:)

R1 = residuals(lme1);
R2 = residuals(lme2);

figure; 
scatter(R1, R2, 'MarkerEdgeColor', [1 1 1])
l1 = lsline(); set(l1, 'Color',[0 0 0], 'LineWidth', 3);
[r] = corrcoef(R1, R2);
% choose different plotting by considering subject definition
for indLoad = 1:4
	R1_bySub(:,indLoad) = double(R1(double(dataS.Condition)==indLoad));
	R2_bySub(:,indLoad) = double(R2(double(dataS.Condition)==indLoad));
end
for indID = 1:size(R1_bySub,1)
    hold on;
    scatter(R1_bySub(indID,:), R2_bySub(indID,:), [], cmap(indID,:), 'filled');
end
xlabel('Residuals in drift'); ylabel('Residuals in EEG LV1'); 
title(['Load as categorical covariate; r = ', num2str(round(r(1,2),1))]);

% create a histogram of individual residual-residual slopes

X = [1 1; 1 2; 1 3; 1 4]; b_est=X\R1_bySub'; slope_b1 = b_est(2,:);
X = [1 1; 1 2; 1 3; 1 4]; b_est=X\R2_bySub'; slope_b2 = b_est(2,:);

r = corrcoef(slope_b1,slope_b2);

figure; subplot(1,2,1); hold on; histogram(slope_b1,10); subplot(1,2,2); histogram(slope_b2,10)

%% load as numeric

tbl = table(double(dataS.Condition),double(dataS.drift_eeg),double(dataS.eeg_lv1),dataS.Subject,...
    'VariableNames',{'Condition','drift_eeg','eeg_lv1', 'Subject'});
lme1 = fitlme(tbl,'drift_eeg~Subject + Condition');
lme2 = fitlme(tbl,'eeg_lv1~Subject + Condition');
%lme.Coefficients(50,:)

R1 = residuals(lme1);
R2 = residuals(lme2);

addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/multimodal/B_crossCorrelations/T_tools/distinguishable_colors');

cmap = distinguishable_colors(size(R1,1));

figure; 
scatter(R1, R2, 'MarkerEdgeColor', [1 1 1])
l1 = lsline(); set(l1, 'Color',[0 0 0], 'LineWidth', 3);
[r] = corrcoef(R1, R2);
% choose different plotting by considering subject definition
for indLoad = 1:4
	R1_bySub(:,indLoad) = double(R1(double(dataS.Condition)==indLoad));
	R2_bySub(:,indLoad) = double(R2(double(dataS.Condition)==indLoad));
end
for indID = 1:size(R1_bySub,1)
    hold on;
    scatter(R1_bySub(indID,:), R2_bySub(indID,:), [], cmap(indID,:), 'filled');
end
xlabel('Residuals in drift'); ylabel('Residuals in EEG LV1'); 
title(['Load as numeric covariate; r = ', num2str(round(r(1,2),1))]);


%%

pn.summaryData = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/multimodal/B_crossCorrelations/B_data/';
load([pn.summaryData, 'STSWDsummary_YA_forR.mat'],'data')

dataS = cell2table(data(2:end,:));
dataS.Properties.VariableNames = data(1,:);

variables{1,1} = 'drift_eeg'; variables{1,2} = 'eeg_lv1';
variables{2,1} = 'ndt_eeg'; variables{2,2} = 'eeg_lv1';
variables{3,1} = 'drift_eeg'; variables{3,2} = 'eeg_prestim_lv1';
variables{4,1} = 'ndt_eeg'; variables{4,2} = 'eeg_prestim_lv1';
variables{5,1} = 'eeg_lv1'; variables{5,2} = 'eeg_prestim_lv1';
variables{6,1} = 'drift_eeg'; variables{6,2} = 'eeg_lv1';

variables{7,1} = 'thetapow'; variables{7,2} = 'thetadur';
variables{8,1} = 'alphapow'; variables{8,2} = 'alphadur';
variables{9,1} = 'alphadur'; variables{9,2} = 'eeg_lv1';
variables{10,1} = 'alphapow'; variables{10,2} = 'eeg_lv1';
variables{11,1} = 'thetapow'; variables{11,2} = 'eeg_lv1';
variables{12,1} = 'thetadur'; variables{12,2} = 'eeg_lv1';

variables{13,1} = 'entropy'; variables{13,2} = 'eeg_lv1';
variables{14,1} = 'drift_eeg'; variables{14,2} = 'entropy';
variables{15,1} = 'ndt_eeg'; variables{15,2} = 'entropy';
variables{16,1} = 'aperiodic'; variables{16,2} = 'eeg_lv1';
variables{17,1} = 'aperiodic'; variables{17,2} = 'entropy';
variables{18,1} = 'aperiodic'; variables{18,2} = 'drift_eeg';
variables{19,1} = 'aperiodic'; variables{19,2} = 'ndt_eeg';
variables{20,1} = 'pupil'; variables{20,2} = 'eeg_lv1';
variables{21,1} = 'pupil'; variables{21,2} = 'entropy';
variables{22,1} = 'pupil'; variables{22,2} = 'aperiodic';

variables{23,1} = 'drift_eeg'; variables{23,2} = 'pupil';
variables{24,1} = 'ndt_eeg'; variables{24,2} = 'pupil';

% variables{25,1} = 'mri_lv1'; variables{25,2} = 'drift_mri';
% variables{26,1} = 'aperiodic'; variables{26,2} = 'entropy';
% variables{27,1} = 'aperiodic'; variables{27,2} = 'entropy';
% variables{28,1} = 'aperiodic'; variables{28,2} = 'entropy';
% variables{29,1} = 'aperiodic'; variables{29,2} = 'entropy';
% variables{30,1} = 'aperiodic'; variables{30,2} = 'entropy';

figure;
for indComparison = 1:size(variables,1)

    cond = dataS.Condition;
    sub = dataS.Subject;
    x1 = dataS.(variables{indComparison,1});
    x2 = dataS.(variables{indComparison,2});

    tbl = table(categorical(cond),double(x1),double(x2),categorical(sub),...
        'VariableNames',{'Condition','x1','x2', 'Subject'});
    lme1 = fitlme(tbl,'x1~Subject + Condition'); R1 = residuals(lme1);
    lme2 = fitlme(tbl,'x2~Subject + Condition'); R2 = residuals(lme2);

    subplot(5,5,indComparison) 
    scatter(R1, R2, 'MarkerEdgeColor', [1 1 1])
    l1 = lsline(); set(l1, 'Color',[0 0 0], 'LineWidth', 3);
    [r] = corrcoef(R1, R2);
    % choose different plotting by considering subject definition
    for indLoad = 1:4
        R1_bySub(:,indLoad) = double(R1(double(dataS.Condition)==indLoad));
        R2_bySub(:,indLoad) = double(R2(double(dataS.Condition)==indLoad));
    end
    for indID = 1:size(R1_bySub,1)
        hold on;
        scatter(R1_bySub(indID,:), R2_bySub(indID,:), [], cmap(indID,:), 'filled');
    end
    xlabel(['Residuals in ', variables{indComparison,1}]); ylabel(['Residuals in ', variables{indComparison,2}]); 
    title(['r = ', num2str(round(r(1,2),1))]);
end


%% assess correlation of individual residual change with original change

R = [];
figure;
for indComparison = 1:size(variables,1)

    cond = dataS.Condition;
    sub = dataS.Subject;
    x1 = dataS.(variables{indComparison,1});
    x2 = dataS.(variables{indComparison,2});
    cov = dataS.eeg_lv1;

    tbl = table(categorical(cond),double(x1),double(x2), double(cov),categorical(sub),...
        'VariableNames',{'Condition','x1','x2', 'cov', 'Subject'});
    lme1 = fitlme(tbl,'x1~Subject + Condition'); R1 = residuals(lme1);
    lme2 = fitlme(tbl,'x2~Subject + Condition'); R2 = residuals(lme2);
    
    subplot(5,5,indComparison); hold on;
    % choose different plotting by considering subject definition
    for indLoad = 1:4
        R1_bySub(:,indLoad) = double(R1(double(dataS.Condition)==indLoad));
        R2_bySub(:,indLoad) = double(R2(double(dataS.Condition)==indLoad));
        
        X1_bySub(:,indLoad) = double(x1(double(dataS.Condition)==indLoad));
        X2_bySub(:,indLoad) = double(x2(double(dataS.Condition)==indLoad));
    end
    
    X = [1 1; 1 2; 1 3; 1 4]; b_est=X\R1_bySub'; slope_b1 = b_est(2,:);
    X = [1 1; 1 2; 1 3; 1 4]; b_est=X\R2_bySub'; slope_b2 = b_est(2,:);

    X = [1 1; 1 2; 1 3; 1 4]; b_est=X\X1_bySub'; slope_x1 = b_est(2,:);
    X = [1 1; 1 2; 1 3; 1 4]; b_est=X\X2_bySub'; slope_x2 = b_est(2,:); 
    
    [r] = corrcoef(slope_b1, slope_x1);
    [r2, p2] = corrcoef(slope_b2, slope_x2);
    
    [r] = corrcoef(slope_b1, slope_b2)
    [r2] = corrcoef(slope_x1, slope_x2)
    
    R(indComparison,1) = r(1,2);
    R(indComparison,2) = r2(1,2);
    
	scatter(slope_b1, slope_x1, 'filled')
	scatter(slope_b2, slope_x2, 'filled')
    
end

figure; histogram(R(:,1),10)
figure; histogram(R(:,2),10)

%% replicate with linear slope fits

pn.summaryData = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/multimodal/B_crossCorrelations/B_data/';
load([pn.summaryData, 'STSWD_summary.mat'], 'STSWD_summary')

idx_Summary = find(ismember(STSWD_summary.IDs, unique(dataS.Subject)));
[STSWD_summary.IDs(idx_Summary),unique(dataS.Subject)]


figure; 
x = STSWD_summary.EEG_LV1.slope(idx_Summary,1);
y = STSWD_summary.HDDM_vt.driftEEG_linear(idx_Summary,1);
scatter(x, y, 'filled')
[r, p] = corrcoef(x, y)

% correlate linear slopes between first level and residuals
tbl = table(categorical(cond),double(dataS.eeg_lv1),double(dataS.drift_eeg), double(cov),categorical(sub),...
        'VariableNames',{'Condition','x1','x2', 'cov', 'Subject'});
    lme1 = fitlme(tbl,'x1~Subject + Condition'); R1 = residuals(lme1);
    lme2 = fitlme(tbl,'x2~Subject + Condition'); R2 = residuals(lme2);

for indLoad = 1:4
    X1_bySub(:,indLoad) = double(R1(double(dataS.Condition)==indLoad));
    X2_bySub(:,indLoad) = double(R2(double(dataS.Condition)==indLoad));
end

X = [1 1; 1 2; 1 3; 1 4]; b_est=X\X1_bySub'; slope_x1 = b_est(2,:);
X = [1 1; 1 2; 1 3; 1 4]; b_est=X\X2_bySub'; slope_x2 = b_est(2,:);

figure; 
x = slope_x1;
y = slope_x2;
scatter(x, y, 'filled')
[r, p] = corrcoef(x, y)

figure; 
x = R1;
y = R2;
scatter(x, y, 'filled')
[r, p] = corrcoef(x, y)

figure; 
x = STSWD_summary.EEG_LV1.slope(idx_Summary,1);
y = slope_x1;
scatter(x, y, 'filled')
[r, p] = corrcoef(x, y)

%% add random condition slopes

% correlate linear slopes between first level and residuals
tbl = table(categorical(cond),double(dataS.eeg_lv1),double(dataS.drift_eeg), double(cov),categorical(sub),...
        'VariableNames',{'Condition','x1','x2', 'cov', 'Subject'});
lme1 = fitlme(tbl,'x1~Subject+Condition', 'CovariancePattern', {'CompSymm'}); R1 = residuals(lme1);
lme2 = fitlme(tbl,'x2~Subject+Condition', 'CovariancePattern', {'CompSymm'}); R2 = residuals(lme2);

for indLoad = 1:4
    X1_bySub(:,indLoad) = double(R1(double(dataS.Condition)==indLoad));
    X2_bySub(:,indLoad) = double(R2(double(dataS.Condition)==indLoad));
end

X = [1 1; 1 2; 1 3; 1 4]; b_est=X\X1_bySub'; slope_x1 = b_est(2,:);
X = [1 1; 1 2; 1 3; 1 4]; b_est=X\X2_bySub'; slope_x2 = b_est(2,:);

figure; 
x = slope_x1;
y = slope_x2;
scatter(x, y, 'filled')
[r, p] = corrcoef(x, y)

figure; 
x = R1;
y = R2;
scatter(x, y, 'filled')
[r, p] = corrcoef(x, y)

figure; 
x = STSWD_summary.EEG_LV1.slope(idx_Summary,1);
y = slope_x1;
scatter(x, y, 'filled')
[r, p] = corrcoef(x, y)

figure; 
x = STSWD_summary.HDDM_vt.driftEEG_linear(idx_Summary,1);
y = slope_x2;
scatter(x, y, 'filled')
[r, p] = corrcoef(x, y)
