% assess inter-individual relations between first level linear effects 

% path management
currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..'))

rootpath = pwd;
pn.data_summary = fullfile(rootpath, 'data');
pn.figures = fullfile(rootpath, 'figures', 'correlations'); mkdir(pn.figures);

filename = fullfile(rootpath, 'code', 'id_list.txt');
fileID = fopen(filename);
IDs = textscan(fileID,'%s');
fclose(fileID);
IDs = IDs{1};

filename = fullfile(rootpath, 'code', 'id_list_eegmr.txt');
fileID = fopen(filename);
IDs_EEGMR = textscan(fileID,'%s');
fclose(fileID);
IDs_EEGMR = IDs_EEGMR{1};

filename = fullfile(rootpath, 'code', 'id_list_eegmr_completeruns.txt');
fileID = fopen(filename);
IDs_EEGMRcomp = textscan(fileID,'%s');
fclose(fileID);
IDs_EEGMRcomp = IDs_EEGMRcomp{1};

%% load summary structure

load(fullfile(pn.data_summary, 'STSWD_summary_YAOA.mat'), 'STSWD_summary')

% select subjects (EEG only)
idxEEG_summary = ismember(STSWD_summary.IDs, IDs);
[STSWD_summary.IDs(idxEEG_summary), IDs]

% select subjects (multimodal only)
idxMulti_summary = ismember(STSWD_summary.IDs, IDs_EEGMR);
[STSWD_summary.IDs(idxMulti_summary), IDs_EEGMR]

% select subjects (multimodal with complete runs only)
idxMultiComplete_summary = ismember(STSWD_summary.IDs, IDs_EEGMRcomp);
[STSWD_summary.IDs(idxMultiComplete_summary), IDs_EEGMRcomp]

idxYA = cellfun(@str2num, IDs_EEGMR)<2000;
idxOA = cellfun(@str2num, IDs_EEGMR)>2000;

%% stroop rt (match) - drift

color = [.5 .7 .8];

h = figure('units','centimeters','position',[10 10 8 7]);
    ax{1} = subplot(1,1,1); cla; hold on;
    x = STSWD_summary.stroop_rt.data(idxMulti_summary,1);
    y = squeeze(nanmean(STSWD_summary.HDDM_vat.driftEEGMRI(idxMulti_summary,1:4),2));
	scatter(x, y, 1, 'filled', 'MarkerFaceColor', color); 
    l1 = lsline(); set(l1, 'Color',.1+color, 'LineWidth', 3);
    scatter(x(idxYA), y(idxYA), 50, 'square', 'filled', 'MarkerFaceColor', .2+color);
    scatter(x(idxOA), y(idxOA), 50, 'diamond', 'filled', 'MarkerFaceColor', color);  
    [r, p, RL, RU] = corrcoef(x, y);
% 	legend([l1], ['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))],'location', 'SouthEast'); legend('boxoff')   
    if p(2)>10^-3; tit = [', p = ', num2str(round(p(2),2))]; else; tit = sprintf(', p = %.1e',p(2)); end
    title(['r(',num2str(numel(x)-2),') = ', num2str(round(r(2),2)), ...
        ', 95%CI = [',num2str(round(RL(2),2)),',',num2str(round(RU(2),2)),']',...
        tit, ])
    xlabel({'stroop rt'}); ylabel({'drift rate';'(avg.)'})
    ylim([min(y)-(.1.*(max(y)-min(y))), max(y)+(.1.*(max(y)-min(y)))])
    xlim([min(x)-(.1.*(max(x)-min(x))), max(x)+(.1.*(max(x)-min(x)))])
    set(h,'Color','w')
    for indAx = 1:1; ax{indAx}.Color = [.2 .2 .2]; end
    set(findall(gcf,'-property','FontSize'),'FontSize',14)
h.InvertHardcopy = 'off';
figureName = ['f_drift-avg_stroop-rt'];
saveas(h, fullfile(pn.figures, figureName), 'epsc');
saveas(h, fullfile(pn.figures, figureName), 'png');

disp(['r(',num2str(numel(x)-2),') = ', num2str(round(r(2),2)), ...
        ', 95%CI = [',num2str(round(RL(2),2)),',',num2str(round(RU(2),2)),']',...
        tit, ])
    
% partialcorr:
h = figure('units','centimeters','position',[10 10 8 7]);
z = STSWD_summary.stroop_rt_mismatch.data(idxMulti_summary,1);
X = [x,y,z, cat(1, repmat(1,numel(find(idxYA)),1), repmat(2,numel(find(idxOA)),1))];
[r, p] = partialcorr(X);
X = [repmat(1,numel(x),1), z,cat(1, repmat(1,numel(find(idxYA)),1), repmat(2,numel(find(idxOA)),1))];
[b, tmp, x_r] = regress(x,X);
[b, tmp, y_r] = regress(y,X);
scatter(x_r, y_r, 70, 'filled', 'MarkerFaceColor', [.8 .8 .8]); l1 = lsline(); set(l1, 'Color',[1 .6 .6], 'LineWidth', 3);
[r, ~, RL, RU] = corrcoef(x_r, y_r);
if p(2)>10^-3; tit = [' p = ', num2str(round(p(2),2))]; else; tit = sprintf(' p = %.1e',p(2)); end
disp(['r(',num2str(numel(x)-2-2),') = ', num2str(round(r(2),2)), ...
        ', 95%CI = [',num2str(round(RL(2),2)),',',num2str(round(RU(2),2)),']',...
        tit, ])

%% stroop rt (mismatch) - drift

color = [.5 .7 .8];

h = figure('units','centimeters','position',[10 10 8 7]);
    ax{1} = subplot(1,1,1); cla; hold on;
    x = STSWD_summary.stroop_rt_mismatch.data(idxMulti_summary,1);
    y = squeeze(nanmean(STSWD_summary.HDDM_vat.driftEEGMRI(idxMulti_summary,1:4),2));
	scatter(x, y, 1, 'filled', 'MarkerFaceColor', color); 
    l1 = lsline(); set(l1, 'Color',.1+color, 'LineWidth', 3);
    scatter(x(idxYA), y(idxYA), 50, 'square', 'filled', 'MarkerFaceColor', .2+color);
    scatter(x(idxOA), y(idxOA), 50, 'diamond', 'filled', 'MarkerFaceColor', color);  
    [r, p, RL, RU] = corrcoef(x, y);
% 	legend([l1], ['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))],'location', 'SouthEast'); legend('boxoff')   
    if p(2)>10^-3; tit = [', p = ', num2str(round(p(2),2))]; else; tit = sprintf(', p = %.1e',p(2)); end
    title(['r(',num2str(numel(x)-2),') = ', num2str(round(r(2),2)), ...
        ', 95%CI = [',num2str(round(RL(2),2)),',',num2str(round(RU(2),2)),']',...
        tit, ])
    xlabel({'stroop rt'}); ylabel({'drift rate';'(avg.)'})
    ylim([min(y)-(.1.*(max(y)-min(y))), max(y)+(.1.*(max(y)-min(y)))])
    xlim([min(x)-(.1.*(max(x)-min(x))), max(x)+(.1.*(max(x)-min(x)))])
    set(h,'Color','w')
    for indAx = 1:1; ax{indAx}.Color = [.2 .2 .2]; end
    set(findall(gcf,'-property','FontSize'),'FontSize',14)
h.InvertHardcopy = 'off';
figureName = ['f_drift-avg_stroop-rt-mismatch'];
saveas(h, fullfile(pn.figures, figureName), 'epsc');
saveas(h, fullfile(pn.figures, figureName), 'png');

disp(['r(',num2str(numel(x)-2),') = ', num2str(round(r(2),2)), ...
        ', 95%CI = [',num2str(round(RL(2),2)),',',num2str(round(RU(2),2)),']',...
        tit, ])
    
% partialcorr including RT of match trials:
h = figure('units','centimeters','position',[10 10 8 7]);
z = STSWD_summary.stroop_rt.data(idxMulti_summary,1);
X = [x,y,z, cat(1, repmat(1,numel(find(idxYA)),1), repmat(2,numel(find(idxOA)),1))];
[r, p] = partialcorr(X);
X = [repmat(1,numel(x),1), z,cat(1, repmat(1,numel(find(idxYA)),1), repmat(2,numel(find(idxOA)),1))];
[b, tmp, x_r] = regress(x,X);
[b, tmp, y_r] = regress(y,X);
scatter(x_r, y_r, 70, 'filled', 'MarkerFaceColor', [.8 .8 .8]); l1 = lsline(); set(l1, 'Color',[1 .6 .6], 'LineWidth', 3);
[r, ~, RL, RU] = corrcoef(x_r, y_r);
if p(2)>10^-3; tit = [' p = ', num2str(round(p(2),2))]; else; tit = sprintf(' p = %.1e',p(2)); end
disp(['r(',num2str(numel(x)-2-2),') = ', num2str(round(r(2),2)), ...
        ', 95%CI = [',num2str(round(RL(2),2)),',',num2str(round(RU(2),2)),']',...
        tit, ])

%% correlation between match and mismatch RTs

color = [.8 .5 .5];

h = figure('units','centimeters','position',[10 10 8 7]);
    ax{1} = subplot(1,1,1); cla; hold on;
    x = STSWD_summary.stroop_rt_mismatch.data(idxMulti_summary,1);
    y = STSWD_summary.stroop_rt.data(idxMulti_summary,1);
	scatter(x, y, 1, 'filled', 'MarkerFaceColor', color); 
    l1 = lsline(); set(l1, 'Color',.1+color, 'LineWidth', 3);
    scatter(x(idxYA), y(idxYA), 50, 'square', 'filled', 'MarkerFaceColor', .2+color);
    scatter(x(idxOA), y(idxOA), 50, 'diamond', 'filled', 'MarkerFaceColor', color);  
    [r, p, RL, RU] = corrcoef(x, y);
% 	legend([l1], ['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))],'location', 'SouthEast'); legend('boxoff')   
    if p(2)>10^-3; tit = [', p = ', num2str(round(p(2),2))]; else; tit = sprintf(', p = %.1e',p(2)); end
    title(['r(',num2str(numel(x)-2),') = ', num2str(round(r(2),2)), ...
        ', 95%CI = [',num2str(round(RL(2),2)),',',num2str(round(RU(2),2)),']',...
        tit, ])
    xlabel({'stroop mismatch'}); ylabel({'stroop match'})
    ylim([min(y)-(.1.*(max(y)-min(y))), max(y)+(.1.*(max(y)-min(y)))])
    xlim([min(x)-(.1.*(max(x)-min(x))), max(x)+(.1.*(max(x)-min(x)))])
    set(h,'Color','w')
    for indAx = 1:1; ax{indAx}.Color = [.2 .2 .2]; end
    set(findall(gcf,'-property','FontSize'),'FontSize',14)
h.InvertHardcopy = 'off';
figureName = ['f_stroop-rt-match_stroop-rt-mismatch'];
saveas(h, fullfile(pn.figures, figureName), 'epsc');
saveas(h, fullfile(pn.figures, figureName), 'png');

disp(['r(',num2str(numel(x)-2),') = ', num2str(round(r(2),2)), ...
        ', 95%CI = [',num2str(round(RL(2),2)),',',num2str(round(RU(2),2)),']',...
        tit, ])
    
% partialcorr:
h = figure('units','centimeters','position',[10 10 8 7]);
X = [x,y, cat(1, repmat(1,numel(find(idxYA)),1), repmat(2,numel(find(idxOA)),1))];
[r, p] = partialcorr(X);
X = [repmat(1,numel(x),1), cat(1, repmat(1,numel(find(idxYA)),1), repmat(2,numel(find(idxOA)),1))];
[b, tmp, x_r] = regress(x,X);
[b, tmp, y_r] = regress(y,X);
scatter(x_r, y_r, 70, 'filled', 'MarkerFaceColor', [.8 .8 .8]); l1 = lsline(); set(l1, 'Color',[1 .6 .6], 'LineWidth', 3);
[r, ~, RL, RU] = corrcoef(x_r, y_r);
if p(2)>10^-3; tit = [' p = ', num2str(round(p(2),2))]; else; tit = sprintf(' p = %.1e',p(2)); end
disp(['r(',num2str(numel(x)-2-1),') = ', num2str(round(r(2),2)), ...
        ', 95%CI = [',num2str(round(RL(2),2)),',',num2str(round(RU(2),2)),']',...
        tit, ])

%% check statistical difference between correlation strengths
%http://comparingcorrelations.org/

% %% stroop rt-interference correlation
% 
% color = [.6 .6 .6];
% 
% h = figure('units','centimeters','position',[10 10 8 7]);
%     ax{1} = subplot(1,1,1); cla; hold on;
%     x = STSWD_summary.stroop_rt.data(idxMulti_summary,1);
%     y = STSWD_summary.stroop_interference.data(idxMulti_summary,1);
% 	scatter(x, y, 1, 'filled', 'MarkerFaceColor', color); 
%     l1 = lsline(); set(l1, 'Color',.1+color, 'LineWidth', 3);
%     scatter(x(idxYA), y(idxYA), 50, 'square', 'filled', 'MarkerFaceColor', .2+color);
%     scatter(x(idxOA), y(idxOA), 50, 'diamond', 'filled', 'MarkerFaceColor', color);  
%     [r, p, RL, RU] = corrcoef(x, y);
% % 	legend([l1], ['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))],'location', 'SouthEast'); legend('boxoff')   
%     if p(2)>10^-3; tit = [', p = ', num2str(round(p(2),2))]; else; tit = sprintf(', p = %.1e',p(2)); end
%     title(['r(',num2str(numel(x)-2),') = ', num2str(round(r(2),2)), ...
%         ', 95%CI = [',num2str(round(RL(2),2)),',',num2str(round(RU(2),2)),']',...
%         tit, ])
%     xlabel({'stroop rt'}); ylabel({'stroop interference'})
%     ylim([min(y)-(.1.*(max(y)-min(y))), max(y)+(.1.*(max(y)-min(y)))])
%     xlim([min(x)-(.1.*(max(x)-min(x))), max(x)+(.1.*(max(x)-min(x)))])
%     set(h,'Color','w')
%     for indAx = 1:1; ax{indAx}.Color = [.2 .2 .2]; end
%     set(findall(gcf,'-property','FontSize'),'FontSize',14)
% h.InvertHardcopy = 'off';
% figureName = ['f_stroop-rt_l1_stroop-int'];
% saveas(h, fullfile(pn.figures, figureName), 'epsc');
% saveas(h, fullfile(pn.figures, figureName), 'png');
% 
% disp(['r(',num2str(numel(x)-2),') = ', num2str(round(r(2),2)), ...
%         ', 95%CI = [',num2str(round(RL(2),2)),',',num2str(round(RU(2),2)),']',...
%         tit, ])
%     
% % partialcorr:
% h = figure('units','centimeters','position',[10 10 8 7]);
% X = [x,y, cat(1, repmat(1,numel(find(idxYA)),1), repmat(2,numel(find(idxOA)),1))];
% [r, p] = partialcorr(X);
% X = [repmat(1,numel(x),1), cat(1, repmat(1,numel(find(idxYA)),1), repmat(2,numel(find(idxOA)),1))];
% [b, tmp, x_r] = regress(x,X);
% [b, tmp, y_r] = regress(y,X);
% scatter(x_r, y_r, 70, 'filled', 'MarkerFaceColor', [.8 .8 .8]); l1 = lsline(); set(l1, 'Color',[1 .6 .6], 'LineWidth', 3);
% [r, ~, RL, RU] = corrcoef(x_r, y_r);
% if p(2)>10^-3; tit = [' p = ', num2str(round(p(2),2))]; else; tit = sprintf(' p = %.1e',p(2)); end
% disp(['r(',num2str(numel(x)-2-1),') = ', num2str(round(r(2),2)), ...
%         ', 95%CI = [',num2str(round(RL(2),2)),',',num2str(round(RU(2),2)),']',...
%         tit, ])
% 
% %% stroop interference - drift
% 
% color = [.5 .7 .8];
% 
% h = figure('units','centimeters','position',[10 10 8 7]);
%     ax{1} = subplot(1,1,1); cla; hold on;
%     x = STSWD_summary.stroop_interference.data(idxMulti_summary,1);
%     y = squeeze(nanmean(STSWD_summary.HDDM_vat.driftEEGMRI(idxMulti_summary,1:4),2));
% 	scatter(x, y, 1, 'filled', 'MarkerFaceColor', color); 
%     l1 = lsline(); set(l1, 'Color',.1+color, 'LineWidth', 3);
%     scatter(x(idxYA), y(idxYA), 50, 'square', 'filled', 'MarkerFaceColor', .2+color);
%     scatter(x(idxOA), y(idxOA), 50, 'diamond', 'filled', 'MarkerFaceColor', color);  
%     [r, p, RL, RU] = corrcoef(x, y);
% % 	legend([l1], ['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))],'location', 'SouthEast'); legend('boxoff')   
%     if p(2)>10^-3; tit = [', p = ', num2str(round(p(2),2))]; else; tit = sprintf(', p = %.1e',p(2)); end
%     title(['r(',num2str(numel(x)-2),') = ', num2str(round(r(2),2)), ...
%         ', 95%CI = [',num2str(round(RL(2),2)),',',num2str(round(RU(2),2)),']',...
%         tit, ])
%     xlabel({'stroop rt'}); ylabel({'drift rate';'(avg.)'})
%     ylim([min(y)-(.1.*(max(y)-min(y))), max(y)+(.1.*(max(y)-min(y)))])
%     xlim([min(x)-(.1.*(max(x)-min(x))), max(x)+(.1.*(max(x)-min(x)))])
%     set(h,'Color','w')
%     for indAx = 1:1; ax{indAx}.Color = [.2 .2 .2]; end
%     set(findall(gcf,'-property','FontSize'),'FontSize',14)
% h.InvertHardcopy = 'off';
% figureName = ['f_drift-l1_stroop-int'];
% saveas(h, fullfile(pn.figures, figureName), 'epsc');
% saveas(h, fullfile(pn.figures, figureName), 'png');
% 
% disp(['r(',num2str(numel(x)-2),') = ', num2str(round(r(2),2)), ...
%         ', 95%CI = [',num2str(round(RL(2),2)),',',num2str(round(RU(2),2)),']',...
%         tit, ])
%     
% % partialcorr:
% h = figure('units','centimeters','position',[10 10 8 7]);
% X = [x,y, cat(1, repmat(1,numel(find(idxYA)),1), repmat(2,numel(find(idxOA)),1))];
% [r, p] = partialcorr(X);
% X = [repmat(1,numel(x),1), cat(1, repmat(1,numel(find(idxYA)),1), repmat(2,numel(find(idxOA)),1))];
% [b, tmp, x_r] = regress(x,X);
% [b, tmp, y_r] = regress(y,X);
% scatter(x_r, y_r, 70, 'filled', 'MarkerFaceColor', [.8 .8 .8]); l1 = lsline(); set(l1, 'Color',[1 .6 .6], 'LineWidth', 3);
% [r, ~, RL, RU] = corrcoef(x_r, y_r);
% if p(2)>10^-3; tit = [' p = ', num2str(round(p(2),2))]; else; tit = sprintf(' p = %.1e',p(2)); end
% disp(['r(',num2str(numel(x)-2-1),') = ', num2str(round(r(2),2)), ...
%         ', 95%CI = [',num2str(round(RL(2),2)),',',num2str(round(RU(2),2)),']',...
%         tit, ])
%     
% %% stroop interference vs. linear drift changes
% 
% color = [.5 .7 .8];
% 
% h = figure('units','centimeters','position',[10 10 8 7]);
%     ax{1} = subplot(1,1,1); cla; hold on;
%     x = STSWD_summary.stroop_interference.data(idxMulti_summary,1);
%     y = STSWD_summary.HDDM_vat.driftEEGMRI_linear_win(idxMulti_summary)';
% 	scatter(x, y, 1, 'filled', 'MarkerFaceColor', color); 
%     l1 = lsline(); set(l1, 'Color',.1+color, 'LineWidth', 3);
%     scatter(x(idxYA), y(idxYA), 50, 'square', 'filled', 'MarkerFaceColor', .2+color);
%     scatter(x(idxOA), y(idxOA), 50, 'diamond', 'filled', 'MarkerFaceColor', color);  
%     [r, p, RL, RU] = corrcoef(x, y);
% % 	legend([l1], ['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))],'location', 'SouthEast'); legend('boxoff')   
%     if p(2)>10^-3; tit = [', p = ', num2str(round(p(2),2))]; else; tit = sprintf(', p = %.1e',p(2)); end
%     title(['r(',num2str(numel(x)-2),') = ', num2str(round(r(2),2)), ...
%         ', 95%CI = [',num2str(round(RL(2),2)),',',num2str(round(RU(2),2)),']',...
%         tit, ])
%     xlabel({'stroop rt'}); ylabel({'drift rate';'(linear mod.)'})
%     ylim([min(y)-(.1.*(max(y)-min(y))), max(y)+(.1.*(max(y)-min(y)))])
%     set(h,'Color','w')
%     for indAx = 1:1; ax{indAx}.Color = [.2 .2 .2]; end
%     set(findall(gcf,'-property','FontSize'),'FontSize',14)
% h.InvertHardcopy = 'off';
% figureName = ['f_drift-mod_stroop-int'];
% saveas(h, fullfile(pn.figures, figureName), 'epsc');
% saveas(h, fullfile(pn.figures, figureName), 'png');
% 
% disp(['r(',num2str(numel(x)-2),') = ', num2str(round(r(2),2)), ...
%         ', 95%CI = [',num2str(round(RL(2),2)),',',num2str(round(RU(2),2)),']',...
%         tit, ])
%     
% % partialcorr:
% h = figure('units','centimeters','position',[10 10 8 7]);
% X = [x,y, cat(1, repmat(1,numel(find(idxYA)),1), repmat(2,numel(find(idxOA)),1))];
% [r, p] = partialcorr(X);
% X = [repmat(1,numel(x),1), cat(1, repmat(1,numel(find(idxYA)),1), repmat(2,numel(find(idxOA)),1))];
% [b, tmp, x_r] = regress(x,X);
% [b, tmp, y_r] = regress(y,X);
% scatter(x_r, y_r, 70, 'filled', 'MarkerFaceColor', [.8 .8 .8]); l1 = lsline(); set(l1, 'Color',[1 .6 .6], 'LineWidth', 3);
% [r, ~, RL, RU] = corrcoef(x_r, y_r);
% if p(2)>10^-3; tit = [' p = ', num2str(round(p(2),2))]; else; tit = sprintf(' p = %.1e',p(2)); end
% disp(['r(',num2str(numel(x)-2-1),') = ', num2str(round(r(2),2)), ...
%         ', 95%CI = [',num2str(round(RL(2),2)),',',num2str(round(RU(2),2)),']',...
%         tit, ])
    
%% stroop rt vs. linear drift changes

color = [.5 .7 .8];

h = figure('units','centimeters','position',[10 10 8 7]);
    ax{1} = subplot(1,1,1); cla; hold on;
    x = STSWD_summary.stroop_rt_mismatch.data(idxMulti_summary,1);
    y = STSWD_summary.HDDM_vat.driftEEGMRI_linear_win(idxMulti_summary)';
	scatter(x, y, 1, 'filled', 'MarkerFaceColor', color); 
    l1 = lsline(); set(l1, 'Color',.1+color, 'LineWidth', 3);
    scatter(x(idxYA), y(idxYA), 50, 'square', 'filled', 'MarkerFaceColor', .2+color);
    scatter(x(idxOA), y(idxOA), 50, 'diamond', 'filled', 'MarkerFaceColor', color);  
    [r, p, RL, RU] = corrcoef(x, y);
% 	legend([l1], ['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))],'location', 'SouthEast'); legend('boxoff')   
    if p(2)>10^-3; tit = [', p = ', num2str(round(p(2),2))]; else; tit = sprintf(', p = %.1e',p(2)); end
    title(['r(',num2str(numel(x)-2),') = ', num2str(round(r(2),2)), ...
        ', 95%CI = [',num2str(round(RL(2),2)),',',num2str(round(RU(2),2)),']',...
        tit, ])
    xlabel({'stroop rt'}); ylabel({'drift rate';'(linear mod.)'})
    ylim([min(y)-(.1.*(max(y)-min(y))), max(y)+(.1.*(max(y)-min(y)))])
    set(h,'Color','w')
    for indAx = 1:1; ax{indAx}.Color = [.2 .2 .2]; end
    set(findall(gcf,'-property','FontSize'),'FontSize',14)
h.InvertHardcopy = 'off';
figureName = ['f_drift-mod_stroop-rt'];
saveas(h, fullfile(pn.figures, figureName), 'epsc');
saveas(h, fullfile(pn.figures, figureName), 'png');

disp(['r(',num2str(numel(x)-2),') = ', num2str(round(r(2),2)), ...
        ', 95%CI = [',num2str(round(RL(2),2)),',',num2str(round(RU(2),2)),']',...
        tit, ])
    
% partialcorr:
h = figure('units','centimeters','position',[10 10 8 7]);
z = STSWD_summary.stroop_rt.data(idxMulti_summary,1);
X = [x,y,z, cat(1, repmat(1,numel(find(idxYA)),1), repmat(2,numel(find(idxOA)),1))];
[r, p] = partialcorr(X);
X = [repmat(1,numel(x),1), cat(1, repmat(1,numel(find(idxYA)),1), repmat(2,numel(find(idxOA)),1))];
[b, tmp, x_r] = regress(x,X);
[b, tmp, y_r] = regress(y,X);
scatter(x_r, y_r, 70, 'filled', 'MarkerFaceColor', [.8 .8 .8]); l1 = lsline(); set(l1, 'Color',[1 .6 .6], 'LineWidth', 3);
[r, ~, RL, RU] = corrcoef(x_r, y_r);
if p(2)>10^-3; tit = [' p = ', num2str(round(p(2),2))]; else; tit = sprintf(' p = %.1e',p(2)); end
disp(['r(',num2str(numel(x)-2-2),') = ', num2str(round(r(2),2)), ...
        ', 95%CI = [',num2str(round(RL(2),2)),',',num2str(round(RU(2),2)),']',...
        tit, ])
