% create a read-in structure for R to perform statistical analyses

    % path management
    currentFile = mfilename('fullpath');
    [pathstr,~,~] = fileparts(currentFile);
    cd(fullfile(pathstr,'..'))

    pn.root = pwd;
    pn.data_summary = fullfile(pn.root, 'data');
    pn.tools = fullfile(pn.root, 'tools');
        addpath(fullfile(pn.tools, 'cell2csv'))

%% select subjects

    filename = fullfile(rootpath, 'code', 'id_list.txt');
    fileID = fopen(filename);
    IDs = textscan(fileID,'%s');
    fclose(fileID);
    IDs = IDs{1};

%% load summary structure

    load(fullfile(pn.data_summary, 'STSWD_summary_YAOA.mat'), 'STSWD_summary')

%% reorganize for R input

    % select full EEG sample
    idx_Summary = find(ismember(STSWD_summary.IDs, IDs));
    [STSWD_summary.IDs(idx_Summary),IDs]

    age(cellfun(@str2num, IDs, 'un', 1)<2000,1) = 1;
    age(cellfun(@str2num, IDs, 'un', 1)>2000,1) = 2;
        
    dataS.id = IDs;
    dataS.age = age;
    dataS.drift = STSWD_summary.HDDM_vat.driftEEGMRI_linear_win(idx_Summary)';
    dataS.nondecision = STSWD_summary.HDDM_vat.nondecisionEEGMRI_linear_win(idx_Summary)';
    dataS.threshold = STSWD_summary.HDDM_vat.thresholdEEGMRI_linear_win(idx_Summary)';
    dataS.pupil = STSWD_summary.pupil_LV1.linear_win(idx_Summary)';
    
    dataS.cppslopes = STSWD_summary.CPPslopes.linear_win(idx_Summary)';
    dataS.betaslopes = STSWD_summary.BetaSlope.linear_win(idx_Summary)';
    
    dataS.excitability = STSWD_summary.excitability_LV1.linear_win(idx_Summary)';
    dataS.sampen = STSWD_summary.SE_LV1.linear_win(idx_Summary)';
    dataS.fooof = STSWD_summary.fooof_LV1.linear_win(idx_Summary)';
    dataS.alpha = STSWD_summary.tfr_alpha_LV1.linear_win(idx_Summary)';
    dataS.theta = STSWD_summary.tfr_theta_LV1.linear_win(idx_Summary)';
    dataS.gamma = STSWD_summary.tfr_gamma_LV1.linear_win(idx_Summary)';
    dataS.prestimalpha = STSWD_summary.tfr_prestim_alpha_LV1.linear_win(idx_Summary)';
    dataS.ssvep = STSWD_summary.SSVEPmag_norm.linear_win(idx_Summary)';
    
    dataS.behav_brainscore = STSWD_summary.behavPLS_BrainS_LV1.linear_win(idx_Summary)';
    dataS.behav_behavscore = STSWD_summary.behavPLS_BehavS_LV1.linear_win(idx_Summary)';

    % combine in data matrix
    data = [];
    data = [dataS.age, ...
        dataS.drift, ...
        dataS.nondecision, ...
        dataS.threshold, ...
        dataS.pupil, ...
        dataS.cppslopes, ...
        dataS.betaslopes, ...
        dataS.excitability, ...
        dataS.sampen, ...
        dataS.fooof, ...
        dataS.alpha, ...
        dataS.theta, ...
        dataS.gamma, ...
        dataS.prestimalpha, ...
        dataS.ssvep, ...
        dataS.behav_brainscore, ...
        dataS.behav_behavscore];
    data = num2cell(data);
    data = [dataS.id, data];

    % add headers
    data = [{'id'},{'age'},{'drift'},{'ndt'},{'threshold'},{'pupil'}, ...
       {'cppslopes'},{'betaslopes'},{'excitability'},{'sampen'},{'fooof'},{'alpha'},{'theta'}, ...
       {'gamma'},{'prestimalpha'},{'ssvep'},{'behav_brainscore'}, {'behav_behavscore'};  data];

    %% save for R

    cell2csv(fullfile(pn.data_summary, 'STSWDsummary_YAOA_linEffects_forR.dat'),data)

    save(fullfile(pn.data_summary, 'STSWDsummary_YAOA_linEffects_forR.mat'),'data')
