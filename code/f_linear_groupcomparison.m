% assess inter-individual relations between first level linear effects 

% path management
currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..'))

rootpath = pwd;
pn.data_summary = fullfile(rootpath, 'data');
pn.figures = fullfile(rootpath, 'figures', 'linearbyage'); mkdir(pn.figures);

filename = fullfile(rootpath, 'code', 'id_list.txt');
fileID = fopen(filename);
IDs = textscan(fileID,'%s');
fclose(fileID);
IDs = IDs{1};

filename = fullfile(rootpath, 'code', 'id_list_eegmr.txt');
fileID = fopen(filename);
IDs_EEGMR = textscan(fileID,'%s');
fclose(fileID);
IDs_EEGMR = IDs_EEGMR{1};

%% load summary structure

load(fullfile(pn.data_summary, 'STSWD_summary_YAOA.mat'), 'STSWD_summary')

% select subjects (EEG only)
idxEEG_summary = ismember(STSWD_summary.IDs, IDs);
[STSWD_summary.IDs(idxEEG_summary), IDs]

% select subjects (multimodal only)
idxMulti_summary = ismember(STSWD_summary.IDs, IDs_EEGMR);
[STSWD_summary.IDs(idxMulti_summary), IDs_EEGMR]

%% define YA and OA

idx_YA = cellfun(@str2num, STSWD_summary.IDs)<2000;
idx_OA = cellfun(@str2num, STSWD_summary.IDs)>2000;

%% group comparison of linear slopes

fields_name = {'driftEEGMRI_linear_win', ...
   'thresholdEEGMRI_linear_win',...
   'nondecisionEEGMRI_linear_win'};

fields_title = {'drift', ...
   'threshold',...
   'nondecision'};

data2Plot = cell(2,numel(fields_name));
for indParam = 1:numel(fields_name)
    tmp = STSWD_summary.HDDM_vat.(fields_name{indParam});
    tmp(tmp==0)=NaN;
    data2Plot{1,indParam} = tmp(idx_YA&idxMulti_summary);
    data2Plot{2,indParam} = tmp(idx_OA&idxMulti_summary);
end

% plot linear estimates using bar charts with individual values

colorm = [.6 .6 .6; 1 .6 .6; .6 .8 1];

for indParam = 1:numel(fields_name)
    h = figure('units','centimeters','position',[.1 .1 7 10]);
    plot_data{1} = data2Plot{1,indParam};
    plot_data{2} = data2Plot{2,indParam};
    set(gcf,'renderer','Painters')
    cla;
    hold on;
    for indGroup = 1:2
        if ttest(plot_data{indGroup})==1 & nanmean(plot_data{indGroup})>0
            bar(indGroup, nanmean(plot_data{indGroup}), 'FaceColor', colorm(2,:), 'EdgeColor', 'none', 'BarWidth', 0.8);
        elseif ttest(plot_data{indGroup})==1 & nanmean(plot_data{indGroup})<0
            bar(indGroup, nanmean(plot_data{indGroup}), 'FaceColor', colorm(3,:), 'EdgeColor', 'none', 'BarWidth', 0.8);
        elseif ttest(plot_data{indGroup})==0
            bar(indGroup, nanmean(plot_data{indGroup}), 'FaceColor', colorm(1,:), 'EdgeColor', 'none', 'BarWidth', 0.8);
        end
        % plot individual values on top
        scatter(repmat(indGroup,numel(plot_data{indGroup}),1)+(rand(numel(plot_data{indGroup}),1)-.5).*.4,...
            plot_data{indGroup}, 20, 'filled', 'MarkerFaceColor', [.3 .3 .3]);
    end
    xlim([.25 2.75]); %ylim([1.3 2])
    set(gca,'xtick',[1,2]); set(gca,'xTickLabel',{'YA'; 'OA'}); xlabel('Age Group'); ylabel([fields_title{indParam},' linear'])
    set(findall(gcf,'-property','FontSize'),'FontSize',20)
    [htest, p] = ttest2(plot_data{1}, plot_data{2});
    if p>10^-3
        title(['p = ', num2str(round(p,2))]);
    else
        tit = sprintf('p = %.1e',p);
        title(tit);
    end
    figureName = ['linearcomp_', fields_title{indParam}];
    saveas(h, fullfile(pn.figures, figureName), 'epsc');
    saveas(h, fullfile(pn.figures, figureName), 'png');
end

%% excitability

fields_name = {'excitability_LV1', ...
   'tfr_theta_LV1',...
   'tfr_alpha_LV1',...
   'tfr_gamma_LV1', ...
   'tfr_prestim_alpha_LV1', ...
   'pupil_LV1', ...
   'CPPslopes', ...
   'BetaSlope', ...
   'CPPthreshold', ...
   'BetaThreshold', ...
   'SSVEPmag_norm', ...
   'SE_LV1', ...
   'fooof_LV1', ...
   'SPM_lv1', ...
   'behavPLS_BrainS_LV1',...
   'behavPLS_BehavS_LV1'};

fields_title = {'excitability', ...
   'theta',...
   'alpha',...
   'gamma', ...
   'prestimalpha', ...
   'pupil', ...
   'cpp', ...
   'betaslope', ...
   'cppthresh', ...
   'betathresh', ...
   'ssvep', ...
   'sampen', ...
   '1f', ...
   'spmlv1', ...
   'bs_brain', ...
   'bs_behav'};

data2Plot = cell(2,numel(fields_name));
for indParam = 1:numel(fields_name)
    if isfield(STSWD_summary.(fields_name{indParam}), 'linear_win')
        tmp = STSWD_summary.(fields_name{indParam}).linear_win;
    elseif isfield(STSWD_summary.(fields_name{indParam}), 'linear')
        tmp = STSWD_summary.(fields_name{indParam}).linear;
    end
    %tmp(tmp==0)=NaN;
    data2Plot{1,indParam} = tmp(idx_YA&idxMulti_summary);
    data2Plot{2,indParam} = tmp(idx_OA&idxMulti_summary);
end

% plot linear estimates using bar charts with individual values

colorm = [.6 .6 .6; 1 .6 .6; .6 .8 1];

for indParam = 1:numel(fields_name)
    h = figure('units','centimeters','position',[.1 .1 7 10]);
    plot_data{1} = data2Plot{1,indParam};
    plot_data{2} = data2Plot{2,indParam};
    set(gcf,'renderer','Painters')
    cla;
    hold on;
    for indGroup = 1:2
        if ttest(plot_data{indGroup})==1 & nanmean(plot_data{indGroup})>0
            bar(indGroup, nanmean(plot_data{indGroup}), 'FaceColor', colorm(2,:), 'EdgeColor', 'none', 'BarWidth', 0.8);
        elseif ttest(plot_data{indGroup})==1 & nanmean(plot_data{indGroup})<0
            bar(indGroup, nanmean(plot_data{indGroup}), 'FaceColor', colorm(3,:), 'EdgeColor', 'none', 'BarWidth', 0.8);
        elseif ttest(plot_data{indGroup})==0
            bar(indGroup, nanmean(plot_data{indGroup}), 'FaceColor', colorm(1,:), 'EdgeColor', 'none', 'BarWidth', 0.8);
        end
        % plot individual values on top
        scatter(repmat(indGroup,numel(plot_data{indGroup}),1)+(rand(numel(plot_data{indGroup}),1)-.5).*.4,...
            plot_data{indGroup}, 20, 'filled', 'MarkerFaceColor', [.3 .3 .3]);
    end
    xlim([.25 2.75]); %ylim([1.3 2])
    set(gca,'xtick',[1,2]); set(gca,'xTickLabel',{'YA'; 'OA'}); xlabel('Age Group'); ylabel([fields_title{indParam},' linear'])
    set(findall(gcf,'-property','FontSize'),'FontSize',20)
    [htest, p] = ttest2(plot_data{1}, plot_data{2});
    if p>10^-3
        title(['p = ', num2str(round(p,2))]);
    else
        tit = sprintf('p = %.1e',p);
        title(tit);
    end
    figureName = ['linearcomp_', fields_title{indParam}];
    saveas(h, fullfile(pn.figures, figureName), 'epsc');
    saveas(h, fullfile(pn.figures, figureName), 'png');
end