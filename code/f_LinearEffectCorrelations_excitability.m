% assess inter-individual relations between first level linear effects 

% path management
currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..'))

rootpath = pwd;
pn.data_summary = fullfile(rootpath, 'data');
pn.figures = fullfile(rootpath, 'figures', 'correlations'); mkdir(pn.figures);

filename = fullfile(rootpath, 'code', 'id_list.txt');
fileID = fopen(filename);
IDs = textscan(fileID,'%s');
fclose(fileID);
IDs = IDs{1};

filename = fullfile(rootpath, 'code', 'id_list_eegmr.txt');
fileID = fopen(filename);
IDs_EEGMR = textscan(fileID,'%s');
fclose(fileID);
IDs_EEGMR = IDs_EEGMR{1};

%% load summary structure

load(fullfile(pn.data_summary, 'STSWD_summary_YAOA.mat'), 'STSWD_summary')

% select subjects (EEG only)
idxEEG_summary = ismember(STSWD_summary.IDs, IDs);
[STSWD_summary.IDs(idxEEG_summary), IDs]

% select subjects (multimodal only)
idxMulti_summary = ismember(STSWD_summary.IDs, IDs_EEGMR);
[STSWD_summary.IDs(idxMulti_summary), IDs_EEGMR]

idxYA = str2double(STSWD_summary.IDs(idxMulti_summary))<2000;
idxOA = str2double(STSWD_summary.IDs(idxMulti_summary))>=2000;

%% perform 2nd level correlations

color = [.8 .5 .5];

h = figure('units','centimeters','position',[10 10 9 7]);
set(0, 'DefaultFigureRenderer', 'painters');
    ax{1} = subplot(1,1,1); cla; hold on;
    x = STSWD_summary.tfr_alpha_LV1.linear_win(idxMulti_summary);
    y = STSWD_summary.SE_LV1.linear_win(idxMulti_summary);
    scatter(x, y, 1, 'filled', 'MarkerFaceColor', color); 
    l1 = lsline(); set(l1, 'Color',.1+color, 'LineWidth', 3);
    scatter(x(idxYA), y(idxYA), 50, 'square', 'filled', 'MarkerFaceColor', .2+color);
    scatter(x(idxOA), y(idxOA), 50, 'diamond', 'filled', 'MarkerFaceColor', color); 
	[r, p] = corrcoef(x, y);
    p = p(2);   
    if p>10^-3; tit = [' p = ', num2str(round(p,2))]; else; tit = sprintf(' p = %.1e',p); end
    title(['r = ', num2str(round(r(2),2)), tit])
    xlabel({'alpha bs';'(linear modulation)'}); ylabel({'sampen';'(linear modulation)'})
    ylim([min(y)-(.1.*(max(y)-min(y))), max(y)+(.1.*(max(y)-min(y)))])
    set(h,'Color','w')
    for indAx = 1:1; ax{indAx}.Color = [.2 .2 .2]; end
    set(findall(gcf,'-property','FontSize'),'FontSize',14)
h.InvertHardcopy = 'off';
figureName = ['f_sampen_alpha'];
saveas(h, fullfile(pn.figures, figureName), 'epsc');
saveas(h, fullfile(pn.figures, figureName), 'png');

h = figure('units','centimeters','position',[10 10 9 7]);
set(0, 'DefaultFigureRenderer', 'painters');
    ax{1} = subplot(1,1,1); cla; hold on;
    x = STSWD_summary.fooof_LV1.linear_win(idxMulti_summary);
    y = STSWD_summary.SE_LV1.linear_win(idxMulti_summary);
    scatter(x, y, 1, 'filled', 'MarkerFaceColor', color); 
    l1 = lsline(); set(l1, 'Color',.1+color, 'LineWidth', 3);
    scatter(x(idxYA), y(idxYA), 50, 'square', 'filled', 'MarkerFaceColor', .2+color);
    scatter(x(idxOA), y(idxOA), 50, 'diamond', 'filled', 'MarkerFaceColor', color); 
	[r, p] = corrcoef(x, y);
    p = p(2);   
    if p>10^-3; tit = [' p = ', num2str(round(p,2))]; else; tit = sprintf(' p = %.1e',p); end
    title(['r = ', num2str(round(r(2),2)), tit])
    xlabel({'fooof bs';'(linear modulation)'}); ylabel({'sampen';'(linear modulation)'})
    ylim([min(y)-(.1.*(max(y)-min(y))), max(y)+(.1.*(max(y)-min(y)))])
    set(h,'Color','w')
    for indAx = 1:1; ax{indAx}.Color = [.2 .2 .2]; end
    set(findall(gcf,'-property','FontSize'),'FontSize',14)
h.InvertHardcopy = 'off';
figureName = ['f_sampen_fooof'];
saveas(h, fullfile(pn.figures, figureName), 'epsc');
saveas(h, fullfile(pn.figures, figureName), 'png');
    
h = figure('units','centimeters','position',[10 10 9 7]);
set(0, 'DefaultFigureRenderer', 'painters');
    ax{1} = subplot(1,1,1); cla; hold on;
    x = STSWD_summary.tfr_alpha_LV1.linear_win(idxMulti_summary);
    y = STSWD_summary.fooof_LV1.linear_win(idxMulti_summary);
    scatter(x, y, 1, 'filled', 'MarkerFaceColor', color); 
    l1 = lsline(); set(l1, 'Color',.1+color, 'LineWidth', 3);
    scatter(x(idxYA), y(idxYA), 50, 'square', 'filled', 'MarkerFaceColor', .2+color);
    scatter(x(idxOA), y(idxOA), 50, 'diamond', 'filled', 'MarkerFaceColor', color); 
	[r, p] = corrcoef(x, y);
    p = p(2);   
    if p>10^-3; tit = [' p = ', num2str(round(p,2))]; else; tit = sprintf(' p = %.1e',p); end
    title(['r = ', num2str(round(r(2),2)), tit])
    xlabel({'alpha bs';'(linear modulation)'}); ylabel({'fooof';'(linear modulation)'})
    ylim([min(y)-(.1.*(max(y)-min(y))), max(y)+(.1.*(max(y)-min(y)))])
    set(h,'Color','w')
    for indAx = 1:1; ax{indAx}.Color = [.2 .2 .2]; end
    set(findall(gcf,'-property','FontSize'),'FontSize',14)
h.InvertHardcopy = 'off';
figureName = ['f_alpha_fooof'];
saveas(h, fullfile(pn.figures, figureName), 'epsc');
saveas(h, fullfile(pn.figures, figureName), 'png');

%% correlation with drift rates

color = [.8 .5 .5];

h = figure('units','centimeters','position',[10 10 8 7]);
set(0, 'DefaultFigureRenderer', 'painters');
    ax{1} = subplot(1,1,1); cla; hold on;
    x = STSWD_summary.tfr_alpha_LV1.linear_win(idxMulti_summary);
    y = STSWD_summary.HDDM_vat.driftEEGMRI(idxMulti_summary,1);
    scatter(x, y, 1, 'filled', 'MarkerFaceColor', color); 
    l1 = lsline(); set(l1, 'Color',.1+color, 'LineWidth', 3);
    scatter(x(idxYA), y(idxYA), 50, 'square', 'filled', 'MarkerFaceColor', .2+color);
    scatter(x(idxOA), y(idxOA), 50, 'diamond', 'filled', 'MarkerFaceColor', color); 
	[r, p] = corrcoef(x, y);
    p = p(2);   
    if p>10^-3; tit = [' p = ', num2str(round(p,2))]; else; tit = sprintf(' p = %.1e',p); end
    title(['r = ', num2str(round(r(2),2)), tit])
    xlabel({'alpha bs';'(linear modulation)'}); ylabel({'drift l1'})
    ylim([min(y)-(.1.*(max(y)-min(y))), max(y)+(.1.*(max(y)-min(y)))])
    set(h,'Color','w')
    for indAx = 1:1; ax{indAx}.Color = [.2 .2 .2]; end
    set(findall(gcf,'-property','FontSize'),'FontSize',14)
h.InvertHardcopy = 'off';
figureName = ['f_alpha_driftl1'];
saveas(h, fullfile(pn.figures, figureName), 'epsc');
saveas(h, fullfile(pn.figures, figureName), 'png');

color = [.5 .7 .8];

h = figure('units','centimeters','position',[10 10 9 7]);
set(0, 'DefaultFigureRenderer', 'painters');
    ax{1} = subplot(1,1,1); cla; hold on;
    x = STSWD_summary.tfr_alpha_LV1.linear_win(idxMulti_summary);
    y = STSWD_summary.HDDM_vat.driftEEGMRI_linear_win(idxMulti_summary);
    scatter(x, y, 1, 'filled', 'MarkerFaceColor', color); 
    l1 = lsline(); set(l1, 'Color',.1+color, 'LineWidth', 3);
    scatter(x(idxYA), y(idxYA), 50, 'square', 'filled', 'MarkerFaceColor', .2+color);
    scatter(x(idxOA), y(idxOA), 50, 'diamond', 'filled', 'MarkerFaceColor', color); 
	[r, p] = corrcoef(x, y);
    p = p(2);   
    if p>10^-3; tit = [' p = ', num2str(round(p,2))]; else; tit = sprintf(' p = %.1e',p); end
    title(['r = ', num2str(round(r(2),2)), tit])
    xlabel({'alpha bs';'(linear modulation)'}); ylabel({'drift';'(linear modulation)'})
    ylim([min(y)-(.1.*(max(y)-min(y))), max(y)+(.1.*(max(y)-min(y)))])
    set(h,'Color','w')
    for indAx = 1:1; ax{indAx}.Color = [.2 .2 .2]; end
    set(findall(gcf,'-property','FontSize'),'FontSize',14)
h.InvertHardcopy = 'off';
figureName = ['f_alpha_drift'];
saveas(h, fullfile(pn.figures, figureName), 'epsc');
saveas(h, fullfile(pn.figures, figureName), 'png');

h = figure('units','centimeters','position',[10 10 9 7]);
set(0, 'DefaultFigureRenderer', 'painters');
    ax{1} = subplot(1,1,1); cla; hold on;
    x = STSWD_summary.SE_LV1.linear_win(idxMulti_summary);
    y = STSWD_summary.HDDM_vat.driftEEGMRI_linear_win(idxMulti_summary);
    scatter(x, y, 1, 'filled', 'MarkerFaceColor', color); 
    l1 = lsline(); set(l1, 'Color',.1+color, 'LineWidth', 3);
    scatter(x(idxYA), y(idxYA), 50, 'square', 'filled', 'MarkerFaceColor', .2+color);
    scatter(x(idxOA), y(idxOA), 50, 'diamond', 'filled', 'MarkerFaceColor', color); 
	[r, p] = corrcoef(x, y);
    p = p(2);   
    if p>10^-3; tit = [' p = ', num2str(round(p,2))]; else; tit = sprintf(' p = %.1e',p); end
    title(['r = ', num2str(round(r(2),2)), tit])
    xlabel({'sampen bs';'(linear modulation)'}); ylabel({'drift';'(linear modulation)'})
    ylim([min(y)-(.1.*(max(y)-min(y))), max(y)+(.1.*(max(y)-min(y)))])
    set(h,'Color','w')
    for indAx = 1:1; ax{indAx}.Color = [.2 .2 .2]; end
    set(findall(gcf,'-property','FontSize'),'FontSize',14)
h.InvertHardcopy = 'off';
figureName = ['f_sampen_drift'];
saveas(h, fullfile(pn.figures, figureName), 'epsc');
saveas(h, fullfile(pn.figures, figureName), 'png');

h = figure('units','centimeters','position',[10 10 9 7]);
set(0, 'DefaultFigureRenderer', 'painters');
    ax{1} = subplot(1,1,1); cla; hold on;
    x = STSWD_summary.fooof_LV1.linear_win(idxMulti_summary);
    y = STSWD_summary.HDDM_vat.driftEEGMRI_linear_win(idxMulti_summary);
    scatter(x, y, 1, 'filled', 'MarkerFaceColor', color); 
    l1 = lsline(); set(l1, 'Color',.1+color, 'LineWidth', 3);
    scatter(x(idxYA), y(idxYA), 50, 'square', 'filled', 'MarkerFaceColor', .2+color);
    scatter(x(idxOA), y(idxOA), 50, 'diamond', 'filled', 'MarkerFaceColor', color); 
	[r, p] = corrcoef(x, y);
    p = p(2);   
    if p>10^-3; tit = [' p = ', num2str(round(p,2))]; else; tit = sprintf(' p = %.1e',p); end
    title(['r = ', num2str(round(r(2),2)), tit])
    xlabel({'fooof bs';'(linear modulation)'}); ylabel({'drift';'(linear modulation)'})
    ylim([min(y)-(.1.*(max(y)-min(y))), max(y)+(.1.*(max(y)-min(y)))])
    set(h,'Color','w')
    for indAx = 1:1; ax{indAx}.Color = [.2 .2 .2]; end
    set(findall(gcf,'-property','FontSize'),'FontSize',14)
h.InvertHardcopy = 'off';
figureName = ['f_fooof_drift'];
saveas(h, fullfile(pn.figures, figureName), 'epsc');
saveas(h, fullfile(pn.figures, figureName), 'png');

%% pupil and theta

color = [.8 .5 .5];

h = figure('units','centimeters','position',[10 10 8 7]);
    ax{1} = subplot(1,1,1); cla; hold on;
    x = STSWD_summary.pupil_LV1.linear_win(idxMulti_summary)';
    y = STSWD_summary.tfr_theta_LV1.linear_win(idxMulti_summary)';
	scatter(x, y, 1, 'filled', 'MarkerFaceColor', color); 
    l1 = lsline(); set(l1, 'Color',.1+color, 'LineWidth', 3);
    scatter(x(idxYA), y(idxYA), 50, 'square', 'filled', 'MarkerFaceColor', .2+color);
    scatter(x(idxOA), y(idxOA), 50, 'diamond', 'filled', 'MarkerFaceColor', color);  
    [r, p, RL, RU] = corrcoef(x, y);
% 	legend([l1], ['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))],'location', 'SouthEast'); legend('boxoff')   
    if p(2)>10^-3; tit = [', p = ', num2str(round(p(2),2))]; else; tit = sprintf(', p = %.1e',p(2)); end
    title(['r(',num2str(numel(x)-2),') = ', num2str(round(r(2),2)), ...
        ', 95%CI = [',num2str(round(RL(2),2)),',',num2str(round(RU(2),2)),']',...
        tit, ])
    xlabel({'pupil bs';'(linear modulation)'}); ylabel({'theta';'(linear modulation)'})
    ylim([min(y)-(.1.*(max(y)-min(y))), max(y)+(.1.*(max(y)-min(y)))])
    set(h,'Color','w')
    for indAx = 1:1; ax{indAx}.Color = [.2 .2 .2]; end
    set(findall(gcf,'-property','FontSize'),'FontSize',14)
h.InvertHardcopy = 'off';
figureName = ['f_pupil_theta'];
saveas(h, fullfile(pn.figures, figureName), 'epsc');
saveas(h, fullfile(pn.figures, figureName), 'png');

disp(['r(',num2str(numel(x)-2),') = ', num2str(round(r(2),2)), ...
        ', 95%CI = [',num2str(round(RL(2),2)),',',num2str(round(RU(2),2)),']',...
        tit, ])
    
% partialcorr:
h = figure('units','centimeters','position',[10 10 8 7]);
X = [x,y, cat(1, repmat(1,numel(find(idxYA)),1), repmat(2,numel(find(idxOA)),1))];
[r, p] = partialcorr(X);
X = [repmat(1,numel(x),1), cat(1, repmat(1,numel(find(idxYA)),1), repmat(2,numel(find(idxOA)),1))];
[b, tmp, x_r] = regress(x,X);
[b, tmp, y_r] = regress(y,X);
scatter(x_r, y_r, 70, 'filled', 'MarkerFaceColor', [.8 .8 .8]); l1 = lsline(); set(l1, 'Color',[1 .6 .6], 'LineWidth', 3);
[r, ~, RL, RU] = corrcoef(x_r, y_r);
if p(2)>10^-3; tit = [' p = ', num2str(round(p(2),3))]; else; tit = sprintf(' p = %.1e',p(2)); end
disp(['r(',num2str(numel(x)-2-1),') = ', num2str(round(r(2),2)), ...
        ', 95%CI = [',num2str(round(RL(2),3)),',',num2str(round(RU(2),3)),']',...
        tit, ])


% h = figure('units','centimeters','position',[10 10 8 7]);
% set(0, 'DefaultFigureRenderer', 'painters');
%     ax{1} = subplot(1,1,1); cla; hold on;
%     x = squeeze(nanmean(cat(1, STSWD_summary.pupil_LV1.linear_win(idxMulti_summary),...
%         STSWD_summary.tfr_theta_LV1.linear_win(idxMulti_summary)),1));
%     y = STSWD_summary.HDDM_vat.driftEEGMRI(idxMulti_summary,1);
%     scatter(x, y, 1, 'filled', 'MarkerFaceColor', color); 
%     l1 = lsline(); set(l1, 'Color',.1+color, 'LineWidth', 3);
%     scatter(x(idxYA), y(idxYA), 50, 'square', 'filled', 'MarkerFaceColor', .2+color);
%     scatter(x(idxOA), y(idxOA), 50, 'diamond', 'filled', 'MarkerFaceColor', color); 
% 	[r, p] = corrcoef(x, y);
%     p = p(2);   
%     if p>10^-3; tit = [' p = ', num2str(round(p,2))]; else; tit = sprintf(' p = %.1e',p); end
%     title(['r = ', num2str(round(r(2),2)), tit])
%     xlabel({'avg. pupil theta bs';'(linear modulation)'}); ylabel({'drift l1'})
%     ylim([min(y)-(.1.*(max(y)-min(y))), max(y)+(.1.*(max(y)-min(y)))])
%     set(h,'Color','w')
%     for indAx = 1:1; ax{indAx}.Color = [.2 .2 .2]; end
%     set(findall(gcf,'-property','FontSize'),'FontSize',14)
% h.InvertHardcopy = 'off';
% figureName = ['f_avgpupiltheta_drift'];
% saveas(h, fullfile(pn.figures, figureName), 'epsc');
% saveas(h, fullfile(pn.figures, figureName), 'png');