% assess inter-individual relations between first level linear effects 

% path management
currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..'))

rootpath = pwd;
pn.data_summary = fullfile(rootpath, 'data');

filename = fullfile(rootpath, 'code', 'id_list.txt');
fileID = fopen(filename);
IDs = textscan(fileID,'%s');
fclose(fileID);
IDs = IDs{1};

filename = fullfile(rootpath, 'code', 'id_list_eegmr.txt');
fileID = fopen(filename);
IDs_EEGMR = textscan(fileID,'%s');
fclose(fileID);
IDs_EEGMR = IDs_EEGMR{1};

filename = fullfile(rootpath, 'code', 'id_list_eegmr_completeruns.txt');
fileID = fopen(filename);
IDs_EEGMRcomp = textscan(fileID,'%s');
fclose(fileID);
IDs_EEGMRcomp = IDs_EEGMRcomp{1};

%% load summary structure

load(fullfile(pn.data_summary, 'STSWD_summary_YAOA.mat'), 'STSWD_summary')

% select subjects (EEG only)
idxEEG_summary = ismember(STSWD_summary.IDs, IDs);
[STSWD_summary.IDs(idxEEG_summary), IDs]

% select subjects (multimodal only)
idxMulti_summary = ismember(STSWD_summary.IDs, IDs_EEGMR);
[STSWD_summary.IDs(idxMulti_summary), IDs_EEGMR]

% select subjects (multimodal with complete runs only)
idxMultiComplete_summary = ismember(STSWD_summary.IDs, IDs_EEGMRcomp);
[STSWD_summary.IDs(idxMultiComplete_summary), IDs_EEGMRcomp]
    
%% perform 2nd level correlations

% excitability score vs drift rate

h = figure('units','normalized','position',[.1 .1 .175 .25]);
    ax{1} = subplot(1,1,1); cla; hold on;
    x = STSWD_summary.excitability_LV1.linear_win(idxMulti_summary);
    y = STSWD_summary.HDDM_vat.driftEEGMRI_linear_win(idxMulti_summary);
	scatter(x, y, 70, 'filled', 'MarkerFaceColor', [1 .6 .6]); l1 = lsline(); set(l1, 'Color',[1 .6 .6], 'LineWidth', 3);
	[r, p] = corrcoef(x, y);
% 	legend([l1], ['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))],'location', 'SouthEast'); legend('boxoff')   
    title(['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))])
    xlabel({'excitability bs';'(linear modulation)'}); ylabel({'Drift rate';'(linear modulation)'})
    ylim([min(y)-(.1.*(max(y)-min(y))), max(y)+(.1.*(max(y)-min(y)))])
    set(h,'Color','w')
    for indAx = 1:1; ax{indAx}.Color = [.2 .2 .2]; end
    set(findall(gcf,'-property','FontSize'),'FontSize',25)
    
h = figure('units','normalized','position',[.1 .1 .175 .25]);
    ax{1} = subplot(1,1,1); cla; hold on;
    x = STSWD_summary.excitability_LV1.linear_win(idxMulti_summary);
    y = STSWD_summary.SPM_lv1.linear_win(idxMulti_summary);
	scatter(x, y, 70, 'filled', 'MarkerFaceColor', [1 .6 .6]); l1 = lsline(); set(l1, 'Color',[1 .6 .6], 'LineWidth', 3);
	[r, p] = corrcoef(x, y);
% 	legend([l1], ['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))],'location', 'SouthEast'); legend('boxoff')   
    title(['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))])
    xlabel({'excitability bs';'(linear modulation)'}); ylabel({'spm lv1';'(linear modulation)'})
    ylim([min(y)-(.1.*(max(y)-min(y))), max(y)+(.1.*(max(y)-min(y)))])
    set(h,'Color','w')
    for indAx = 1:1; ax{indAx}.Color = [.2 .2 .2]; end
    set(findall(gcf,'-property','FontSize'),'FontSize',25)
    
h = figure('units','normalized','position',[.1 .1 .175 .25]);
    ax{1} = subplot(1,1,1); cla; hold on;
    x = STSWD_summary.excitability_LV1.linear_win(idxMulti_summary);
    y = STSWD_summary.pupil_LV1.linear_win(idxMulti_summary);
	scatter(x, y, 70, 'filled', 'MarkerFaceColor', [1 .6 .6]); l1 = lsline(); set(l1, 'Color',[1 .6 .6], 'LineWidth', 3);
	[r, p] = corrcoef(x, y);
% 	legend([l1], ['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))],'location', 'SouthEast'); legend('boxoff')   
    title(['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))])
    xlabel({'excitability bs';'(linear modulation)'}); ylabel({'pupil';'(linear modulation)'})
    ylim([min(y)-(.1.*(max(y)-min(y))), max(y)+(.1.*(max(y)-min(y)))])
    set(h,'Color','w')
    for indAx = 1:1; ax{indAx}.Color = [.2 .2 .2]; end
    set(findall(gcf,'-property','FontSize'),'FontSize',25)
    
h = figure('units','normalized','position',[.1 .1 .175 .25]);
    ax{1} = subplot(1,1,1); cla; hold on;
    x = STSWD_summary.excitability_LV1.linear_win(idxMulti_summary);
    y = STSWD_summary.CPPslopes.linear_win(idxMulti_summary);
	scatter(x, y, 70, 'filled', 'MarkerFaceColor', [1 .6 .6]); l1 = lsline(); set(l1, 'Color',[1 .6 .6], 'LineWidth', 3);
	[r, p] = corrcoef(x, y);
% 	legend([l1], ['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))],'location', 'SouthEast'); legend('boxoff')   
    title(['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))])
    xlabel({'excitability bs';'(linear modulation)'}); ylabel({'CPP slope';'(linear modulation)'})
    ylim([min(y)-(.1.*(max(y)-min(y))), max(y)+(.1.*(max(y)-min(y)))])
    set(h,'Color','w')
    for indAx = 1:1; ax{indAx}.Color = [.2 .2 .2]; end
    set(findall(gcf,'-property','FontSize'),'FontSize',25)
    
h = figure('units','normalized','position',[.1 .1 .175 .25]);
    ax{1} = subplot(1,1,1); cla; hold on;
    x = STSWD_summary.excitability_LV1.linear_win(idxMulti_summary);
    y = STSWD_summary.tfr_gamma_LV1.linear_win(idxMulti_summary);
	scatter(x, y, 70, 'filled', 'MarkerFaceColor', [1 .6 .6]); l1 = lsline(); set(l1, 'Color',[1 .6 .6], 'LineWidth', 3);
	[r, p] = corrcoef(x, y);
% 	legend([l1], ['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))],'location', 'SouthEast'); legend('boxoff')   
    title(['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))])
    xlabel({'excitability bs';'(linear modulation)'}); ylabel({'gamma bs';'(linear modulation)'})
    ylim([min(y)-(.1.*(max(y)-min(y))), max(y)+(.1.*(max(y)-min(y)))])
    set(h,'Color','w')
    for indAx = 1:1; ax{indAx}.Color = [.2 .2 .2]; end
    set(findall(gcf,'-property','FontSize'),'FontSize',25)

%% pupil and theta

h = figure('units','normalized','position',[.1 .1 .175 .25]);
    ax{1} = subplot(1,1,1); cla; hold on;
    x = STSWD_summary.tfr_theta_LV1.linear_win(idxMulti_summary);
    y = STSWD_summary.pupil_LV1.linear_win(idxMulti_summary);
	scatter(x, y, 70, 'filled', 'MarkerFaceColor', [1 .6 .6]); l1 = lsline(); set(l1, 'Color',[1 .6 .6], 'LineWidth', 3);
	[r, p] = corrcoef(x, y);
    title(['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))])
    xlabel({'theta bs';'(linear modulation)'}); ylabel({'pupil bs';'(linear modulation)'})
    ylim([min(y)-(.1.*(max(y)-min(y))), max(y)+(.1.*(max(y)-min(y)))])
    set(h,'Color','w')
    for indAx = 1:1; ax{indAx}.Color = [.2 .2 .2]; end
    set(findall(gcf,'-property','FontSize'),'FontSize',25)
    
%% tfr - drift

h = figure('units','normalized','position',[.1 .1 .175 .25]);
    ax{1} = subplot(1,1,1); cla; hold on;
    x = STSWD_summary.tfr_alpha_LV1.linear_win(idxMulti_summary);
    y = STSWD_summary.HDDM_vat.driftEEG_linear(idxMulti_summary);
	scatter(x, y, 70, 'filled', 'MarkerFaceColor', [1 .6 .6]); l1 = lsline(); set(l1, 'Color',[1 .6 .6], 'LineWidth', 3);
	[r, p] = corrcoef(x, y);
% 	legend([l1], ['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))],'location', 'SouthEast'); legend('boxoff')   
    title(['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))])
    xlabel({'EEG alpha bs';'(linear modulation)'}); ylabel({'Drift rate';'(linear modulation)'})
    ylim([min(y)-(.1.*(max(y)-min(y))), max(y)+(.1.*(max(y)-min(y)))])
    set(h,'Color','w')
    for indAx = 1:1; ax{indAx}.Color = [.2 .2 .2]; end
    set(findall(gcf,'-property','FontSize'),'FontSize',25)
    
h = figure('units','normalized','position',[.1 .1 .175 .25]);
    ax{1} = subplot(1,1,1); cla; hold on;
    x = STSWD_summary.tfr_theta_LV1.linear_win(idxMulti_summary);
    y = STSWD_summary.HDDM_vat.driftEEG_linear(idxMulti_summary);
	scatter(x, y, 70, 'filled', 'MarkerFaceColor', [1 .6 .6]); l1 = lsline(); set(l1, 'Color',[1 .6 .6], 'LineWidth', 3);
	[r, p] = corrcoef(x, y);
% 	legend([l1], ['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))],'location', 'SouthEast'); legend('boxoff')   
    title(['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))])
    xlabel({'EEG theta bs';'(linear modulation)'}); ylabel({'Drift rate';'(linear modulation)'})
    ylim([min(y)-(.1.*(max(y)-min(y))), max(y)+(.1.*(max(y)-min(y)))])
    set(h,'Color','w')
    for indAx = 1:1; ax{indAx}.Color = [.2 .2 .2]; end
    set(findall(gcf,'-property','FontSize'),'FontSize',25)
   
h = figure('units','normalized','position',[.1 .1 .175 .25]);
    ax{1} = subplot(1,1,1); cla; hold on;
    x = STSWD_summary.tfr_gamma_LV1.linear_win(idxMulti_summary);
    y = STSWD_summary.HDDM_vat.driftEEG_linear(idxMulti_summary);
	scatter(x, y, 70, 'filled', 'MarkerFaceColor', [1 .6 .6]); l1 = lsline(); set(l1, 'Color',[1 .6 .6], 'LineWidth', 3);
	[r, p] = corrcoef(x, y);
% 	legend([l1], ['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))],'location', 'SouthEast'); legend('boxoff')   
    title(['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))])
    xlabel({'EEG gamma bs';'(linear modulation)'}); ylabel({'Drift rate';'(linear modulation)'})
    ylim([min(y)-(.1.*(max(y)-min(y))), max(y)+(.1.*(max(y)-min(y)))])
    set(h,'Color','w')
    for indAx = 1:1; ax{indAx}.Color = [.2 .2 .2]; end
    set(findall(gcf,'-property','FontSize'),'FontSize',25)

%% prestim alpha is uncorrelated with drift changes

% h = figure('units','normalized','position',[.1 .1 .15 .2]);
%     ax{1} = subplot(1,1,1); cla; hold on;
%     x = STSWD_summary.tfr_prestim_alpha_LV1.linear_win(idxMulti_summary);
%     y = STSWD_summary.tfr_gamma_LV1.linear_win(idxMulti_summary);
% 	scatter(x, y, 70, 'filled', 'MarkerFaceColor', [1 .6 .6]); l1 = lsline(); set(l1, 'Color',[1 .6 .6], 'LineWidth', 3);
% 	[r, p] = corrcoef(x, y);
% % 	legend([l1], ['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))],'location', 'SouthEast'); legend('boxoff')   
%     title(['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))])
%     xlabel({'EEG prestim alpha bs';'(linear modulation)'}); ylabel({'EEG stim gamma lv1 bs';'(linear modulation)'})
%     ylim([min(y)-(.1.*(max(y)-min(y))), max(y)+(.1.*(max(y)-min(y)))])
%     set(h,'Color','w')
%     for indAx = 1:1; ax{indAx}.Color = [.2 .2 .2]; end
%     set(findall(gcf,'-property','FontSize'),'FontSize',25)

h = figure('units','normalized','position',[.1 .1 .15 .2]);
    ax{1} = subplot(1,1,1); cla; hold on;
    x = STSWD_summary.tfr_prestim_alpha_LV1.linear_win(idxMulti_summary);
    y = STSWD_summary.HDDM_vat.driftEEG_linear(idxMulti_summary);
	scatter(x, y, 70, 'filled', 'MarkerFaceColor', [1 .6 .6]); l1 = lsline(); set(l1, 'Color',[1 .6 .6], 'LineWidth', 3);
	[r, p] = corrcoef(x, y);
% 	legend([l1], ['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))],'location', 'SouthEast'); legend('boxoff')   
    title(['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))])
    xlabel({'EEG prestim alpha bs';'(linear modulation)'}); ylabel({'Drift rate';'(linear modulation)'})
    ylim([min(y)-(.1.*(max(y)-min(y))), max(y)+(.1.*(max(y)-min(y)))])
    set(h,'Color','w')
    for indAx = 1:1; ax{indAx}.Color = [.2 .2 .2]; end
    set(findall(gcf,'-property','FontSize'),'FontSize',25)
    
%% correlations among excitability signatures

% EEG alpha - SE

h = figure('units','normalized','position',[.1 .1 .15 .2]);
    ax{1} = subplot(1,1,1); cla; hold on;
    x = STSWD_summary.tfr_alpha_LV1.linear_win(idxMulti_summary);
    y = STSWD_summary.SE_LV1.linear_win(idxMulti_summary);
	scatter(x, y, 70, 'filled', 'MarkerFaceColor', [1 .6 .6]); l1 = lsline(); set(l1, 'Color',[1 .6 .6], 'LineWidth', 3);
	[r, p] = corrcoef(x, y);
% 	legend([l1], ['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))],'location', 'SouthEast'); legend('boxoff')   
    title(['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))])
    xlabel({'EEG alpha lv1 bs';'(linear modulation)'}); ylabel({'sample entropy';'(linear modulation)'})
    ylim([min(y)-(.1.*(max(y)-min(y))), max(y)+(.1.*(max(y)-min(y)))])
    set(h,'Color','w')
    for indAx = 1:1; ax{indAx}.Color = [.2 .2 .2]; end
    set(findall(gcf,'-property','FontSize'),'FontSize',25)

% EEG alpha - SE

h = figure('units','normalized','position',[.1 .1 .15 .2]);
    ax{1} = subplot(1,1,1); cla; hold on;
    x = STSWD_summary.tfr_alpha_LV1.linear_win(idxMulti_summary);
    y = STSWD_summary.fooof_LV1.linear_win(idxMulti_summary);
	scatter(x, y, 70, 'filled', 'MarkerFaceColor', [1 .6 .6]); l1 = lsline(); set(l1, 'Color',[1 .6 .6], 'LineWidth', 3);
	[r, p] = corrcoef(x, y);
% 	legend([l1], ['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))],'location', 'SouthEast'); legend('boxoff')   
    title(['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))])
    xlabel({'EEG alpha lv1 bs';'(linear modulation)'}); ylabel({'1/f bs';'(linear modulation)'})
    ylim([min(y)-(.1.*(max(y)-min(y))), max(y)+(.1.*(max(y)-min(y)))])
    set(h,'Color','w')
    for indAx = 1:1; ax{indAx}.Color = [.2 .2 .2]; end
    set(findall(gcf,'-property','FontSize'),'FontSize',25)

% 1/f slopes - SE

h = figure('units','normalized','position',[.1 .1 .15 .2]);
    ax{1} = subplot(1,1,1); cla; hold on;
    x = STSWD_summary.fooof_LV1.linear_win(idxMulti_summary);
    y = STSWD_summary.SE_LV1.linear_win(idxMulti_summary);
	scatter(x, y, 70, 'filled', 'MarkerFaceColor', [1 .6 .6]); l1 = lsline(); set(l1, 'Color',[1 .6 .6], 'LineWidth', 3);
	[r, p] = corrcoef(x, y);
% 	legend([l1], ['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))],'location', 'SouthEast'); legend('boxoff')   
    title(['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))])
    xlabel({'1/f slopes';'(linear modulation)'}); ylabel({'sample entropy';'(linear modulation)'})
    ylim([min(y)-(.1.*(max(y)-min(y))), max(y)+(.1.*(max(y)-min(y)))])
    set(h,'Color','w')
    for indAx = 1:1; ax{indAx}.Color = [.2 .2 .2]; end
    set(findall(gcf,'-property','FontSize'),'FontSize',25)

% excitability vs. excitability brainscore
    h = figure('units','normalized','position',[.1 .1 .15 .2]);
    ax{1} = subplot(1,1,1); cla; hold on;
    x = nanmean(zscore(cat(2, STSWD_summary.SE_LV1.linear_win(idxMulti_summary), ...
        STSWD_summary.fooof_LV1.linear_win(idxMulti_summary),...
        STSWD_summary.tfr_alpha_LV1.linear_win(idxMulti_summary)),[],1),2);
    y = STSWD_summary.excitability_LV1.linear_win(idxMulti_summary);
	scatter(x, y, 70, 'filled', 'MarkerFaceColor', [1 .6 .6]); l1 = lsline(); set(l1, 'Color',[1 .6 .6], 'LineWidth', 3);
	[r, p] = corrcoef(x, y);
% 	legend([l1], ['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))],'location', 'SouthEast'); legend('boxoff')   
    title(['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))])
    xlabel({'excitability mod. lv1';'(linear modulation)'}); ylabel({'excitability bs';'(linear modulation)'})
    ylim([min(y)-(.1.*(max(y)-min(y))), max(y)+(.1.*(max(y)-min(y)))])
    set(h,'Color','w')
    for indAx = 1:1; ax{indAx}.Color = [.2 .2 .2]; end
    set(findall(gcf,'-property','FontSize'),'FontSize',25)

%% pupil is unrelated to excitability sigs, but coupled to theta
    
h = figure('units','normalized','position',[.1 .1 .15 .2]);
    ax{1} = subplot(1,1,1); cla; hold on;
    x = STSWD_summary.excitability_LV1.linear_win(idxMulti_summary);
    y = STSWD_summary.pupil_LV1.linear_win(idxMulti_summary);
	scatter(x, y, 70, 'filled', 'MarkerFaceColor', [1 .6 .6]); l1 = lsline(); set(l1, 'Color',[1 .6 .6], 'LineWidth', 3);
	[r, p] = corrcoef(x, y);
% 	legend([l1], ['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))],'location', 'SouthEast'); legend('boxoff')   
    title(['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))])
    xlabel({'excitability mod. lv1';'(linear modulation)'}); ylabel({'pupil';'(linear modulation)'})
    ylim([min(y)-(.1.*(max(y)-min(y))), max(y)+(.1.*(max(y)-min(y)))])
    set(h,'Color','w')
    for indAx = 1:1; ax{indAx}.Color = [.2 .2 .2]; end
    set(findall(gcf,'-property','FontSize'),'FontSize',25)

h = figure('units','normalized','position',[.1 .1 .15 .2]);
    ax{1} = subplot(1,1,1); cla; hold on;
    x = STSWD_summary.tfr_theta_LV1.linear_win(idxMulti_summary);
    y = STSWD_summary.pupil_LV1.linear_win(idxMulti_summary);
	scatter(x, y, 70, 'filled', 'MarkerFaceColor', [1 .6 .6]); l1 = lsline(); set(l1, 'Color',[1 .6 .6], 'LineWidth', 3);
	[r, p] = corrcoef(x, y);
% 	legend([l1], ['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))],'location', 'SouthEast'); legend('boxoff')   
    title(['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))])
    xlabel({'Theta lv1';'(linear modulation)'}); ylabel({'pupil';'(linear modulation)'})
    ylim([min(y)-(.1.*(max(y)-min(y))), max(y)+(.1.*(max(y)-min(y)))])
    set(h,'Color','w')
    for indAx = 1:1; ax{indAx}.Color = [.2 .2 .2]; end
    set(findall(gcf,'-property','FontSize'),'FontSize',25)
    
h = figure('units','normalized','position',[.1 .1 .15 .2]);
    ax{1} = subplot(1,1,1); cla; hold on;
    x = STSWD_summary.HDDM_vat.driftEEG_linear(idxMulti_summary);
    y = STSWD_summary.pupil_LV1.linear_win(idxMulti_summary);
	scatter(x, y, 70, 'filled', 'MarkerFaceColor', [1 .6 .6]); l1 = lsline(); set(l1, 'Color',[1 .6 .6], 'LineWidth', 3);
	[r, p] = corrcoef(x, y);
% 	legend([l1], ['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))],'location', 'SouthEast'); legend('boxoff')   
    title(['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))])
    xlabel({'Drift rate';'(linear modulation)'}); ylabel({'pupil';'(linear modulation)'})
    ylim([min(y)-(.1.*(max(y)-min(y))), max(y)+(.1.*(max(y)-min(y)))])
    set(h,'Color','w')
    for indAx = 1:1; ax{indAx}.Color = [.2 .2 .2]; end
    set(findall(gcf,'-property','FontSize'),'FontSize',25)

    %% decoding
    
    h = figure('units','normalized','position',[.1 .1 .175 .25]);
    ax{1} = subplot(1,1,1); cla; hold on;
    x = STSWD_summary.HDDM_vat.driftEEGMRI_linear_win(idxMultiComplete_summary);
    y = STSWD_summary.dec_unprobed.linear_win(idxMultiComplete_summary);
	scatter(x, y, 70, 'filled', 'MarkerFaceColor', [1 .6 .6]); l1 = lsline(); set(l1, 'Color',[1 .6 .6], 'LineWidth', 3);
	[r, p] = corrcoef(x, y);
% 	legend([l1], ['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))],'location', 'SouthEast'); legend('boxoff')   
    title(['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))])
    ylim([min(y)-(.1.*(max(y)-min(y))), max(y)+(.1.*(max(y)-min(y)))])
    set(h,'Color','w')
    for indAx = 1:1; ax{indAx}.Color = [.2 .2 .2]; end
    set(findall(gcf,'-property','FontSize'),'FontSize',25)
    
    h = figure('units','normalized','position',[.1 .1 .175 .25]);
    ax{1} = subplot(1,1,1); cla; hold on;
    x = STSWD_summary.HDDM_vat.driftEEGMRI_linear_win(idxMultiComplete_summary);
    y = STSWD_summary.dec_probed.linear_win(idxMultiComplete_summary);
	scatter(x, y, 70, 'filled', 'MarkerFaceColor', [1 .6 .6]); l1 = lsline(); set(l1, 'Color',[1 .6 .6], 'LineWidth', 3);
	[r, p] = corrcoef(x, y);
% 	legend([l1], ['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))],'location', 'SouthEast'); legend('boxoff')   
    title(['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))])
    ylim([min(y)-(.1.*(max(y)-min(y))), max(y)+(.1.*(max(y)-min(y)))])
    set(h,'Color','w')
    for indAx = 1:1; ax{indAx}.Color = [.2 .2 .2]; end
    set(findall(gcf,'-property','FontSize'),'FontSize',25)
    
    h = figure('units','normalized','position',[.1 .1 .175 .25]);
    ax{1} = subplot(1,1,1); cla; hold on;
    x = STSWD_summary.HDDM_vat.driftEEGMRI_linear_win(idxMultiComplete_summary);
    y = STSWD_summary.dec_cued.linear_win(idxMultiComplete_summary);
	scatter(x, y, 70, 'filled', 'MarkerFaceColor', [1 .6 .6]); l1 = lsline(); set(l1, 'Color',[1 .6 .6], 'LineWidth', 3);
	[r, p] = corrcoef(x, y);
% 	legend([l1], ['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))],'location', 'SouthEast'); legend('boxoff')   
    title(['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))])
    ylim([min(y)-(.1.*(max(y)-min(y))), max(y)+(.1.*(max(y)-min(y)))])
    set(h,'Color','w')
    for indAx = 1:1; ax{indAx}.Color = [.2 .2 .2]; end
    set(findall(gcf,'-property','FontSize'),'FontSize',25)
