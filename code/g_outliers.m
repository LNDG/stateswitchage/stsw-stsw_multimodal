% assess inter-individual relations between first level linear effects 

% path management
currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..'))

rootpath = pwd;
pn.data_summary = fullfile(rootpath, 'data');
pn.figures = fullfile(rootpath, 'figures', 'linearbyage'); mkdir(pn.figures);

filename = fullfile(rootpath, 'code', 'id_list.txt');
fileID = fopen(filename);
IDs = textscan(fileID,'%s');
fclose(fileID);
IDs = IDs{1};

filename = fullfile(rootpath, 'code', 'id_list_eegmr.txt');
fileID = fopen(filename);
IDs_EEGMR = textscan(fileID,'%s');
fclose(fileID);
IDs_EEGMR = IDs_EEGMR{1};

%% load summary structure

load(fullfile(pn.data_summary, 'STSWD_summary_YAOA.mat'), 'STSWD_summary')

% select subjects (EEG only)
idxEEG_summary = ismember(STSWD_summary.IDs, IDs);
[STSWD_summary.IDs(idxEEG_summary), IDs]

% select subjects (multimodal only)
idxMulti_summary = ismember(STSWD_summary.IDs, IDs_EEGMR);
[STSWD_summary.IDs(idxMulti_summary), IDs_EEGMR]

%% define YA and OA

idx_YA = cellfun(@str2num, STSWD_summary.IDs)<2000;
idx_OA = cellfun(@str2num, STSWD_summary.IDs)>2000;

%% get overview of outliers (defined within each group)

fields_name = {'driftEEGMRI_linear', ...
   'thresholdEEGMRI_linear',...
   'nondecisionEEGMRI_linear'};

outliers_hddm_mod = [];
for indParam = 1:numel(fields_name)
    outliers_hddm_mod(1:numel(STSWD_summary.IDs),indParam) = 0;
    
    tmp_ya = STSWD_summary.HDDM_vat.(fields_name{indParam})(idx_YA);
    i_outlier_ya = find(isoutlier(tmp_ya, 'median'));
    tmp_ids = find(idx_YA);
    outliers_hddm_mod(tmp_ids(i_outlier_ya),indParam) = 1;
    
    tmp_oa = STSWD_summary.HDDM_vat.(fields_name{indParam})(idx_OA);
    i_outlier_oa = find(isoutlier(tmp_oa, 'median'));
    tmp_ids = find(idx_OA);
    outliers_hddm_mod(tmp_ids(i_outlier_oa),indParam) = 1;
end

fields_name = {'driftEEGMRI', ...
   'thresholdEEGMRI',...
   'nondecisionEEGMRI'};

outliers_hddm = [];
for indParam = 1:numel(fields_name)
    outliers_hddm(1:numel(STSWD_summary.IDs),indParam) = 0;
    
    tmp_ya = squeeze(nanmean(STSWD_summary.HDDM_vat.(fields_name{indParam})(idx_YA,:),2));
    i_outlier_ya = find(isoutlier(tmp_ya, 'median'));
    tmp_ids = find(idx_YA);
    outliers_hddm(tmp_ids(i_outlier_ya),indParam) = 1;
    
    tmp_oa = squeeze(nanmean(STSWD_summary.HDDM_vat.(fields_name{indParam})(idx_OA),2));
    i_outlier_oa = find(isoutlier(tmp_oa, 'median'));
    tmp_ids = find(idx_OA);
    outliers_hddm(tmp_ids(i_outlier_oa),indParam) = 1;
end

fields_name = {'excitability_LV1', ...
   'tfr_theta_LV1',...
   'tfr_gamma_LV1', ...
   'tfr_prestim_alpha_LV1', ...
   'pupil_LV1', ...
   'CPPslopes', ...
   'BetaSlope', ...
   'CPPthreshold', ...
   'BetaThreshold', ...
   'SSVEPmag_norm', ...
   'SPM_lv1', ...
   'tfr_alpha_LV1', ...
   'SE_LV1', ...
   'fooof_LV1'};

outliers = [];
for indParam = 1:numel(fields_name)
    outliers(1:numel(STSWD_summary.IDs),indParam) = 0;
    
    tmp_ya = STSWD_summary.(fields_name{indParam}).linear(idx_YA);
    i_outlier_ya = find(isoutlier(tmp_ya, 'median'));
    tmp_ids = find(idx_YA);
    outliers(tmp_ids(i_outlier_ya),indParam) = 1;
    
    tmp_oa = STSWD_summary.(fields_name{indParam}).linear(idx_OA);
    i_outlier_oa = find(isoutlier(tmp_oa, 'median'));
    tmp_ids = find(idx_OA);
    outliers(tmp_ids(i_outlier_oa),indParam) = 1;
end

outliers_all = cat(2, outliers_hddm, outliers_hddm_mod, outliers);


figure; imagesc(outliers_all)

figure; plot(squeeze(nanmean(outliers_all,2)))

STSWD_summary.IDs(squeeze(nanmean(outliers_all,2))>=.1)

% behavioral outliers?

avgBehavEEG = squeeze(nanmean(STSWD_summary.behav.EEGAcc,2));
avgBehavMRI = squeeze(nanmean(STSWD_summary.behav.MRIAcc,2));

STSWD_summary.IDs(avgBehavEEG<=.5)
STSWD_summary.IDs(avgBehavMRI<=.5)

figure; histogram(squeeze(nanmean(STSWD_summary.behav.EEGAcc,2)))