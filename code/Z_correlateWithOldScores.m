% path management
currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..'))

rootpath = pwd;
pn.data_summary = fullfile(rootpath, 'data');

filename = fullfile(rootpath, 'code', 'id_list.txt');
fileID = fopen(filename);
IDs = textscan(fileID,'%s');
fclose(fileID);
IDs = IDs{1};

filename = fullfile(rootpath, 'code', 'id_list_eegmr.txt');
fileID = fopen(filename);
IDs_EEGMR = textscan(fileID,'%s');
fclose(fileID);
IDs_EEGMR = IDs_EEGMR{1};

%% load summary structure

load(fullfile(pn.data_summary, 'STSWD_summary_YAOA.mat'), 'STSWD_summary')

NatComms = load(fullfile(pn.data_summary, 'STSWD_summary_YA_NatComms.mat'), 'STSWD_summary');

% select subjects (EEG only)
idxEEG_summary = ismember(STSWD_summary.IDs, IDs);
[STSWD_summary.IDs(idxEEG_summary), IDs]

% select subjects (multimodal only)
idxMulti_summary = ismember(STSWD_summary.IDs, IDs_EEGMR);
[STSWD_summary.IDs(idxMulti_summary), IDs_EEGMR]

idxYA = str2double(STSWD_summary.IDs)<2000;
idxOA = str2double(STSWD_summary.IDs)>=2000;

%% perform 2nd level correlations

% excitability score vs NatComms EEG LV1

h = figure('units','normalized','position',[.1 .1 .175 .25]);
    ax{1} = subplot(1,1,1); cla; hold on;
    x = STSWD_summary.excitability_LV1.linear_win(idxMulti_summary&idxYA);
    y = NatComms.STSWD_summary.EEG_LV1.slope_win(idxMulti_summary&idxYA);
	scatter(x, y, 70, 'filled', 'MarkerFaceColor', [1 .6 .6]); l1 = lsline(); set(l1, 'Color',[1 .6 .6], 'LineWidth', 3);
	[r, p] = corrcoef(x, y);
    title(['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))])
    xlabel({'excitability bs';'(linear modulation)'}); ylabel({'NatComms: EEG LV1';'(linear modulation)'})
    ylim([min(y)-(.1.*(max(y)-min(y))), max(y)+(.1.*(max(y)-min(y)))])
    set(h,'Color','w')
    for indAx = 1:1; ax{indAx}.Color = [.2 .2 .2]; end
    set(findall(gcf,'-property','FontSize'),'FontSize',25)
    
h = figure('units','normalized','position',[.1 .1 .175 .25]);
    ax{1} = subplot(1,1,1); cla; hold on;
    x = STSWD_summary.tfr_alpha_LV1.linear_win(idxMulti_summary&idxYA);
    y = NatComms.STSWD_summary.EEG_LV1.slope_win(idxMulti_summary&idxYA);
	scatter(x, y, 70, 'filled', 'MarkerFaceColor', [1 .6 .6]); l1 = lsline(); set(l1, 'Color',[1 .6 .6], 'LineWidth', 3);
	[r, p] = corrcoef(x, y);
    title(['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))])
    xlabel({'alpha bs';'(linear modulation)'}); ylabel({'NatComms: EEG LV1';'(linear modulation)'})
    ylim([min(y)-(.1.*(max(y)-min(y))), max(y)+(.1.*(max(y)-min(y)))])
    set(h,'Color','w')
    for indAx = 1:1; ax{indAx}.Color = [.2 .2 .2]; end
    set(findall(gcf,'-property','FontSize'),'FontSize',25)
    
h = figure('units','normalized','position',[.1 .1 .175 .25]);
    ax{1} = subplot(1,1,1); cla; hold on;
    x = STSWD_summary.fooof_LV1.linear_win(idxMulti_summary&idxYA);
    y = NatComms.STSWD_summary.OneFslope.linear_win(idxMulti_summary&idxYA);
	scatter(x, y, 70, 'filled', 'MarkerFaceColor', [1 .6 .6]); l1 = lsline(); set(l1, 'Color',[1 .6 .6], 'LineWidth', 3);
	[r, p] = corrcoef(x, y);
    title(['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))])
    xlabel({'fooof bs';'(linear modulation)'}); ylabel({'NatComms: 1/f slope';'(linear modulation)'})
    ylim([min(y)-(.1.*(max(y)-min(y))), max(y)+(.1.*(max(y)-min(y)))])
    set(h,'Color','w')
    for indAx = 1:1; ax{indAx}.Color = [.2 .2 .2]; end
    set(findall(gcf,'-property','FontSize'),'FontSize',25)
    
h = figure('units','normalized','position',[.1 .1 .175 .25]);
    ax{1} = subplot(1,1,1); cla; hold on;
    x = STSWD_summary.SE_LV1.linear_win(idxMulti_summary&idxYA);
    y = NatComms.STSWD_summary.SE.data_slope_win(idxMulti_summary&idxYA);
	scatter(x, y, 70, 'filled', 'MarkerFaceColor', [1 .6 .6]); l1 = lsline(); set(l1, 'Color',[1 .6 .6], 'LineWidth', 3);
	[r, p] = corrcoef(x, y);
    title(['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))])
    xlabel({'sampen bs';'(linear modulation)'}); ylabel({'NatComms: sampen';'(linear modulation)'})
    ylim([min(y)-(.1.*(max(y)-min(y))), max(y)+(.1.*(max(y)-min(y)))])
    set(h,'Color','w')
    for indAx = 1:1; ax{indAx}.Color = [.2 .2 .2]; end
    set(findall(gcf,'-property','FontSize'),'FontSize',25)

%%

load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/B4_PLS_preproc2/B_data/SPM_STSWD_v3/behavPLS_STSWD_Mean_fullModel_N41_3mm_1000P1000B_BfMRIresult_bs.mat')

idx_natcommspls = ismember(STSWD_summary.IDs, BS.IDs);

h = figure('units','normalized','position',[.1 .1 .15 .25]);hold on;
ax{1} = subplot(1,1,1);
% add AMF
x = BS.data;
y = NatComms.STSWD_summary.SE.data_slope_win(idx_natcommspls);
scatter(x, y, 70, 'filled', 'MarkerFaceColor', [1 .6 .6]); l1 = lsline(); set(l1, 'Color',[1 .6 .6], 'LineWidth', 3);
[r, p, RL, RU] = corrcoef(x, y);
title(['r = ', num2str(round(r(2),2)), ' CI = [',num2str(round(RL(2),2)),',',num2str(round(RU(2),2)),']'])
ylabel({'sample entropy';'(linear mod.)'})
ylim([min(y)-(.1.*(max(y)-min(y))), max(y)+(.1.*(max(y)-min(y)))])
xlim([min(x)-(.1.*(max(x)-min(x))), max(x)+(.1.*(max(x)-min(x)))])
ax{1}.Color = [.2 .2 .2];
set(findall(gcf,'-property','FontSize'),'FontSize',25)

h = figure('units','normalized','position',[.1 .1 .15 .25]);hold on;
ax{1} = subplot(1,1,1);
% add AMF
x = BS.data;
y = STSWD_summary.SE_LV1.linear_win(idx_natcommspls);
scatter(x, y, 70, 'filled', 'MarkerFaceColor', [1 .6 .6]); l1 = lsline(); set(l1, 'Color',[1 .6 .6], 'LineWidth', 3);
[r, p, RL, RU] = corrcoef(x, y);
title(['r = ', num2str(round(r(2),2)), ' CI = [',num2str(round(RL(2),2)),',',num2str(round(RU(2),2)),']'])
ylabel({'sample entropy bs';'(linear mod.)'})
ylim([min(y)-(.1.*(max(y)-min(y))), max(y)+(.1.*(max(y)-min(y)))])
xlim([min(x)-(.1.*(max(x)-min(x))), max(x)+(.1.*(max(x)-min(x)))])
ax{1}.Color = [.2 .2 .2];
set(findall(gcf,'-property','FontSize'),'FontSize',25)

h = figure('units','normalized','position',[.1 .1 .15 .25]);hold on;
ax{1} = subplot(1,1,1);
% add AMF
x = BS.data;
y = STSWD_summary.SE.data_slope_win(idx_natcommspls);
scatter(x, y, 70, 'filled', 'MarkerFaceColor', [1 .6 .6]); l1 = lsline(); set(l1, 'Color',[1 .6 .6], 'LineWidth', 3);
[r, p, RL, RU] = corrcoef(x, y);
title(['r = ', num2str(round(r(2),2)), ' CI = [',num2str(round(RL(2),2)),',',num2str(round(RU(2),2)),']'])
ylabel({'sample entropy new';'(linear mod.)'})
ylim([min(y)-(.1.*(max(y)-min(y))), max(y)+(.1.*(max(y)-min(y)))])
xlim([min(x)-(.1.*(max(x)-min(x))), max(x)+(.1.*(max(x)-min(x)))])
ax{1}.Color = [.2 .2 .2];
set(findall(gcf,'-property','FontSize'),'FontSize',25)

h = figure('units','normalized','position',[.1 .1 .15 .25]);hold on;
ax{1} = subplot(1,1,1);
% add AMF
x = BS.data;
y = STSWD_summary.excitability_LV1.linear_win(idx_natcommspls);
scatter(x, y, 70, 'filled', 'MarkerFaceColor', [1 .6 .6]); l1 = lsline(); set(l1, 'Color',[1 .6 .6], 'LineWidth', 3);
[r, p, RL, RU] = corrcoef(x, y);
title(['r = ', num2str(round(r(2),2)), ' CI = [',num2str(round(RL(2),2)),',',num2str(round(RU(2),2)),']'])
ylabel({'sample entropy new';'(linear mod.)'})
ylim([min(y)-(.1.*(max(y)-min(y))), max(y)+(.1.*(max(y)-min(y)))])
xlim([min(x)-(.1.*(max(x)-min(x))), max(x)+(.1.*(max(x)-min(x)))])
ax{1}.Color = [.2 .2 .2];
set(findall(gcf,'-property','FontSize'),'FontSize',25)