% create a read-in structure for R to perform statistical analyses

    % path management
    currentFile = mfilename('fullpath');
    [pathstr,~,~] = fileparts(currentFile);
    cd(fullfile(pathstr,'..'))

    pn.root = pwd;
    pn.data_summary = fullfile(pn.root, 'data');

%% select subjects

    filename = fullfile(rootpath, 'code', 'id_list.txt');
    fileID = fopen(filename);
    IDs = textscan(fileID,'%s');
    fclose(fileID);
    IDs = IDs{1};

%% load summary structure

    load(fullfile(pn.data_summary, 'STSWD_summary_YAOA.mat'), 'STSWD_summary')

%% reorganize for R input

    % select full EEG sample
    idx_Summary = find(ismember(STSWD_summary.IDs, IDs));
    [STSWD_summary.IDs(idx_Summary),IDs]

    dataS.Subject = repmat(IDs,1,4); dataS.Subject = reshape(dataS.Subject', [],1);
    dataS.Condition = repmat([1:4],1,numel(IDs)); dataS.Condition = reshape(dataS.Condition', [],1);
    dataS.drift_eeg = reshape(STSWD_summary.HDDM_vt.driftEEG(idx_Summary,1:4)', [],1);
    dataS.ndt_eeg = reshape(STSWD_summary.HDDM_vt.nondecisionEEG(idx_Summary,1:4)', [],1);
    dataS.drift_mri = reshape(STSWD_summary.HDDM_vt.driftMRI(idx_Summary,1:4)', [],1);
    dataS.ndt_mri = reshape(STSWD_summary.HDDM_vt.nondecisionMRI(idx_Summary,1:4)', [],1);
    dataS.pupil = reshape(STSWD_summary.pupil2.stimdiff(idx_Summary,1:4)', [],1);
    dataS.eeg_lv1 = reshape(STSWD_summary.EEG_LV1.data(idx_Summary,1:4)', [],1);
    dataS.eeg_prestim_lv1 = reshape(STSWD_summary.EEG_prestim.data(idx_Summary,1:4)', [],1);
    dataS.mri_lv1 = reshape(STSWD_summary.SPM_task.LV1.data(idx_Summary,1:4)', [],1);
    dataS.mri_lv2 = reshape(STSWD_summary.SPM_task.LV2.data(idx_Summary,1:4)', [],1);
    dataS.aperiodic = reshape(STSWD_summary.OneFslope.data(idx_Summary,1:4)', [],1);
    dataS.entropy = reshape(STSWD_summary.SE.data(idx_Summary,1:4)', [],1);
    dataS.thetapow = reshape(STSWD_summary.eBOSC.rhythmicThetaPower.data(idx_Summary,1:4)', [],1);
    dataS.thetadur = reshape(STSWD_summary.eBOSC.rhythmicThetaDuration.data(idx_Summary,1:4)', [],1);
    dataS.alphapow = reshape(STSWD_summary.eBOSC.rhythmicAlphaPower.data(idx_Summary,1:4)', [],1);
    dataS.alphadur = reshape(STSWD_summary.eBOSC.rhythmicAlphaDuration.data(idx_Summary,1:4)', [],1);
    dataS.eeg_probe = reshape(STSWD_summary.EEG_probe.data(idx_Summary,1:4)', [],1);
    dataS.CPPslope = reshape(STSWD_summary.CPPSlope.data(idx_Summary,1:4)', [],1);
    dataS.CPPthreshold = reshape(STSWD_summary.CPPthreshold.data(idx_Summary,1:4)', [],1);
    dataS.betaslope = reshape(STSWD_summary.BetaSlope.data(idx_Summary,1:4)', [],1);
    dataS.betathreshold = reshape(STSWD_summary.BetaThreshold.data(idx_Summary,1:4)', [],1);
    dataS.NDTpotential = reshape(STSWD_summary.NDTpotential.data(idx_Summary,1:4)', [],1);
    dataS.ThetaResp = reshape(STSWD_summary.ThetaResp.data(idx_Summary,1:4)', [],1);    
    dataS.thresh_eeg = reshape(STSWD_summary.HDDM.thresholdEEG(idx_Summary,1:4)', [],1);
    dataS.thresh_mri = reshape(STSWD_summary.HDDM.thresholdMRI(idx_Summary,1:4)', [],1);
    
    dataS.acc_eeg = reshape(STSWD_summary.behav.EEGAcc(idx_Summary,1:4)', [],1);
    dataS.rt_eeg = reshape(STSWD_summary.behav.EEGRT(idx_Summary,1:4)', [],1);    
    dataS.acc_mri = reshape(STSWD_summary.behav.MRIAcc(idx_Summary,1:4)', [],1);
    dataS.rt_mri = reshape(STSWD_summary.behav.MRIRT(idx_Summary,1:4)', [],1);

    % combine in data matrix
    data = [];
    data = [dataS.Condition, dataS.drift_eeg, dataS.ndt_eeg, dataS.drift_mri, dataS.ndt_mri, ...
       dataS.pupil,dataS.eeg_lv1,dataS.eeg_prestim_lv1,dataS.mri_lv1,dataS.mri_lv2,dataS.aperiodic,dataS.entropy, ...
       dataS.thetapow,dataS.thetadur, dataS.alphapow, dataS.alphadur, dataS.eeg_probe,dataS.CPPslope, dataS.CPPthreshold, ...
       dataS.betaslope, dataS.betathreshold, dataS.thresh_eeg, dataS.thresh_mri, dataS.NDTpotential, dataS.ThetaResp, ...
       dataS.acc_eeg, dataS.rt_eeg, dataS.acc_mri, dataS.rt_mri];
    data = num2cell(data);
    data = [dataS.Subject, data];

    % add headers
    data = [{'Subject'},{'Condition'},{'drift_eeg'},{'ndt_eeg'},{'drift_mri'},{'ndt_mri'}, ...
       {'pupil'},{'eeg_lv1'},{'eeg_prestim_lv1'},{'mri_lv1'},{'mri_lv2'},{'aperiodic'},{'entropy'}, ...
       {'thetapow'},{'thetadur'},{'alphapow'},{'alphadur'},...
       {'eeg_probe'},{'CPPslope'},{'CPPthreshold'},{'betaslope'},{'betathreshold'},{'thresh_eeg'},...
       {'thresh_mri'},{'NDTpotential'}, {'ThetaResp'}, {'acc_eeg'},{'rt_eeg'}, {'acc_mri'}, {'rt_mri'};  data];

    %% save for R

    addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/behavior/STSW_dynamic/B_reliabilityAcrossSessions/D_tools/cell2csv/')
    cell2csv([pn.summaryData, 'STSWDsummary_YAOA_forR.dat'],data)

    save([pn.summaryData, 'STSWDsummary_YAOA_forR.mat'],'data')
