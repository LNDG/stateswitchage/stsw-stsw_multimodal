% assess inter-individual relations between first level linear effects 

% path management
currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..'))

rootpath = pwd;
pn.data_summary = fullfile(rootpath, 'data');
pn.figures = fullfile(rootpath, 'figures', 'l1byage'); mkdir(pn.figures);

filename = fullfile(rootpath, 'code', 'id_list.txt');
fileID = fopen(filename);
IDs = textscan(fileID,'%s');
fclose(fileID);
IDs = IDs{1};

filename = fullfile(rootpath, 'code', 'id_list_eegmr.txt');
fileID = fopen(filename);
IDs_EEGMR = textscan(fileID,'%s');
fclose(fileID);
IDs_EEGMR = IDs_EEGMR{1};

%% load summary structure

load(fullfile(pn.data_summary, 'STSWD_summary_YAOA.mat'), 'STSWD_summary')

% select subjects (EEG only)
idxEEG_summary = ismember(STSWD_summary.IDs, IDs);
[STSWD_summary.IDs(idxEEG_summary), IDs]

% select subjects (multimodal only)
idxMulti_summary = ismember(STSWD_summary.IDs, IDs_EEGMR);
[STSWD_summary.IDs(idxMulti_summary), IDs_EEGMR]

%% define YA and OA

idx_YA = cellfun(@str2num, STSWD_summary.IDs)<2000;
idx_OA = cellfun(@str2num, STSWD_summary.IDs)>2000;

%% group comparison of linear slopes

fields_title = {'drift l1', ...
   'cpp l1'; 'drift linear', ...
   'cpp linear'};

data2Plot = cell(2,4);
for indParam = 1:4
    if indParam == 1
        tmp = STSWD_summary.HDDM_vat.driftEEGMRI(:,1);
    elseif indParam == 3
        tmp = STSWD_summary.CPPslopes.data(:,1);
    elseif indParam == 2
        tmp = STSWD_summary.HDDM_vat.driftEEGMRI_linear_win(:);
    elseif indParam == 4
        tmp = STSWD_summary.CPPslopes.linear_win(:);
    end
    tmp(tmp==0)=NaN;
    data2Plot{1,indParam} = tmp(idx_YA&idxMulti_summary);
    data2Plot{2,indParam} = tmp(idx_OA&idxMulti_summary);
end

% plot linear estimates using bar charts with individual values

colorm = [.6 .6 .6; 1 .6 .6; .6 .8 1];

h = figure('units','centimeters','position',[.1 .1 10 15]);

for indParam = 1:4
    subplot(2,2,indParam)
    plot_data{1} = data2Plot{1,indParam};
    plot_data{2} = data2Plot{2,indParam};
    set(gcf,'renderer','Painters')
    cla;
    hold on;
    for indGroup = 1:2
        if ttest(plot_data{indGroup})==1 & nanmean(plot_data{indGroup})>0
            bar(indGroup, nanmean(plot_data{indGroup}), 'FaceColor', colorm(2,:), 'EdgeColor', 'none', 'BarWidth', 0.8);
        elseif ttest(plot_data{indGroup})==1 & nanmean(plot_data{indGroup})<0
            bar(indGroup, nanmean(plot_data{indGroup}), 'FaceColor', colorm(3,:), 'EdgeColor', 'none', 'BarWidth', 0.8);
        elseif ttest(plot_data{indGroup})==0
            bar(indGroup, nanmean(plot_data{indGroup}), 'FaceColor', colorm(1,:), 'EdgeColor', 'none', 'BarWidth', 0.8);
        end
        % plot individual values on top
        scatter(repmat(indGroup,numel(plot_data{indGroup}),1)+(rand(numel(plot_data{indGroup}),1)-.5).*.4,...
            plot_data{indGroup}, 20, 'filled', 'MarkerFaceColor', [.3 .3 .3]);
    end
    xlim([.25 2.75]); %ylim([1.3 2])
    set(gca,'xtick',[1,2]); set(gca,'xTickLabel',{'YA'; 'OA'});
    %xlabel('Age Group');
    [htest, p] = ttest2(plot_data{1}, plot_data{2});
    if p>10^-3
        title(['p = ', num2str(round(p,2))]);
    else
        tit = sprintf('p = %.1e',p);
        title(tit);
    end
end
set(findall(gcf,'-property','FontSize'),'FontSize',20)

figureName = ['drift_cpp'];
saveas(h, fullfile(pn.figures, figureName), 'epsc');
saveas(h, fullfile(pn.figures, figureName), 'png');


%% get statistics estimates

for indParam = 1:4
    plot_data{1} = data2Plot{1,indParam};
    plot_data{2} = data2Plot{2,indParam};
    for indGroup = 1:2
        [h_test,p,ci,stats] = ttest(plot_data{indGroup});
        disp(['Param ', num2str(indParam), ...
            ', Group ', num2str(indGroup), ...
            ', df ', num2str(stats.df), ...
            ', t-value: ', num2str(round(stats.tstat,2)), ...
            ', p-value: ', sprintf('%.1e',p), ...
            ', cohen-d: ', num2str(round(mean(plot_data{indGroup})/std(plot_data{indGroup}),2))]);
    end
end
