% This file can be used to correct pvalues for multiple comparisons

currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..'))
pn.root = pwd;

addpath(fullfile(pn.root, 'tools', 'pval_adjust'));

%% initialize data

data = [];
% manually copy data

%% data mean

squeeze(nanmean(data,1))

%% benjamini-hochberg-adjusted p-values

pval = []; ci = []; stats = [];
for indComparison = 1:3
    [~, pval(indComparison), ci(indComparison,:), stats{indComparison}] = ttest(data(:,indComparison), data(:,indComparison+1));
end

pval_adjust(pval, 'fdr')

%% linear effect

X = [1 1; 1 2; 1 3; 1 4]; 
b=X\data(:,:)'; 
IndividualSlopes = b(2,:);
IndividualIntercept = b(1,:);
pval = []; ci = []; stats = [];
[~, pval, ci, stats] = ttest(IndividualSlopes);

disp(['b=',num2str(round(mean(IndividualSlopes),2)),...
    ' [',num2str(round(ci(1),2)),',',num2str(round(ci(2),2)),']; t(',num2str(stats.df),')=', ...
    num2str(round(stats.tstat,2)), ', p=',num2str(pval)])

disp(['b=',num2str(mean(IndividualSlopes)),...
    '[',num2str(ci(1)),',',num2str(ci(2)),']; t(',num2str(stats.df),')=', ...
    num2str(round(stats.tstat,2)), ', p=',num2str(pval)])

figure; scatter(IndividualIntercept, squeeze(mean(data(:,1),2)))
figure; scatter(IndividualSlopes, squeeze(mean(diff(data(:,1:4),[],2),2)))

%% for more than 4 conds

X = [1 1; 1 2; 1 3; 1 4; 1 5; 1 6]; 
b=X\data(:,:)'; 
IndividualSlopes = b(2,:);
IndividualIntercept = b(1,:);
pval = []; ci = []; stats = [];
[~, pval, ci, stats] = ttest(IndividualSlopes);

disp(['b=',num2str(round(mean(IndividualSlopes),2)),...
    ' [',num2str(round(ci(1),2)),',',num2str(round(ci(2),2)),']; t(',num2str(stats.df),')=', ...
    num2str(round(stats.tstat,2)), ', p=',num2str(pval)])

disp(['b=',num2str(mean(IndividualSlopes)),...
    '[',num2str(ci(1)),',',num2str(ci(2)),']; t(',num2str(stats.df),')=', ...
    num2str(round(stats.tstat,2)), ', p=',num2str(pval)])


%% benjamini-hochberg-adjusted p-values

pval = []; ci = []; stats = [];
for indComparison = 1:4
    [~, pval(indComparison), ci(indComparison,:), stats{indComparison}] = ttest(data(:,indComparison),0);
end

pval_adjust(pval, 'fdr')


pval = []; ci = []; stats = [];
for indComparison = 1:8
    [~, pval(indComparison), ci(indComparison,:), stats{indComparison}] = ttest(data(:,indComparison),.5);
end

pval_adjust(pval, 'fdr')

%% standardized betas: why are the statistics not the same?

X = [1 1; 1 2; 1 3; 1 4]; 
X_stand = zscore(X,0,1); data_stand = zscore(data',0,1); b=X_stand\data_stand; 
%X_stand = zscore(X,[],1); data_stand = (data-mean(data,2))./std(data,[],2); b=X_stand\data_stand'; 
b2=X\data(:,:)'; 
IndividualSlopes = b(2,:);
pval = []; ci = []; stats = [];
[~, pval, ci, stats] = ttest(IndividualSlopes);

disp(['b=',num2str(round(mean(IndividualSlopes),2)),...
    '[',num2str(round(ci(1),2)),',',num2str(round(ci(2),2)),']; t(',num2str(stats.df),')=', ...
    num2str(round(stats.tstat,2)), ', p=',num2str(pval)])

figure; scatter(b2(2,:), b(2,:))

regress(data_stand(1,:)', X_stand)

mean(IndividualSlopes)/std(IndividualSlopes)

%% benjamini-hochberg-adjusted p-values (all combinations)

combs = [1,2; 1,3; 1,4; 2,3; 2,4; 3,4];

pval = []; ci = []; stats = [];
for indComparison = 1:size(combs,1)
    [~, pval(indComparison), ci(indComparison,:), stats{indComparison}] = ttest(data(:,combs(indComparison,1)), data(:,combs(indComparison,2)));
end

pval_adjust(pval, 'fdr')

%% get correlation 95% CIs

[r, p , ci1, ci2] = corrcoef(data(:,1), data(:,2))
[r, p , ci1, ci2] = corrcoef(data(:,1), data(:,3))

CI = [round(ci1(2),2), round(ci2(2),2)]

%% test that algo works

pvalue = [.2703633;...
    .2861805;...
 .0674455;...
  .2884618;...
  .0522945;...
  .0094186;...
 .0412532;...
  .1566575;...
 .00296;...
.0068728;...
.025553;...
.3102804;...
.0042578;...
.0031542;...
.000024];

sort_pval = sort(pvalue, 'ascend');
N = numel(sort_pval);
alpha = .05;
criteria = alpha./(N-([1:N]-1));

sort_pval<criteria'

pval_adjust(sort_pval, 'sidak')
pval_adjust(sort_pval, 'holm')
pval_adjust(sort_pval, 'fdr')
