[![made-with-datalad](https://www.datalad.org/badges/made_with.svg)](https://datalad.org)

## Work with multimodal summary data

## Code overview

**a_createCSV_YAOA**

- collect individual avg. data for various signatures to collect in summary structure

**f_drift_cpp_agecomp**

![image](figures/drift_cpp.png)

**g_STSW_stats**

- control for multiple comparisons
- pvals to correct for have to be manually copied